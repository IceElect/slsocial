<div class="main-col">
	<div class="user-page">
		<div class="col-sidebar">
			<div class="avatar thumb big">
				<div onclick="return photo.photo('{$u->avatar}', 1, event);">{get_avatar u=$u}</div>

				{if $is_my_page}
				<button class="md-icon" onclick="photo.upload_popup(photo.avatar_upload, {$u->avatar_album}, event)">camera_alt</button>
				<button class="md-icon" onclick="photo.crop_popup(photo.avatar_crop, {$u->avatar}, event)">photo_size_select_small</button>
				{/if}
			</div>

			<div class="thumb level-thumb">
				<div class="level-block">
					<div class="level-counter">
						{$exp = 300}
						{$exp_ = 1}
						{$exp_razniza = 300}
						{$level = 0}
						{while $exp <= $u->exp}
							{$exp = $exp + $level * 300}
							{$exp_razniza = $exp - $exp_}
							{$exp_ = $exp}
							{$level = $level + 1}
						{/while}
						{$level} уровень
						<div class="level-exp-counter" title="До следующего уровня осталось {$exp-($u->exp)} опыта">{$u->exp}</div>
					</div>
					<div class="level-progress">
						{$progress = ($exp_razniza - ($exp-$u->exp)) / ($exp_razniza) * 100}
						<div class="level-progress-bg" style="width: {$progress}% !important;"></div>
					</div>
				</div>
			</div>

			<!--
			<div class="level thumb">
				Кнопочки действий
			</div>
			<div class="level thumb">
				Уровень
			</div>
			<div class="level thumb">
				Достижения
				<br><br><br>
			</div>
			<div class="level thumb">
				Реклама
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			</div>
			-->
		</div>
		<div class="col-content">
			<div class="user-page-header {if ($u->last_action >= ($time - 900))}online{/if} big">
				<div class="user-page-info thumb flr">
					<div class="row">
						<h1 href="#" class="name">{$u->fname} {$u->lname}</h1>
						<div class="spacer"></div>
						{if ($u->last_action >= ($time - 900))}<span>В сети</span>{else}<span><abbr title="{$u->last_action|date_format:"%Y-%m-%d %H:%M:%S"}" class="time"></abbr></span>{/if}
					</div>
					<div class="row user-actions">
						{if $is_logged}
						{if $is_my_page}
							<button href="/edit" data-type="load" class="icon">Редактировать</button>
						{else}
							<button class="icon user-action-friend">Добавить в друзья</button>
						{/if}
						<div class="spacer"></div>
						<button href="/im/{$u->id}" data-type="load" class="md-icon user-action-message">textsms</button>
						<button class="md-icon user-action-more" data-type="actions" data-action="user" data-id="{$u->id}">more_horiz</button>

						<ul class="actions-menu user-menu">
							<li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
						</ul>
						{else}
							<p class="log-message">Пожалуйста, войдите на сайт или зарегистрируйтесь, чтобы написать сообщение.</p>
						{/if}
					</div>
					<div class="row status-holder"{if $is_my_page} onclick="Page.infoEdit(event, this);" data-item="status" data-action="change_status"{/if}>
						<p data-page-info="status">{$u->status}</p>
						{if $is_my_page}
						<div id="info_editor" class="page-status-editor" onclick="cancelEvent(event)">
							<div class="editor">
								<div class="form-row">
									<div class="page-status-input field" id="info_input" contenteditable="true" role="textbox" data-page-info="status">{$u->status}</div>
								</div>
								<div class="form-row">
									<button class="button" onclick="Page.infoSave(event, this);">Сохранить</button>
								</div>
							</div>
						</div>
						{/if}
					</div>
					<div class="clearfix"></div>
					{*
					<div class="row info-param">
						<a href="#"><i class="md-icon">work</i>Работает в SL</a>
					</div>
					*}
					<div class="row info-param">
						<a href="#"><i class="md-icon">home</i>{$countries.{$u->country}}, {$cities.{$u->city}}</a>
					</div>
					{if $u->birthdate != "0000-00-00"}
					<div class="row info-param">
						<a href="#"><i class="md-icon">cake</i>{birthday date=$u->birthdate}</a>
					</div>
					{/if}
					<div class="row info-param">
						<a href="#"><i class="md-icon">favorite</i>{$user_lstatus.{$u->lstatus}}</a>
					</div>
					<div class="row info-param">
						<a href="#"><i class="md-icon">people</i>{decline number={$counts->friends} text="%n% дру%o%" eArray=['г', 'га', 'зей']}</a>
					</div>
					{*
					<div class="row info-param">
						<a href="#"><i class="md-icon">people</i>2 общих друга</a>
					</div>
					*}
				</div>
				<div class="clearfix"></div>
			</div>
		
			<div class="wall">
				<div class="send-post-form send-post" id="send-post-form" data-wall-id="{$u->id}">
			        <div class="avatar middle">
			            {get_avatar u=$oUser}
			        </div>
			        <div class="send-form-holder">
			        	<div style="position: relative">
					        <div onkeydown="onCtrlEnter(event, wall.sendPost)" onpaste="wall.postChanged(this)" class="send-form-area" contenteditable="true" placeholder="Что нового?" id="send-post-content"></div>
					        <div class="buttons">
					        	<button class="fl-r attach-button icon icon-attach" onclick="wall.showAttachMenu()">
					        		<span class="count"></span>
					        	</button>
					            <button class="fl-r send-button icon icon-paper-plane"></button>
					            <!--
					            <label for="attach-input" >
					            	<span class="count"></span>
					        	</label>
					        	<input type="file" id="attach-input" class="attach-input" multiple="multiple">
					        	-->
					        	<ul class="actions-menu actions-attach user-menu loaded">
					        		<li><a href="javascript:void(0)" onclick="wall.showAttachPhoto();"><i class="icon icon-flag"></i><span>Фотографию</span></a></li>
					        		<li><a href="javascript:void(0)"><i class="icon icon-flag"></i><span>Аудиозапись</span></a></li>
					        	</ul>
					        </div>
				        </div>
				        <div class="form-attaches">
				        	<div class="photos-choose-rows"></div>
				        </div>
			        </div>
			    </div>

			    <div class="thumb user-tabs">
			    	<ul>
			    		<li><a href="javascript:void(0)" class="user-tab user-tab-sel" data-type="wall" onclick="Page.loadPageSect('{$u->id}', 'wall');">Новости <span>{$counts->posts}</span></a></li>
			    		<li><a href="javascript:void(0)" class="user-tab" data-type="friends" onclick="Page.loadPageSect('{$u->id}', 'friends');">Друзья <span>{$counts->friends}</span></a></li>
			    		<li><a href="javascript:void(0)" class="user-tab" data-type="albums" onclick="Page.loadPageSect('{$u->id}', 'albums');">Фотоальбомы <span>{$counts->albums}</span></a></li>
			    	</ul>
			    </div>

				<section data-page-info="section">
					<div class="wall-posts">
						{foreach from=$wall item=post}
							{include file="wall/post.tpl" post=$post}
						{/foreach}
						<div class="nav"><a href="{$base_url}@1?page={$page}"></a></div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>