<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Photo
{

    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        //$this->activeModule = &$this->CI->activeModule;
    }

    function view($path, $size = '200x200'){
        $size = explode('x', $size);

        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
        header('Content-Type: image/jpeg');

        $width = $size[0];
        $height = (isset($size[1]))?$size[1]:false;

        $img = new Imagick();
        $img->readImage($path);
        $img->setImageFormat('jpg');
        $img->thumbnailImage($width, $height);
        $img->setImageCompression(Imagick::COMPRESSION_JPEG);
        $img->setImageCompressionQuality(75);

        echo $img;
    }

    function getThumbnail($image, $size = 200, $create = false){
        $file = pathinfo($image->src);
        $thumbnail_filename = $file['filename'] . '_' . $size . '.' . $file['extension'];
        $thumbnail_src = $file['dirname'] . '/' . $thumbnail_filename;
        if(file_exists(FCPATH . $thumbnail_src)){
            return $thumbnail_src;
        }else{
            if($create){

                $size = explode('x', $size);
                
                $width = $size[0];
                $height = (isset($size[1]))?$size[1]:false;

                $img = new Imagick();
                $img->readImage(FCPATH . $image->src);
                $img->setImageFormat('jpg');
                $img->thumbnailImage($width, false);
                $img->setImageCompression(Imagick::COMPRESSION_JPEG);
                $img->setImageCompressionQuality(75);

                $img->writeImage(FCPATH . $thumbnail_src);

                return $thumbnail_src;
            }
        }
    }

    function view_old($imgpath, $size = '200x200'){

        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
        
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime_type = finfo_file($finfo, $imgpath);
        finfo_close($finfo);

        list($width, $height) = getimagesize($imgpath);
        $size = explode('x', $size);

        $thumb = imagecreatetruecolor($size[0], $size[1]);

        imagealphablending($thumb, false);
        imagesavealpha($thumb,true);
        $transparent = imagecolorallocatealpha($thumb, 0, 0, 0, 0);
        imagefilledrectangle($thumb, 0, 0, $size[0], $size[1], $transparent);
         
        switch ($mime_type){
            case "image/jpeg":
                header('Content-Type: image/jpeg');
                 
                $source = imagecreatefromjpeg($imgpath);
                imagecopyresampled($thumb, $source, 0, 0, 0, 0, $size[0], $size[1], $width, $height);
                imagejpeg($thumb);
                 
                break;
            case "image/png":
                header('Content-Type: image/png');
                $img = imagecreatefrompng($imgpath);
                $background = imagecolorallocate($img, 0, 0, 0);
                imagecolortransparent($img, $background);
                imagealphablending($img, false);
                imagesavealpha($img, true);
                imagecopyresized($thumb, $img, 0, 0, 0, 0, $size[0], $size[1], $width, $height);
                imagepng($thumb);
                 
                break;
            case "image/gif":
                header('Content-Type: image/gif');
                $img = imagecreatefromgif($imgpath);
                $background = imagecolorallocate($img, 0, 0, 0);
                imagecolortransparent($img, $background);

                imagecopyresized($thumb, $img, 0, 0, 0, 0, $size[0], $size[1], $width, $height);
                imagegif($thumb);
                 
                break;
        }

        imagedestroy($img);
    }

}