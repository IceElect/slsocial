<div class="albums-choose-rows">
    {foreach from=$albums item=$a}
        <div class="album-choose-row">
            <div class="album-row-img" style="background-image: url('{$a->cover}');">
                <div class="album-row-title-holder">
                    <div class="clearfix">
                    <div class="album-row-counter flr">{$a->photos_count}</div>
                    <div class="album-row-title" title="{$a->name}">{$a->name}</div>
                    </div>
                    <div class="photos_album_description_wrap"><div class="photos_album_description description"></div></div>
                </div>
            </div>
        </div>
    {/foreach}
</div>