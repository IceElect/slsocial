<?php


ini_set('error_reporting', E_ALL);
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', 'error.log');
ini_set('date_default_timezone_set', 'UTC');
version_compare(PHP_VERSION, '5.2', '>=') or exit('PHP ' . PHP_VERSION . ' is not supported');

$conf = parse_ini_file('conf.ini');
ob_start();
    $content = require_once('/index.html');
    $content = ob_get_contents();
ob_end_clean();
$r = '~<(h1|h2|h3|h4|h5|h6|p|span|div|a)[^>]*>\K[^<]+(?=</\1>)~';
$content = preg_replace($r, '<edit contenteditable data-default-content="$0">$0</edit>', $content);

$u = array(
    'auth' => '',
    'base' => '<!DOCTYPE html><html lang="en"><head><link rel="stylesheet" href="elite.css"></head><body id="elite-body"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script><script src="elite.js"></script><script src="js/common.js"></script><div id="eliter-file" class="eliter-file">'.$content.'</div></body></html>',
);
echo $u['base'];