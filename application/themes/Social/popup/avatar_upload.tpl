<div class="popup_bg"></div>
<div class="popup block" data-id="avatar_upload">
    <div class="popup_header_holder">
        <div class="popup_header module_title">
            Аватар профиля <button class="md-icon close" onclick="popup.hide('avatar_upload');">close</button>
        </div>
    </div>
    <div class="popup_content module_content">
        <b>Ограничения</b>
        <ul>
            <li>Файл должен иметь размно не более 20 МБ и расширения PNG, JPEG или GIF</li>
        </ul>

        <div class="clearfix"></div>

        <div class="file-upload">
            <input id="file" type="file" style="display: none;" onchange="upload.onFile('avatar', '{$album}', this.files);" size="28" accept="image/jpeg,image/png,image/gif,image/heic,image/heif">
            <label for="file" class="button">Загрузить файл</label>
        </div>
        
        <div class="load-info">
            <span id="message"></span>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!--<script src="/views/Social/js/drop_load.js"></script>-->