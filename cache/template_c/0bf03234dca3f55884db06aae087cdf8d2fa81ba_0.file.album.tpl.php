<?php
/* Smarty version 3.1.29, created on 2020-01-09 21:24:56
  from "/srv/slto.ru/www/application/themes/Social/popup/album.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e176ff84fe866_79910059',
  'file_dependency' => 
  array (
    '0bf03234dca3f55884db06aae087cdf8d2fa81ba' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/popup/album.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e176ff84fe866_79910059 ($_smarty_tpl) {
?>
<div class="popup_bg"></div>
<div class="popup popup-top" data-id="attach_photo">
    <div class="popup_header_holder">
        <div class="popup_header module_title">
            <?php echo $_smarty_tpl->tpl_vars['album']->value->name;?>
 <button class="md-icon close" onclick="popup.hide('attach_photo');">close</button>
        </div>
    </div>
    <div class="popup_controls module_conten">
        <input id="file" type="file" style="display: none;" onchange="upload.onFile('photo', <?php echo $_smarty_tpl->tpl_vars['album']->value->id;?>
, this.files);" multiple="true" size="28" accept="image/jpeg,image/png,image/gif,image/heic,image/heif">
            <label for="file" class="button">Загрузить файл</label>
        <div class="clearfix"></div>
    </div>
    <div class="popup_content module_content">

        <div class="photos-choose-rows">
            <?php
$_from = $_smarty_tpl->tpl_vars['photos']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_photo_0_saved_item = isset($_smarty_tpl->tpl_vars['photo']) ? $_smarty_tpl->tpl_vars['photo'] : false;
$__foreach_photo_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['photo']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
$__foreach_photo_0_saved_local_item = $_smarty_tpl->tpl_vars['photo'];
?>
            <a class="photo-choose-row" onclick="return photo.photo('<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
', 1, event);">
                <div class="photo-row-img" style="background-image: url('/ajax/photo/view/<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
');"></div>
            </a>
            <?php
$_smarty_tpl->tpl_vars['photo'] = $__foreach_photo_0_saved_local_item;
}
if ($__foreach_photo_0_saved_item) {
$_smarty_tpl->tpl_vars['photo'] = $__foreach_photo_0_saved_item;
}
if ($__foreach_photo_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_photo_0_saved_key;
}
?>
        </div>
        
        <div class="load-info">
            <span id="message"></span>
        </div>
        <div class="clearfix"></div>
    </div>
</div><?php }
}
