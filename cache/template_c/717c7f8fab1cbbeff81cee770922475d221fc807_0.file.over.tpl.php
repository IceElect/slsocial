<?php
/* Smarty version 3.1.29, created on 2020-02-13 15:27:10
  from "/srv/slto.ru/www/application/themes/Social/notify/over.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e45409e159bd0_32545958',
  'file_dependency' => 
  array (
    '717c7f8fab1cbbeff81cee770922475d221fc807' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/notify/over.tpl',
      1 => 1576341909,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e45409e159bd0_32545958 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<div class="notify" id="notify_over_<?php echo $_smarty_tpl->tpl_vars['notify']->value['id'];?>
">
	<a class="notify-inner" href="<?php echo $_smarty_tpl->tpl_vars['notify']->value['link'];?>
" data-type="load">
		<div class="notify-photo">
			<?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['_user']->value),$_smarty_tpl);?>

		</div>
		<div class="notify-content">
			<div class="notify-title">
				<?php echo $_smarty_tpl->tpl_vars['_user']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['_user']->value->lname;?>

			</div>
			<div class="notify-content">
				<?php echo $_smarty_tpl->tpl_vars['notify']->value['text'];?>

			</div>
		</div>
	</a>
</div><?php }
}
