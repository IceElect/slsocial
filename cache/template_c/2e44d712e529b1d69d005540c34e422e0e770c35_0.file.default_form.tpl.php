<?php
/* Smarty version 3.1.29, created on 2020-01-08 19:32:36
  from "/srv/slto.ru/www/application/themes/Social/default_form.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e160424b551c4_37580875',
  'file_dependency' => 
  array (
    '2e44d712e529b1d69d005540c34e422e0e770c35' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/default_form.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:sys/messages.tpl' => 1,
    'file:custom/".((string)$_smarty_tpl->tpl_vars[\'path\']->value).".tpl' => 1,
    'file:default_form_row.tpl' => 2,
    'file:default/".((string)$_smarty_tpl->tpl_vars[\'hook\']->value).".tpl' => 3,
  ),
),false)) {
function content_5e160424b551c4_37580875 ($_smarty_tpl) {
if (!is_callable('smarty_function_csrf')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.csrf.php';
if (!is_callable('smarty_function_translate')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.translate.php';
$_smarty_tpl->tpl_vars["required_star"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "required_star", 0);
$_smarty_tpl->tpl_vars["i"] = new Smarty_Variable('0', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?><div class="main-col"> <div class="user-page"><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/messages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="col-content left-content fll"> <form <?php
$_from = $_smarty_tpl->tpl_vars['form_attributes']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_0_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_0_saved_key = isset($_smarty_tpl->tpl_vars['var']) ? $_smarty_tpl->tpl_vars['var'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['var'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['var']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$__foreach_value_0_saved_local_item = $_smarty_tpl->tpl_vars['value'];
echo $_smarty_tpl->tpl_vars['var']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
"<?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_local_item;
}
if ($__foreach_value_0_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_item;
}
if ($__foreach_value_0_saved_key) {
$_smarty_tpl->tpl_vars['var'] = $__foreach_value_0_saved_key;
}
?> data-type="ajax"><?php echo smarty_function_csrf(array(),$_smarty_tpl);?>
<div class="thumb"> <div class="pedit-form"><?php if ($_smarty_tpl->tpl_vars['action']->value != 'add') {?><input id="item_id" type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['nId']->value;?>
"/><?php } else { ?><input type="hidden" name="change_pwd" value="1"/><?php }?><!--<?php if ($_smarty_tpl->tpl_vars['action']->value != 'add') {?><div class="form-group"> <div class="col-lg-2 col-sm-2 control-label">ID :</div> <div class="col-sm-6 infofield"> <p class="form-control-static"><?php echo $_smarty_tpl->tpl_vars['nId']->value;?>
</p> </div> </div><?php }?>--><?php $_smarty_tpl->tpl_vars['num'] = new Smarty_Variable(0, true);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'num', 0);
$_from = $_smarty_tpl->tpl_vars['aFields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_fields_1_saved_item = isset($_smarty_tpl->tpl_vars['fields']) ? $_smarty_tpl->tpl_vars['fields'] : false;
$__foreach_fields_1_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['fields'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['fields']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['fields']->value) {
$_smarty_tpl->tpl_vars['fields']->_loop = true;
$__foreach_fields_1_saved_local_item = $_smarty_tpl->tpl_vars['fields'];
if ((!isset($_smarty_tpl->tpl_vars['tabs']->value[$_smarty_tpl->tpl_vars['num']->value]))) {
continue 1;
}?><section class="content tab-content <?php if ($_smarty_tpl->tpl_vars['num']->value == 0) {?>current<?php }?>" id="<?php echo $_smarty_tpl->tpl_vars['tabs']->value[$_smarty_tpl->tpl_vars['num']->value]['id'];?>
"><?php
$_from = $_smarty_tpl->tpl_vars['fields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_2_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$__foreach_field_2_saved_key = isset($_smarty_tpl->tpl_vars['field_key']) ? $_smarty_tpl->tpl_vars['field_key'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field_key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field_key']->value => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_2_saved_local_item = $_smarty_tpl->tpl_vars['field'];
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, true);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if (!empty($_smarty_tpl->tpl_vars['field']->value['custom_field_template'])) {
$_smarty_tpl->tpl_vars['path'] = new Smarty_Variable($_smarty_tpl->tpl_vars['field']->value['custom_field_template'], true);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'path', 0);
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:custom/".((string)$_smarty_tpl->tpl_vars['path']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['field']->value,'aData'=>$_smarty_tpl->tpl_vars['aData']->value,'i'=>$_smarty_tpl->tpl_vars['i']->value), 0, true);
} else {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:default_form_row.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['field']->value,'aData'=>$_smarty_tpl->tpl_vars['aData']->value,'i'=>$_smarty_tpl->tpl_vars['i']->value), 0, true);
}
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_2_saved_local_item;
}
if ($__foreach_field_2_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_2_saved_item;
}
if ($__foreach_field_2_saved_key) {
$_smarty_tpl->tpl_vars['field_key'] = $__foreach_field_2_saved_key;
}
if (isset($_smarty_tpl->tpl_vars['hooks']->value[$_smarty_tpl->tpl_vars['tabs']->value[$_smarty_tpl->tpl_vars['num']->value]['id']])) {
$_from = $_smarty_tpl->tpl_vars['hooks']->value[$_smarty_tpl->tpl_vars['tabs']->value[$_smarty_tpl->tpl_vars['num']->value]['id']];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_hook_3_saved_item = isset($_smarty_tpl->tpl_vars['hook']) ? $_smarty_tpl->tpl_vars['hook'] : false;
$__foreach_hook_3_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['hook'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['hook']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['hook']->value) {
$_smarty_tpl->tpl_vars['hook']->_loop = true;
$__foreach_hook_3_saved_local_item = $_smarty_tpl->tpl_vars['hook'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:default/".((string)$_smarty_tpl->tpl_vars['hook']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['hook'] = $__foreach_hook_3_saved_local_item;
}
if ($__foreach_hook_3_saved_item) {
$_smarty_tpl->tpl_vars['hook'] = $__foreach_hook_3_saved_item;
}
if ($__foreach_hook_3_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_hook_3_saved_key;
}
}?><div class="form-row clearfix"> <div class="form-label"></div> <div class="form-input-wrap"> <button type="submit" name="submit" value="1" class="button flr">Сохранить</button> </div> </div> </section><?php
$_smarty_tpl->tpl_vars['fields'] = $__foreach_fields_1_saved_local_item;
}
if ($__foreach_fields_1_saved_item) {
$_smarty_tpl->tpl_vars['fields'] = $__foreach_fields_1_saved_item;
}
if ($__foreach_fields_1_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_fields_1_saved_key;
}
if ($_smarty_tpl->tpl_vars['tabs']->value) {?></div><?php }
if (isset($_smarty_tpl->tpl_vars['hooks']->value['after_form'])) {
$_from = $_smarty_tpl->tpl_vars['hooks']->value['after_form'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_hook_4_saved_item = isset($_smarty_tpl->tpl_vars['hook']) ? $_smarty_tpl->tpl_vars['hook'] : false;
$__foreach_hook_4_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['hook'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['hook']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['hook']->value) {
$_smarty_tpl->tpl_vars['hook']->_loop = true;
$__foreach_hook_4_saved_local_item = $_smarty_tpl->tpl_vars['hook'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:default/".((string)$_smarty_tpl->tpl_vars['hook']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['hook'] = $__foreach_hook_4_saved_local_item;
}
if ($__foreach_hook_4_saved_item) {
$_smarty_tpl->tpl_vars['hook'] = $__foreach_hook_4_saved_item;
}
if ($__foreach_hook_4_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_hook_4_saved_key;
}
}?></div> </form> </div> <div class="col-sidebar flr"><?php if ($_smarty_tpl->tpl_vars['tabs']->value) {?><div class="thumb tabs"><?php
$_from = $_smarty_tpl->tpl_vars['tabs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tab_5_saved_item = isset($_smarty_tpl->tpl_vars['tab']) ? $_smarty_tpl->tpl_vars['tab'] : false;
$__foreach_tab_5_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['tab']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
$__foreach_tab_5_saved_local_item = $_smarty_tpl->tpl_vars['tab'];
?><div role="tab" id="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" href="#<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" aria-controls="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" class="body-nest tab <?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>current<?php }?>"><?php echo $_smarty_tpl->tpl_vars['tab']->value['title'];?>
</div><?php
$_smarty_tpl->tpl_vars['tab'] = $__foreach_tab_5_saved_local_item;
}
if ($__foreach_tab_5_saved_item) {
$_smarty_tpl->tpl_vars['tab'] = $__foreach_tab_5_saved_item;
}
if ($__foreach_tab_5_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_tab_5_saved_key;
}
?></div><?php }?><!--<div class="thumb"> <div class="title-alt"> <h6>Действия</h6> <div class="titleClose"> <a class="gone" href="#elementClose"> <span class="entypo-cancel"></span> </a> </div> <div class="titleToggle"> <a class="nav-toggle-alt" href="#element"> <span class="entypo-up-open"></span> </a> </div> </div> <div class="body-nest"> <section class="col-xs-12"><?php if (isset($_smarty_tpl->tpl_vars['aFields']->value['sidebar'])) {
$_from = $_smarty_tpl->tpl_vars['aFields']->value['sidebar'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_6_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$__foreach_field_6_saved_key = isset($_smarty_tpl->tpl_vars['field_key']) ? $_smarty_tpl->tpl_vars['field_key'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field_key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field_key']->value => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_6_saved_local_item = $_smarty_tpl->tpl_vars['field'];
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, true);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:default_form_row.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('field'=>$_smarty_tpl->tpl_vars['field']->value,'aData'=>$_smarty_tpl->tpl_vars['aData']->value,'i'=>$_smarty_tpl->tpl_vars['i']->value), 0, true);
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_6_saved_local_item;
}
if ($__foreach_field_6_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_6_saved_item;
}
if ($__foreach_field_6_saved_key) {
$_smarty_tpl->tpl_vars['field_key'] = $__foreach_field_6_saved_key;
}
}?><div class="row box-btn form-actions btn-group pull-left"> <div class="col-sm-12"> <button type="submit" name="submit" value="1" class="btn btn-success"> <i class="fa fa-floppy-o" style="margin-right: 0"></i> </button> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button> <ul class="dropdown-menu" role="menu"> <li><a><button name="submit_exit" value="1" type="submit"><?php echo smarty_function_translate(array('code'=>"form_save_exit",'text'=>"Сохранить и выйти"),$_smarty_tpl);?>
</button></a></li> <li><a><button name="submit_next" value="1" type="submit"><?php echo smarty_function_translate(array('code'=>"form_save_next",'text'=>"Сохранить и далее"),$_smarty_tpl);?>
</button></a></li> </ul> </div> </div> <div class="row box-btn form-actions btn-group pull-right"> <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['aConf']->value['base_url'];
echo $_smarty_tpl->tpl_vars['aConf']->value['active_module'];?>
"> <span><?php echo smarty_function_translate(array('code'=>"form_cancel",'text'=>"Отмена"),$_smarty_tpl);?>
</span> </a> </div> </section> <div class="clearfix"></div> </div> </div>--><?php if (isset($_smarty_tpl->tpl_vars['hooks']->value['after_sidebar'])) {
$_from = $_smarty_tpl->tpl_vars['hooks']->value['after_sidebar'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_hook_7_saved_item = isset($_smarty_tpl->tpl_vars['hook']) ? $_smarty_tpl->tpl_vars['hook'] : false;
$__foreach_hook_7_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['hook'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['hook']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['hook']->value) {
$_smarty_tpl->tpl_vars['hook']->_loop = true;
$__foreach_hook_7_saved_local_item = $_smarty_tpl->tpl_vars['hook'];
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:default/".((string)$_smarty_tpl->tpl_vars['hook']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
$_smarty_tpl->tpl_vars['hook'] = $__foreach_hook_7_saved_local_item;
}
if ($__foreach_hook_7_saved_item) {
$_smarty_tpl->tpl_vars['hook'] = $__foreach_hook_7_saved_item;
}
if ($__foreach_hook_7_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_hook_7_saved_key;
}
}?></div> </div> </div><?php echo '<script'; ?>
 type="text/javascript">

        function fields_remove_image(el, id, field_name) {
            $.ajax({
                type: 'POST',
                url: '<?php echo $_smarty_tpl->tpl_vars['aConf']->value['base_url'];
echo $_smarty_tpl->tpl_vars['aConf']->value['active_module'];?>
/remove_file/' + id + '/' + field_name,
                dataType: 'json',
                success: function (data) {
                    $(el).parent('.image_holder').hide();
                },
                data: {'ajax': 1},
                async: false
            });
        }
        $(document).on('click', '.tabs .tab', function(e){
            e.preventDefault();

            var id = $(this).attr('aria-controls'),
                tabs = $(this).parents('.tabs');
            
            tabs.find('.current').removeClass('current');
            $(this).addClass('current');

            $(".content.tab-content.current").removeClass('current');
            $(".content.tab-content#"+id).addClass('current');
        })
        <?php echo '</script'; ?>
><?php }
}
