<?php
/* Smarty version 3.1.29, created on 2020-01-05 16:28:56
  from "/srv/slto.ru/www/application/themes/Social/user/profile.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e11e4989dda04_28122497',
  'file_dependency' => 
  array (
    'fd30740d28d57f31629189ee0f0ccec32704a3c1' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/user/profile.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:wall/post.tpl' => 1,
  ),
),false)) {
function content_5e11e4989dda04_28122497 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
if (!is_callable('smarty_modifier_date_format')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
if (!is_callable('smarty_function_birthday')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.birthday.php';
if (!is_callable('smarty_function_decline')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.decline.php';
?>
<div class="main-col">
	<div class="user-page">
		<div class="col-sidebar">
			<div class="avatar thumb big">
				<div onclick="return photo.photo('<?php echo $_smarty_tpl->tpl_vars['u']->value->avatar;?>
', 1, event);"><?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['u']->value),$_smarty_tpl);?>
</div>

				<?php if ($_smarty_tpl->tpl_vars['is_my_page']->value) {?>
				<button class="md-icon" onclick="photo.upload_popup(photo.avatar_upload, <?php echo $_smarty_tpl->tpl_vars['u']->value->avatar_album;?>
, event)">camera_alt</button>
				<button class="md-icon" onclick="photo.crop_popup(photo.avatar_crop, <?php echo $_smarty_tpl->tpl_vars['u']->value->avatar;?>
, event)">photo_size_select_small</button>
				<?php }?>
			</div>

			<div class="thumb level-thumb">
				<div class="level-block">
					<div class="level-counter">
						<?php $_smarty_tpl->tpl_vars['exp'] = new Smarty_Variable(300, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'exp', 0);?>
						<?php $_smarty_tpl->tpl_vars['exp_'] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'exp_', 0);?>
						<?php $_smarty_tpl->tpl_vars['exp_razniza'] = new Smarty_Variable(300, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'exp_razniza', 0);?>
						<?php $_smarty_tpl->tpl_vars['level'] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'level', 0);?>
						<?php
while ($_smarty_tpl->tpl_vars['exp']->value <= $_smarty_tpl->tpl_vars['u']->value->exp) {?>
							<?php $_smarty_tpl->tpl_vars['exp'] = new Smarty_Variable($_smarty_tpl->tpl_vars['exp']->value+$_smarty_tpl->tpl_vars['level']->value*300, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'exp', 0);?>
							<?php $_smarty_tpl->tpl_vars['exp_razniza'] = new Smarty_Variable($_smarty_tpl->tpl_vars['exp']->value-$_smarty_tpl->tpl_vars['exp_']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'exp_razniza', 0);?>
							<?php $_smarty_tpl->tpl_vars['exp_'] = new Smarty_Variable($_smarty_tpl->tpl_vars['exp']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'exp_', 0);?>
							<?php $_smarty_tpl->tpl_vars['level'] = new Smarty_Variable($_smarty_tpl->tpl_vars['level']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'level', 0);?>
						<?php }?>

						<?php echo $_smarty_tpl->tpl_vars['level']->value;?>
 уровень
						<div class="level-exp-counter" title="До следующего уровня осталось <?php echo $_smarty_tpl->tpl_vars['exp']->value-($_smarty_tpl->tpl_vars['u']->value->exp);?>
 опыта"><?php echo $_smarty_tpl->tpl_vars['u']->value->exp;?>
</div>
					</div>
					<div class="level-progress">
						<?php $_smarty_tpl->tpl_vars['progress'] = new Smarty_Variable(($_smarty_tpl->tpl_vars['exp_razniza']->value-($_smarty_tpl->tpl_vars['exp']->value-$_smarty_tpl->tpl_vars['u']->value->exp))/($_smarty_tpl->tpl_vars['exp_razniza']->value)*100, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'progress', 0);?>
						<div class="level-progress-bg" style="width: <?php echo $_smarty_tpl->tpl_vars['progress']->value;?>
% !important;"></div>
					</div>
				</div>
			</div>

			<!--
			<div class="level thumb">
				Кнопочки действий
			</div>
			<div class="level thumb">
				Уровень
			</div>
			<div class="level thumb">
				Достижения
				<br><br><br>
			</div>
			<div class="level thumb">
				Реклама
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			</div>
			-->
		</div>
		<div class="col-content">
			<div class="user-page-header <?php if (($_smarty_tpl->tpl_vars['u']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?> big">
				<div class="user-page-info thumb flr">
					<div class="row">
						<h1 href="#" class="name"><?php echo $_smarty_tpl->tpl_vars['u']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['u']->value->lname;?>
</h1>
						<div class="spacer"></div>
						<?php if (($_smarty_tpl->tpl_vars['u']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?><span>В сети</span><?php } else { ?><span><abbr title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['u']->value->last_action,"%Y-%m-%d %H:%M:%S");?>
" class="time"></abbr></span><?php }?>
					</div>
					<div class="row user-actions">
						<?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?>
						<?php if ($_smarty_tpl->tpl_vars['is_my_page']->value) {?>
							<button href="/edit" data-type="load" class="icon">Редактировать</button>
						<?php } else { ?>
							<button class="icon user-action-friend">Добавить в друзья</button>
						<?php }?>
						<div class="spacer"></div>
						<button href="/im/<?php echo $_smarty_tpl->tpl_vars['u']->value->id;?>
" data-type="load" class="md-icon user-action-message">textsms</button>
						<button class="md-icon user-action-more" data-type="actions" data-action="user" data-id="<?php echo $_smarty_tpl->tpl_vars['u']->value->id;?>
">more_horiz</button>

						<ul class="actions-menu user-menu">
							<li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
						</ul>
						<?php } else { ?>
							<p class="log-message">Пожалуйста, войдите на сайт или зарегистрируйтесь, чтобы написать сообщение.</p>
						<?php }?>
					</div>
					<div class="row status-holder"<?php if ($_smarty_tpl->tpl_vars['is_my_page']->value) {?> onclick="Page.infoEdit(event, this);" data-item="status" data-action="change_status"<?php }?>>
						<p data-page-info="status"><?php echo $_smarty_tpl->tpl_vars['u']->value->status;?>
</p>
						<?php if ($_smarty_tpl->tpl_vars['is_my_page']->value) {?>
						<div id="info_editor" class="page-status-editor" onclick="cancelEvent(event)">
							<div class="editor">
								<div class="form-row">
									<div class="page-status-input field" id="info_input" contenteditable="true" role="textbox" data-page-info="status"><?php echo $_smarty_tpl->tpl_vars['u']->value->status;?>
</div>
								</div>
								<div class="form-row">
									<button class="button" onclick="Page.infoSave(event, this);">Сохранить</button>
								</div>
							</div>
						</div>
						<?php }?>
					</div>
					<div class="clearfix"></div>
					
					<div class="row info-param">
						<a href="#"><i class="md-icon">home</i><?php echo $_smarty_tpl->tpl_vars['countries']->value[$_smarty_tpl->tpl_vars['u']->value->country];?>
, <?php echo $_smarty_tpl->tpl_vars['cities']->value[$_smarty_tpl->tpl_vars['u']->value->city];?>
</a>
					</div>
					<?php if ($_smarty_tpl->tpl_vars['u']->value->birthdate != "0000-00-00") {?>
					<div class="row info-param">
						<a href="#"><i class="md-icon">cake</i><?php echo smarty_function_birthday(array('date'=>$_smarty_tpl->tpl_vars['u']->value->birthdate),$_smarty_tpl);?>
</a>
					</div>
					<?php }?>
					<div class="row info-param">
						<a href="#"><i class="md-icon">favorite</i><?php echo $_smarty_tpl->tpl_vars['user_lstatus']->value[$_smarty_tpl->tpl_vars['u']->value->lstatus];?>
</a>
					</div>
					<div class="row info-param">
						<a href="#"><i class="md-icon">people</i><?php ob_start();
echo $_smarty_tpl->tpl_vars['counts']->value->friends;
$_tmp1=ob_get_clean();
echo smarty_function_decline(array('number'=>$_tmp1,'text'=>"%n% дру%o%",'eArray'=>array('г','га','зей')),$_smarty_tpl);?>
</a>
					</div>
					
				</div>
				<div class="clearfix"></div>
			</div>
		
			<div class="wall">
				<div class="send-post-form send-post" id="send-post-form" data-wall-id="<?php echo $_smarty_tpl->tpl_vars['u']->value->id;?>
">
			        <div class="avatar middle">
			            <?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['oUser']->value),$_smarty_tpl);?>

			        </div>
			        <div class="send-form-holder">
			        	<div style="position: relative">
					        <div onkeydown="onCtrlEnter(event, wall.sendPost)" onpaste="wall.postChanged(this)" class="send-form-area" contenteditable="true" placeholder="Что нового?" id="send-post-content"></div>
					        <div class="buttons">
					        	<button class="fl-r attach-button icon icon-attach" onclick="wall.showAttachMenu()">
					        		<span class="count"></span>
					        	</button>
					            <button class="fl-r send-button icon icon-paper-plane"></button>
					            <!--
					            <label for="attach-input" >
					            	<span class="count"></span>
					        	</label>
					        	<input type="file" id="attach-input" class="attach-input" multiple="multiple">
					        	-->
					        	<ul class="actions-menu actions-attach user-menu loaded">
					        		<li><a href="javascript:void(0)" onclick="wall.showAttachPhoto();"><i class="icon icon-flag"></i><span>Фотографию</span></a></li>
					        		<li><a href="javascript:void(0)"><i class="icon icon-flag"></i><span>Аудиозапись</span></a></li>
					        	</ul>
					        </div>
				        </div>
				        <div class="form-attaches">
				        	<div class="photos-choose-rows"></div>
				        </div>
			        </div>
			    </div>

			    <div class="thumb user-tabs">
			    	<ul>
			    		<li><a href="javascript:void(0)" class="user-tab user-tab-sel" data-type="wall" onclick="Page.loadPageSect('<?php echo $_smarty_tpl->tpl_vars['u']->value->id;?>
', 'wall');">Новости <span><?php echo $_smarty_tpl->tpl_vars['counts']->value->posts;?>
</span></a></li>
			    		<li><a href="javascript:void(0)" class="user-tab" data-type="friends" onclick="Page.loadPageSect('<?php echo $_smarty_tpl->tpl_vars['u']->value->id;?>
', 'friends');">Друзья <span><?php echo $_smarty_tpl->tpl_vars['counts']->value->friends;?>
</span></a></li>
			    		<li><a href="javascript:void(0)" class="user-tab" data-type="albums" onclick="Page.loadPageSect('<?php echo $_smarty_tpl->tpl_vars['u']->value->id;?>
', 'albums');">Фотоальбомы <span><?php echo $_smarty_tpl->tpl_vars['counts']->value->albums;?>
</span></a></li>
			    	</ul>
			    </div>

				<section data-page-info="section">
					<div class="wall-posts">
						<?php
$_from = $_smarty_tpl->tpl_vars['wall']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_post_0_saved_item = isset($_smarty_tpl->tpl_vars['post']) ? $_smarty_tpl->tpl_vars['post'] : false;
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$__foreach_post_0_saved_local_item = $_smarty_tpl->tpl_vars['post'];
?>
							<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:wall/post.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('post'=>$_smarty_tpl->tpl_vars['post']->value), 0, true);
?>

						<?php
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_local_item;
}
if ($__foreach_post_0_saved_item) {
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_item;
}
?>
						<div class="nav"><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
@1?page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
"></a></div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div><?php }
}
