<?php
/* Smarty version 3.1.29, created on 2020-01-09 21:25:18
  from "/srv/slto.ru/www/application/themes/Social/sidebar/dialogs.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e17700f003681_93803142',
  'file_dependency' => 
  array (
    'e2c22ea662f27be5d4719d5bb2754865d19c81c9' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/sidebar/dialogs.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e17700f003681_93803142 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<div class="users-list">
    <div class="simple-scrollbar">
        <ul>
            <?php if ($_smarty_tpl->tpl_vars['dialogs']->value) {?>
            <li>
                <ul class="list">
                    <?php
$_from = $_smarty_tpl->tpl_vars['dialogs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_0_saved_item = isset($_smarty_tpl->tpl_vars['d']) ? $_smarty_tpl->tpl_vars['d'] : false;
$__foreach_d_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['d'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['d']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['d']->value) {
$_smarty_tpl->tpl_vars['d']->_loop = true;
$__foreach_d_0_saved_local_item = $_smarty_tpl->tpl_vars['d'];
?>
                    <li class="user-item dialog-item">
                        <a href="/@<?php echo $_smarty_tpl->tpl_vars['d']->value->user_id;?>
" class="avatar middle" data-type="load">
                            <?php echo smarty_function_get_avatar(array('u_av'=>$_smarty_tpl->tpl_vars['d']->value->avatar,'u_id'=>$_smarty_tpl->tpl_vars['d']->value->user_id),$_smarty_tpl);?>

                        </a>
                        <a href="/im/<?php echo $_smarty_tpl->tpl_vars['d']->value->user_id;?>
" class="user <?php if (($_smarty_tpl->tpl_vars['d']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?>" data-type="load">
                            <span class="name"><?php echo $_smarty_tpl->tpl_vars['d']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['d']->value->lname;?>
</span>
                            <p><?php if ($_smarty_tpl->tpl_vars['d']->value->from_user_id == $_smarty_tpl->tpl_vars['oUser']->value->id) {?>Вы:<?php }?> <?php echo $_smarty_tpl->tpl_vars['d']->value->text;?>
</p>
                        </a>
                        <div class="user-button">
                            <button class="md-icon">textsms</button>
                        </div>
                    </li>
                    <?php
$_smarty_tpl->tpl_vars['d'] = $__foreach_d_0_saved_local_item;
}
if ($__foreach_d_0_saved_item) {
$_smarty_tpl->tpl_vars['d'] = $__foreach_d_0_saved_item;
}
if ($__foreach_d_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_d_0_saved_key;
}
?>
                </ul>
            </li>
            <?php }?>
        </ul>
    </div>
</div><?php }
}
