function photo(){
    var self = {};
    self.crop = false;
    self.close_handler = false;
    self.upload_handler = false;

    self.albums = function(id, type, e){
        if(type){
            nav.load(setGet('albums', id), e, true);
        }else{
            nav.load(delGet('albums'), e, true);
            popup.remove('albums');
        }
    }
    self.album = function(id, type, e){
        if(type){
            nav.load(setGet('album', id), e, true);
        }else{
            popup.bind = $(document).off('click', '.popup_bg');
            nav.load(delGet('album'), e, true);
            popup.remove('album');
        }
    }
    self.photo = function(id, type, e){
        if(type){
            nav.load(setGet('photo', id), e, true);
        }else{
            popup.bind = $(document).off('click', '.popup_bg');
            nav.load(delGet('photo'), e, true);
            if($('body').width()<=480){
                $(".app-bar").animate({ bottom: '0px' }, 200);
            }
            popup.remove('photo');
        }
    }

    self.photoSet = function(id, type, e){
        var postData = {};
        $.post('ajax/photo/'+id+'/set/'+type, postData, function(data){
            data = eval('('+data+')');
            if(data.response){
                switch(type){
                    case 'cover':
                        self.photo(id, false, e);
                    break;
                }
            }
        })
    }

    self.setAvatar = function(result){
        if(!result.photo_id)
            return false;

        $.post('/ajax/photo/setAvatar', {photo_id: result.photo_id}, function(data){
            data = eval('('+data+')');
            if(data.response){
                nav.load(window.location.href, event, true);
            }
        })
        popup.hide('avatar_upload');
    }

    self.like = function(id, type, event){
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/ajax/photo/like/'+id+'/'+type,
            data: {},
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    var likes = $("[data-photo="+id+"] span.like-action"),
                        dislikes = $("[data-photo="+id+"] span.dislike-action"),
                        rating = $("[data-photo="+id+"] b.rating"),
                        likes_count = data.response['likes_count'],
                        rating_count = data.response['rating_count'],
                        dislikes_count = data.response['dislikes_count'];
                    rating.html(rating_count);
                    
                    if(type == 0){
                        likes.addClass('active');
                        dislikes.removeClass('active');
                    }
                    if(type == 1){
                        likes.removeClass('active');
                        dislikes.addClass('active');
                    }

                    if('remove' in data){
                        likes.removeClass('active');
                        dislikes.removeClass('active');
                    }
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };    

    self.answer = 0;
    self.setAnswer = function(comment_id, photo_id, e){
        self.answer = comment_id;
        self.showCommentForm(photo_id, e);

        return false;
    };

    self.comment = function(id){
        var text = $(".photo[data-id="+id+"] .send_form .field").html();
        var data = {text: text};
        if(self.answer)
            data.answer = self.answer;
        $.ajax({
            type: 'POST',
            url: '/photo/'+id+'/comment',
            data: data,
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    $(".photo[data-id="+id+"] .comments").append(data.response['html']);
                    $(".photo[data-id="+id+"] .send_form .field").html("");
                    $("abbr.timeago").timeago();
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };
    self.showCommentForm = function(id, e){
        e.preventDefault();
        $(".photo[data-id="+id+"] .send_holder").show();
        return false;
    }
    self.showCommentFormButtons = function(id, e){
        e.preventDefault();
        $(".photo[data-id="+id+"] .send_comment .buttons").show();
        return false;
    }
    self.hideCommentFormButtons = function(element, e){
        var id = $(element).attr('data-id');
        $('div').on('click', function(e){
            if(!$(".photo[data-id="+id+"]").is(':hover')){
                $(".photo[data-id="+id+"] .send_holder").hide();
            }
        })
        return false;
    }

    self.upload_popup = function(callback, album, e){
        e.preventDefault();

        self.upload_handler = $(document).off('click', '#avatar_upload form[data-type="upload"]');

        popup.show('photo_upload/avatar', {album: album});

        self.upload_handler = $('#avatar_upload form[data-type="upload"]').bind('submit', function(e){
            e.preventDefault();

            callback(123);
        });

    }

    self.crop_popup = function(callback, photo_id, e){
        e.preventDefault();
        //self.upload_handler = $(document).off('click', '#avatar_crop');

        popup.show('avatar_crop', {photo_id: photo_id}, function(params){

            var target = $(".avatar-crop img");

            //$(document).on('load', target, function(){
                var x1 = target.width() / 2 - 100;
                var x2 = target.width() / 2 + 100;
                var y1 = target.height() / 2 - 100;
                var y2 = target.height() / 2 + 100;

                var x1 = 0;
                var x2 = 900;

                target.Jcrop({
                    aspectRatio: 1/1,
                    minSize:     [ 200, 200 ],
                    onChange:    self.crop_change,
                    onSelect:    self.crop_change,
                    setSelect:   [x1, y1, x2, y2],
                },function(){       
                    self.crop = this;       
                });
            //})
        });
    }

    self.crop_change = function(c){
        self.crop = c;
    }

    self.avatar_crop = function(){
        if(!self.crop)
            return false;

        var target = $(".avatar-crop img");

        self.crop.target_w = target.width();

        $.post('ajax/user/avatar_crop', self.crop, function(data) {
            data = JSON.parse(data);
            
            if(data.response){
                popup.remove('avatar_crop');

                window.location.reload();
            }
        })
    }

    return self;
}
var photo = new photo();