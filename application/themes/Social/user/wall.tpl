<div class="wall-posts">
    {foreach from=$wall item=post}
        {include file="wall/post.tpl" post=$post}
    {/foreach}
    <div class="nav"><a href="{$base_url}ajax/user/sect/wall?id={$uid}&page={$page}"></a></div>
</div>