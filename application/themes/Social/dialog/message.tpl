<div class="message-holder">
    <a href="/@{$message->user_id}" data-type="load" class="avatar middle">
        {get_avatar u_av=$message->avatar u_id=$message->user_id}
    </a>
    <div class="message {if $message->user_id == $oUser->id && isset($my)}my{/if}">
        <div class="message-row">
            {$message->text}
            <div class="clearfix"></div>
        </div>
    </div>
    <abbr title="{$message->date|date_format:"%M:%S"}" class="time">{$message->date|date_format:"%H:%M"}</abbr>
</div>