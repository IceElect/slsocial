<?php
/* Smarty version 3.1.29, created on 2020-01-08 19:32:45
  from "/srv/slto.ru/www/application/themes/Social/popup/avatar_upload.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e16042d3f6796_91692390',
  'file_dependency' => 
  array (
    '2c2a86a2fbbffd6be3ade6fb30a0a9024af5be67' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/popup/avatar_upload.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e16042d3f6796_91692390 ($_smarty_tpl) {
?>
<div class="popup_bg"></div>
<div class="popup block" data-id="avatar_upload">
    <div class="popup_header_holder">
        <div class="popup_header module_title">
            Аватар профиля <button class="md-icon close" onclick="popup.hide('avatar_upload');">close</button>
        </div>
    </div>
    <div class="popup_content module_content">
        <b>Ограничения</b>
        <ul>
            <li>Файл должен иметь размно не более 20 МБ и расширения PNG, JPEG или GIF</li>
        </ul>

        <div class="clearfix"></div>

        <div class="file-upload">
            <input id="file" type="file" style="display: none;" onchange="upload.onFile('avatar', '<?php echo $_smarty_tpl->tpl_vars['album']->value;?>
', this.files);" size="28" accept="image/jpeg,image/png,image/gif,image/heic,image/heif">
            <label for="file" class="button">Загрузить файл</label>
        </div>
        
        <div class="load-info">
            <span id="message"></span>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!--<?php echo '<script'; ?>
 src="/views/Social/js/drop_load.js"><?php echo '</script'; ?>
>--><?php }
}
