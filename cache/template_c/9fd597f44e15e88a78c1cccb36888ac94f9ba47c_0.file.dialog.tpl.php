<?php
/* Smarty version 3.1.29, created on 2020-01-09 21:25:22
  from "/srv/slto.ru/www/application/themes/Social/dialog.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e17701271f333_73614689',
  'file_dependency' => 
  array (
    '9fd597f44e15e88a78c1cccb36888ac94f9ba47c' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/dialog.tpl',
      1 => 1576341909,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e17701271f333_73614689 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<div class="dialog-page" data-dialog-id="<?php echo $_smarty_tpl->tpl_vars['dialog']->value->id;?>
">
    <div class="thumb dialog-page-inner">
        <div class="messages-holder">
            <?php
$_from = $_smarty_tpl->tpl_vars['messages']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_message_0_saved_item = isset($_smarty_tpl->tpl_vars['message']) ? $_smarty_tpl->tpl_vars['message'] : false;
$__foreach_message_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['message'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['message']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['message']->value) {
$_smarty_tpl->tpl_vars['message']->_loop = true;
$__foreach_message_0_saved_local_item = $_smarty_tpl->tpl_vars['message'];
?>
                <?php if ((!isset($_smarty_tpl->tpl_vars['u_id']->value))) {?> <?php $_smarty_tpl->tpl_vars['u_id'] = new Smarty_Variable($_smarty_tpl->tpl_vars['message']->value->user_id, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'u_id', 0);?> <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['i']->value > 0) {?>
                    <?php if ($_smarty_tpl->tpl_vars['u_id']->value != $_smarty_tpl->tpl_vars['message']->value->user_id) {?>
                        </div>
                            <abbr title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['message']->value->date,"%M:%S");?>
" class="time"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['message']->value->date,"%H:%M");?>
</abbr>
                        </div>
                        <div class="message-holder">
                            <a href="/@<?php echo $_smarty_tpl->tpl_vars['message']->value->user_id;?>
" data-type="load" class="avatar middle">
                                <?php echo smarty_function_get_avatar(array('u_av'=>$_smarty_tpl->tpl_vars['message']->value->avatar,'u_id'=>$_smarty_tpl->tpl_vars['message']->value->user_id),$_smarty_tpl);?>

                            </a>
                            <div class="message <?php if ($_smarty_tpl->tpl_vars['message']->value->user_id == $_smarty_tpl->tpl_vars['oUser']->value->id) {?>my<?php }?>">
                    <?php }?>
                <?php } else { ?>
                    <div class="message-holder">
                        <a href="/@<?php echo $_smarty_tpl->tpl_vars['message']->value->user_id;?>
" data-type="load" class="avatar middle">
                            <?php echo smarty_function_get_avatar(array('u_av'=>$_smarty_tpl->tpl_vars['message']->value->avatar,'u_id'=>$_smarty_tpl->tpl_vars['message']->value->user_id),$_smarty_tpl);?>

                        </a>
                        <div class="message <?php if ($_smarty_tpl->tpl_vars['message']->value->user_id == $_smarty_tpl->tpl_vars['oUser']->value->id) {?>my<?php }?>">
                <?php }?>

                <div class="message-row">
                    <?php echo $_smarty_tpl->tpl_vars['message']->value->text;?>

                    <div class="clearfix"></div>
                </div>

                <?php if ($_smarty_tpl->tpl_vars['i']->value == count($_smarty_tpl->tpl_vars['messages']->value)-1) {?>
                        </div>
                        <abbr title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['message']->value->date,"%M:%S");?>
" class="time"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['message']->value->date,"%H:%M");?>
</abbr>
                    </div>
                <?php }?>

                <?php $_smarty_tpl->tpl_vars['u_id'] = new Smarty_Variable($_smarty_tpl->tpl_vars['message']->value->user_id, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'u_id', 0);?>

            <?php
$_smarty_tpl->tpl_vars['message'] = $__foreach_message_0_saved_local_item;
}
if ($__foreach_message_0_saved_item) {
$_smarty_tpl->tpl_vars['message'] = $__foreach_message_0_saved_item;
}
if ($__foreach_message_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_message_0_saved_key;
}
?>
        </div>
    </div>
    <div class="send-post-form send-post send-message" id="send-post-form" data-dialog-id="<?php echo $_smarty_tpl->tpl_vars['dialog']->value->id;?>
">
        <div class="buttons">
            <button class="fl-l icon attach-button icon-attach"></button>
        </div>
        <div class="send-form-area" contenteditable="true" placeholder="Введите сообщение" id="send-post-content"></div>
        <div class="buttons">
            <button class="fl-r send-button icon icon-paper-plane"></button>
            <!--
            <label for="attach-input" class="fl-r attach-button icon icon-attach">
                <span class="count"></span>
            </label>
            <input type="file" id="attach-input" class="attach-input" multiple="multiple">
            -->
        </div>
    </div>
</div><?php }
}
