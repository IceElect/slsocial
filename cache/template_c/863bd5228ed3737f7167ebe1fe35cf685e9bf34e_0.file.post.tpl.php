<?php
/* Smarty version 3.1.29, created on 2020-01-05 16:28:57
  from "/srv/slto.ru/www/application/themes/Social/wall/post.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e11e49902b380_11957382',
  'file_dependency' => 
  array (
    '863bd5228ed3737f7167ebe1fe35cf685e9bf34e' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/wall/post.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:wall/comment.tpl' => 1,
  ),
),false)) {
function content_5e11e49902b380_11957382 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
if (!is_callable('smarty_modifier_date_format')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
if (!is_callable('smarty_function_get_comments')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_comments.php';
?>
<div class="post-holder" data-post="<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
">
    <div class="post">
        <div class="post-header">
            <a href="/@<?php echo $_smarty_tpl->tpl_vars['post']->value->author_id;?>
" class="user-avatar avatar middle" data-type="load">
				<?php echo smarty_function_get_avatar(array('u_id'=>$_smarty_tpl->tpl_vars['post']->value->author_id,'u_av'=>$_smarty_tpl->tpl_vars['post']->value->avatar),$_smarty_tpl);?>

			</a>
            <div class="info-holder <?php if (($_smarty_tpl->tpl_vars['post']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?>">
                <a href="/@<?php echo $_smarty_tpl->tpl_vars['post']->value->author_id;?>
" class="name" data-type="load"><?php echo $_smarty_tpl->tpl_vars['post']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['post']->value->lname;?>
</a>
                <abbr title='<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%Y-%m-%d %H:%M:%S");?>
' class="time"></abbr>
            </div>
            <div class="spacer"></div>
            <div class="actions">
                <button class="icon fa icon-dot-3" data-type="actions" data-action="post" data-id="<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
"></button>
                <ul class="actions-menu user-menu">
                    <li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="post-content">
            <?php echo $_smarty_tpl->tpl_vars['post']->value->content;?>


            <?php $_smarty_tpl->tpl_vars['attach'] = new Smarty_Variable($_smarty_tpl->tpl_vars['functions']->value->parse_attach($_smarty_tpl->tpl_vars['post']->value->attach), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'attach', 0);?>

            <div class="photos-choose-rows">
                <?php
$_from = $_smarty_tpl->tpl_vars['attach']->value['photo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_photo_0_saved_item = isset($_smarty_tpl->tpl_vars['photo']) ? $_smarty_tpl->tpl_vars['photo'] : false;
$__foreach_photo_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['photo']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['photo']->value) {
$_smarty_tpl->tpl_vars['photo']->_loop = true;
$__foreach_photo_0_saved_local_item = $_smarty_tpl->tpl_vars['photo'];
?>
                <a class="photo-choose-row" onclick="return photo.photo('<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
', 1, event);">
                    <div class="photo-row-img" style="background-image: url('/ajax/photo/view/<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
');"></div>
                </a>
                <?php
$_smarty_tpl->tpl_vars['photo'] = $__foreach_photo_0_saved_local_item;
}
if ($__foreach_photo_0_saved_item) {
$_smarty_tpl->tpl_vars['photo'] = $__foreach_photo_0_saved_item;
}
if ($__foreach_photo_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_photo_0_saved_key;
}
?>
            </div>
        </div>
        <div class="actions">
            <div class="action">
                
                

                <span class="like-action <?php if ($_smarty_tpl->tpl_vars['post']->value->is_liked) {?>active<?php }?>" onclick="wall.like(<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
, 0, event)">
                    <i class="md-icon">thumb_up</i>
                </span>

                <b class="action-count rating">
                    <?php if (($_smarty_tpl->tpl_vars['post']->value->likes_count > 0 || $_smarty_tpl->tpl_vars['post']->value->dislikes_count > 0)) {
echo ($_smarty_tpl->tpl_vars['post']->value->likes_count-$_smarty_tpl->tpl_vars['post']->value->dislikes_count);
}?>
                </b>
                
                <span class="dislike-action <?php if ($_smarty_tpl->tpl_vars['post']->value->is_disliked) {?>active<?php }?>" onclick="wall.like(<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
, 1, event)">
                    <i class="md-icon">thumb_down</i>
                </span>
            </div>

            <!--
            <div class="action share <?php if (false) {?>active<?php }?>">
                 
                <i class="md-icon">share</i>
                 
                <?php if (false) {?>
                    <b class="action-count"></b>
                <?php }?>
            </div>
            -->

            <div class="spacer"></div>

            <div class="action action-comment">
                <span>
                    <b class="action-count"><?php if ($_smarty_tpl->tpl_vars['post']->value->comments_count > 0) {
echo $_smarty_tpl->tpl_vars['post']->value->comments_count;
}?></b>
                    <i class="md-icon">mode_comment</i>
                </span>
            </div>
        </div>
        <?php ob_start();
echo $_smarty_tpl->tpl_vars['post']->value->id;
$_tmp1=ob_get_clean();
echo smarty_function_get_comments(array('id'=>$_tmp1),$_smarty_tpl);?>

        <div class="comments-list <?php if (count($_smarty_tpl->tpl_vars['comments']->value) == 0) {?>empty<?php }?>">
            <?php
$_from = $_smarty_tpl->tpl_vars['comments']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_comment_1_saved_item = isset($_smarty_tpl->tpl_vars['comment']) ? $_smarty_tpl->tpl_vars['comment'] : false;
$_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['comment']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
$__foreach_comment_1_saved_local_item = $_smarty_tpl->tpl_vars['comment'];
?>
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:wall/comment.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('post'=>$_smarty_tpl->tpl_vars['comment']->value), 0, true);
?>

            <?php
$_smarty_tpl->tpl_vars['comment'] = $__foreach_comment_1_saved_local_item;
}
if ($__foreach_comment_1_saved_item) {
$_smarty_tpl->tpl_vars['comment'] = $__foreach_comment_1_saved_item;
}
?>
        </div>
    </div>
    <div class="send-comment">
        <div class="send-post-form send-comment-form" data-post-id="<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
">
            <div class="avatar middle">
                <?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['oUser']->value),$_smarty_tpl);?>

            </div>
            <div onkeypress="onCtrlEnter(event, this);" class="send-form-area" contenteditable="true" placeholder="Написать комментарий"></div>
            <div class="buttons">
                <button class="fl-r send-button icon icon-paper-plane"></button>
            </div>
        </div>
    </div>
</div>
<?php }
}
