var __debugMode = false;

function rand(mi, ma) { return Math.random() * (ma - mi + 1) + mi; }
function irand(mi, ma) { return Math.floor(rand(mi, ma)); }
function isUndefined(obj) { return typeof obj === 'undefined' };
function isFunction(obj) {return obj && Object.prototype.toString.call(obj) === '[object Function]'; }
function isArray(obj) { return Object.prototype.toString.call(obj) === '[object Array]'; }
function isString(obj) { return typeof obj === 'string'; }
function isObject(obj) { return Object.prototype.toString.call(obj) === '[object Object]'; }
function isEmpty(o) { if(Object.prototype.toString.call(o) !== '[object Object]') {return false;} for(var i in o){ if(o.hasOwnProperty(i)){return false;} } return true; }
function nowTime() { return +new Date; }

function error(error_text){
	alert(error_text);
}

function clone(obj, req) {
  var newObj = !isObject(obj) && typeof obj.length !== 'undefined' ? [] : {};
  for (var i in obj) {
    //if (/webkit/i.test(_ua) && (i == 'layerX' || i == 'layerY' || i == 'webkitMovementX' || i == 'webkitMovementY')) continue;
    if (req && typeof(obj[i]) === 'object' && i !== 'prototype' && obj[i] !== null) {
      newObj[i] = clone(obj[i]);
    } else {
      newObj[i] = obj[i];
    }

  }
  return newObj;
}

function _eventHandle(event) {
  event = normEvent(event);

  var handlers = data(this, 'events');
  if (!handlers || typeof(event.type) != 'string' || !handlers[event.type] || !handlers[event.type].length) {
    return;
  }

  var eventHandlers = (handlers[event.type] || []).slice();
  for (var i in eventHandlers) {
    if (event.type == 'mouseover' || event.type == 'mouseout') {
      var parent = event.relatedElement;
      while (parent && parent != this) {
        parent = parent.parentNode;
      }
      if (parent == this) {
        continue
      }
    }
    var ret = eventHandlers[i].apply(this, arguments);
    if (ret === false || ret === -1) {
      cancelEvent(event);
    }
    if (ret === -1) {
      return false;
    }
  }
}

function normEvent(event) {
  event = event || window.event;

  var originalEvent = event;
  event = clone(originalEvent);
  event.originalEvent = originalEvent;

  if (!event.target) {
    event.target = event.srcElement || document;
  }

  // check if target is a textnode (safari)
  if (event.target.nodeType == 3) {
    event.target = event.target.parentNode;
  }

  if (!event.relatedTarget && event.fromElement) {
    event.relatedTarget = event.fromElement == event.target;
  }

  if (event.pageX == null && event.clientX != null) {
    var doc = document.documentElement, body = bodyNode;
    event.pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc.clientLeft || 0);
    event.pageY = event.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc.clientTop || 0);
  }

  if (!event.which && ((event.charCode || event.charCode === 0) ? event.charCode : event.keyCode)) {
    event.which = event.charCode || event.keyCode;
  }

  if (!event.metaKey && event.ctrlKey) {
    event.metaKey = event.ctrlKey;
  } else if (!event.ctrlKey && event.metaKey && browser.mac) {
    event.ctrlKey = event.metaKey;
  }

  // click: 1 == left; 2 == middle; 3 == right
  if (!event.which && event.button) {
    event.which = (event.button & 1 ? 1 : ( event.button & 2 ? 3 : ( event.button & 4 ? 2 : 0 ) ));
  }

  return event;
}

var vkExpand = 'SL1', vkUUID = 0, cache = {};

function data(elem, name, data) {
  if (!elem) return false;
  var id = elem[vkExpand], undefined;
  if (!id) {
    id = elem[vkExpand] = ++vkUUID;
  }

  if (data !== undefined) {
    if (!cache[id]) {
      cache[id] = {};
      if (__debugMode) cache[id].__elem = elem;
    }
    cache[id][name] = data;
  }

  return name ? cache[id] && cache[id][name] : id;
}
function addEvent(elem, types, handler, custom, context, useCapture) {
  elem = elem;
  if (!elem || elem.nodeType == 3 || elem.nodeType == 8) { // 3 - Node.TEXT_NODE, 8 - Node.COMMENT_NODE
	return;
  }
  console.log(elem);

  var realHandler = context ? function() {
	var newHandler = function(e) {
	  var prevData = e.data;
	  e.data = context;
	  var ret = handler.apply(this, [e]);
	  e.data = prevData;
	  return ret;
	}
	newHandler.handler = handler;
	return newHandler;
  }() : handler;

  // For IE
  if (elem.setInterval && elem != window) elem = window;

  var events = data(elem, 'events') || data(elem, 'events', {}),
	  handle = data(elem, 'handle') || data(elem, 'handle', function() {
		_eventHandle.apply(arguments.callee.elem, arguments);
	  });
  // to prevent a memory leak
  handle.elem = elem;

  $.each(types.split(/\s+/), function(index, type) {
	if (!events[type]) {
	  events[type] = [];
	  if (!custom && elem.addEventListener) {
		elem.addEventListener(type, handle, useCapture);
	  } else if (!custom && elem.attachEvent) {
		elem.attachEvent('on' + type, handle);
	  }
	}
	events[type].push(realHandler);
  });

  elem = null;
}
function removeEvent(elem, types, handler, useCapture) {
  if (typeof useCapture === 'undefined') {
	useCapture = false;
  }

  elem = $(elem);
  if (!elem) return;
  var events = data(elem, 'events');
  if (!events) return;
  if (typeof (types) != 'string') {
	for (var i in events) {
	  removeEvent(elem, i);
	}
	return;
  }

  each(types.split(/\s+/), function(index, type) {
	if (!isArray(events[type])) return;
	var l = events[type].length;
	if (isFunction(handler)) {
	  for (var i = l - 1; i >= 0; i--) {
		if (events[type][i] && (events[type][i] === handler || events[type][i].handler === handler)) {
		  events[type].splice(i, 1);
		  l--;
		  break;
		}
	  }
	} else {
	  for (var i = 0; i < l; i++) {
		delete events[type][i];
	  }
	  l = 0;
	}
	if (!l) {
	  if (elem.removeEventListener) {
		elem.removeEventListener(type, data(elem, 'handle'), useCapture);
	  } else if (elem.detachEvent) {
		elem.detachEvent('on' + type, data(elem, 'handle'));
	  }
	  delete events[type];
	}
  });
  if (isEmpty(events)) {
	removeData(elem, 'events')
	removeData(elem, 'handle')
  }
}
function triggerEvent(elem, type, ev, now) {
  elem = $(elem);
  var handle = data(elem, 'handle');
  if (handle) {
	var f = function() {
	  handle.call(elem, extend((ev || {}), {type: type, target: elem}))
	};
	now ? f() : setTimeout(f, 0);
  }
}
function cancelEvent(event) {
  event = (event || window.event);
  if (!event) return false;
  while (event.originalEvent) {
	event = event.originalEvent;
  }
  if (event.preventDefault) event.preventDefault();
  if (event.stopPropagation) event.stopPropagation();
  if (event.stopImmediatePropagation) event.stopImmediatePropagation();
  event.cancelBubble = true;
  event.returnValue = false;
  return false;
}
function stopEvent(event) {
  event = (event || window.event);
  if (!event) return false;
  while (event.originalEvent) {
	event = event.originalEvent;
  }
  if (event.stopPropagation) event.stopPropagation();
  event.cancelBubble = true;
  return false;
}

counts = {};
function handlePageCount(id, value, lnk, add) {
  var v = parseInt(value);
  if (counts === undefined) counts = {};
  if (counts[id] === v) return;
  counts[id] = v;

  var e = $('notify_'+id);
  if (e) {

    if (add && lnk) {
      var toAdd = (v > 0 && add) ? ('?' + add) : '';
      //e.firstChild.href = '/' + lnk + toAdd;
    }
  }
  if (v >= 0 || !hc) {
    toggle(e, v >= 0);
  }
}

function toggle(elem, v) {
  if (v === undefined) {
    v = !isVisible(elem);
  }
  if (v) {
    show(elem);
  } else {
    hide(elem);
  }
  return v
}

setInterval(function(){
  $.each(counts, function(i, value){
    console.log('count '+i+' '+value);
    counts[i]--;
    if (counts[i] < 0) {
      toggle('#notify_over_'+i, counts[i] >= 0);
      delete counts[i];
    }
  })
}, 1000);

function show(elem) {
  var l = arguments.length;
  if (l > 1) {
    for (var i = 0; i < l; i++) {
      show(arguments[i]);
    }
    return;
  }

  elem = $(elem);
  if (!elem) return;

  var old = elem.olddisplay;
  var newStyle = 'block';
  var tag = $(elem).prop("tagName");
  //var tag = $(elem).prop("tagName").toLowerCase();
  elem.css('display', old);

  if ($(elem).css('display') !== 'none') {
    return;
  }

  if (hasClass(elem, 'inline') || hasClass(elem, '_inline')) {
    newStyle = 'inline';
  } else if (hasClass(elem, '_inline_block')) {
    newStyle = 'inline-block';
  } else if (tag === 'tr' && !browser.msie) {
    newStyle = 'table-row';
  } else if (tag === 'table' && !browser.msie) {
    newStyle = 'table';
  } else {
    newStyle = 'block';
  }
  elem.olddisplay = newStyle;
  elem.css('display', newStyle);
}

function hide(elem) {
  var l = arguments.length;
  if (l > 1) {
    for (var i = 0; i < l; i++) {
      hide(arguments[i]);
    }
    return;
  }

  elem = $(elem);
  if (!elem) return;

  var display = $(elem).css('display');
  elem.olddisplay = ((display != 'none') ? display : '');
  elem.css('display', 'none');
}

window.whitespaceRegex = /[\t\r\n\f]/g;
function hasClass(obj, name) {
  obj = $(obj);
  if (obj &&
    obj.nodeType === 1 &&
    (" " + obj.className + " ").replace(window.whitespaceRegex, " ").indexOf(" " + name + " ") >= 0) {
    return true;
  }

  return false;
}

function isVisible(elem) {
  elem = ge(elem);
  if (!elem || !elem.style) return false;
  return getStyle(elem, 'display') != 'none';
}

jQuery(document).on('submit', 'form[data-type=ajax]', function(e){
	e.preventDefault();
	var $that = jQuery(this),
	formData = new FormData($that.get(0));
	formData.append('submit', true);
	jQuery.ajax({
	  url: $that.attr('action'),
	  type: $that.attr('method'),
	  contentType: false,
	  processData: false,
	  data: formData,
	  dataType: 'html',
	  beforeSend: function(){
		  jQuery("body").addClass('loading');
		  $that.find('.form_result').html('<i class="fa fa-spin fa-refresh"></i>');
	  },
	  success: function(data){
		jQuery("body").removeClass('loading');
		jQuery(".error").html("");
		var data = eval('('+data+')');
		console.log(data);
		if(data.response){
			if($that.attr('data-act') == 'pedit'){
				$that.find('.form-row').removeClass('has-error');
				if(data.errors.length > 0){
					$.each(data.errors, function(key, value){
						$that.find('.form-row[data-field="'+value.field.field+'"]').addClass('has-error');

					})
          $(".tabs .tab").removeClass('current');
          $(".tabs .tab[aria-controls='"+data.errors[0].tab.id+"']").click();
          alert(data.errors[0].error);
				}
			}
			if('text' in data){
				if($that.attr('data-act') == 'login')
				  window.location.href = data.text;
				$that.find('.form_result').html(data.text);
			}
		}else{
			if($that.attr('data-act') == 'login'){
			  jQuery(".error").html(data.error);
			}else{
			  error(data.error);
			}
		}
	  }
	});
});

$(document).on('click', '.tabs .tab', function(e){
	e.preventDefault();

	var id = $(this).attr('aria-controls'),
		tabs = $(this).parents('.tabs');
	
	tabs.find('.current').removeClass('current');
	$(this).addClass('current');

	$(".tab-content.current").removeClass('current');
	$(".tab-content#"+id).addClass('current');
})