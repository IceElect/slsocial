<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_dialog extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('dialog_model', 'Dialog');

        $this->user_id = $this->session->userdata('user_id');
    }

    function _need_log(){
        if(!$this->user->is_logged()){
            $this->response['error'] = 10;
            echo json_encode($this->response);
            exit;
        }
    }

    public function index()
    {
        $this->frontend->view('welcome');
    }

    function filter($string)
    {
        $string = trim($string);
        $string = preg_replace('/(\<div\>\<br\>\<\/div\>)+/', "\r\n", $string);
        return $string;
    }

    function send($dialog_id){
        $this->_need_log();

        $aData = $this->input->post();
        $this->response = array('result' => false);

        if(isset($aData['content'])){
            if(!empty($aData['content'])){

                $content = $this->filter($aData['content']);

                $data = array(
                    'user_id' => $this->oUser->id,
                    'dialog_id' => $dialog_id,
                    'text' => $content,
                    'date' => time(),
                    'status' => 0,
                );
                $id = $this->Dialog->send($data);

                if($id){
                    $data['id'] = $id;
                    $tpl_data = $data;
                    $tpl_data['avatar_file'] = $this->oUser->avatar_file;
                    $tpl_data = (object) $tpl_data;
                    $this->my_smarty->assign('message', $tpl_data);
                    $data['message_html'] = $this->frontend->fetch('dialog/message');
                    $data['message_html'] = $data['message_html']['data'];

                    $users = $this->Dialog->getDialogUsers($dialog_id, $this->oUser->id);

                    $this->load->library('notify');
                    $this->notify->set_user($this->oUser);
                    foreach($users as $user){
                        $this->notify->add('message', $user->user_id, $data, $content, '/dialog'.$dialog_id);
                    }

                    $this->my_smarty->assign('my', true);
                    $data['message_html'] = $this->frontend->fetch('dialog/message');
                    $data['message_html'] = $data['message_html']['data'];

                    $this->response['data'] = $data;
                    $this->response['result'] = true;
                }

            }
        }else{

        }

        echo json_encode($this->response);
    }
}