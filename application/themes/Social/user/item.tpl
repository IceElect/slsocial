<li class="user-item">
	<a href="/@{$user->id}" class="avatar middle" data-type="load">
		{get_avatar u=$user}
	</a>
	<a href="/@{$user->id}" class="user {if ($user->last_action >= ($time - 900))}online{/if}" data-type="load">
		<span class="name">{$user->fname} {$user->lname}</span>
	</a>
	<div class="user-button">
		{if $user->is_friend}
			<button class="md-icon" data-type="actions" data-action="user" data-id="{$user->id}">group</button>
		{/if}
		<button class="md-icon" data-type="actions" data-action="user" data-id="{$user->id}">more_horiz</button>
		<ul class="actions-menu user-menu">
			<li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
		</ul>
	</div>
</li>