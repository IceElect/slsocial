<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Functions
{
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->config =& $this->CI->config;
        $this->uri =& $this->CI->uri;
        $this->my_cache =& $this->CI->my_cache;
        $this->my_smarty =& $this->CI->my_smarty;
        $this->user =& $this->CI->user;

    }

    function parse_attach($attach){
        $response = array('file' => array(), 'photo' => array());

        $attachA = json_decode($attach);
        if(count($attachA) > 0){
            foreach($attachA as $key => $value){
                $response[$value->type][] = $value;
            }
        }

        return $response;
    }
}