<div class="users-list">
    <div class="simple-scrollbar">
        <ul>
            {if $dialogs}
            <li>
                <ul class="list">
                    {foreach from=$dialogs key="i" item="d"}
                    <li class="user-item dialog-item">
                        <a href="/@{$d->user_id}" class="avatar middle" data-type="load">
                            {get_avatar u_av=$d->avatar u_id=$d->user_id}
                        </a>
                        <a href="/im/{$d->user_id}" class="user {if ($d->last_action >= ($time - 900))}online{/if}" data-type="load">
                            <span class="name">{$d->fname} {$d->lname}</span>
                            <p>{if $d->from_user_id == $oUser->id}Вы:{/if} {$d->text}</p>
                        </a>
                        <div class="user-button">
                            <button class="md-icon">textsms</button>
                        </div>
                    </li>
                    {/foreach}
                </ul>
            </li>
            {/if}
        </ul>
    </div>
</div>