<?php
/* Smarty version 3.1.29, created on 2020-01-08 19:32:36
  from "/srv/slto.ru/www/application/themes/Social/default_form_row.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e160424c22209_10297951',
  'file_dependency' => 
  array (
    '8cd519a97c59cfacba129288e0135359b209b676' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/default_form_row.tpl',
      1 => 1576341909,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e160424c22209_10297951 ($_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['field']->value['additional_field'])) {
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable(('custom_').($_smarty_tpl->tpl_vars['field']->value['additional_field']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'v', 0);
if (isset($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['v']->value])) {
$_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'field', null);
$_smarty_tpl->tpl_vars['field']->value['value'] = $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['v']->value];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'field', 0);
}
}
if (($_smarty_tpl->tpl_vars['field']->value['form_show'])) {
if ($_smarty_tpl->tpl_vars['field']->value['type'] == 'hidden') {?><input type="<?php echo $_smarty_tpl->tpl_vars['field']->value['type'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['field']->value['field'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['field']->value['value'];?>
"><?php } else { ?><div class="form-row clearfix <?php if ($_smarty_tpl->tpl_vars['field']->value['error']) {?>has-error<?php }?>" data-field="<?php echo $_smarty_tpl->tpl_vars['field']->value['field'];?>
"> <div class="form-label"> <label for="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php if ((strpos($_smarty_tpl->tpl_vars['field']->value['rules'],'required') !== false)) {
echo $_smarty_tpl->tpl_vars['required_star']->value;
}
echo $_smarty_tpl->tpl_vars['field']->value['title'];?>
</label> </div> <div class="form-input-wrap input-wrap-<?php echo $_smarty_tpl->tpl_vars['field']->value['type'];?>
"><?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['prefix']) || !empty($_smarty_tpl->tpl_vars['field']->value['suffix']))) {?><div class="input-group"><?php }
if ((!empty($_smarty_tpl->tpl_vars['field']->value['prefix']))) {?><span class="input-group-addon" id="basic-addon1"><?php echo $_smarty_tpl->tpl_vars['field']->value['prefix'];?>
</span><?php }?>
        		<<?php echo $_smarty_tpl->tpl_vars['field']->value['tag_name'];?>

        			id="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"
        			<?php
$_from = $_smarty_tpl->tpl_vars['field']->value['attributes'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_0_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$__foreach_value_0_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                        <?php if ($_smarty_tpl->tpl_vars['key']->value !== 'value') {?>
        				    <?php if ($_smarty_tpl->tpl_vars['value']->value !== false) {
echo $_smarty_tpl->tpl_vars['key']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
"<?php }?>
                        <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['key']->value;?>
="<?php if (isset($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {
echo $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value];
} else {
echo $_smarty_tpl->tpl_vars['field']->value['value'];
}?>"
                        <?php }?>
        			<?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_local_item;
}
if ($__foreach_value_0_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_0_saved_item;
}
if ($__foreach_value_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_value_0_saved_key;
}
?>
                    <?php if ($_smarty_tpl->tpl_vars['field']->value['type'] == 'checkbox' && $_smarty_tpl->tpl_vars['field']->value['value'] == '1') {?>checked<?php }?>
        		><?php if ($_smarty_tpl->tpl_vars['field']->value['field'] == 'id') {
echo $_smarty_tpl->tpl_vars['nId']->value;
} elseif ($_smarty_tpl->tpl_vars['field']->value['type'] == 'select') {
if ((!empty($_smarty_tpl->tpl_vars['field']->value['options']))) {
$_from = $_smarty_tpl->tpl_vars['field']->value['options'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_option_1_saved_item = isset($_smarty_tpl->tpl_vars['option']) ? $_smarty_tpl->tpl_vars['option'] : false;
$__foreach_option_1_saved_key = isset($_smarty_tpl->tpl_vars['option_key']) ? $_smarty_tpl->tpl_vars['option_key'] : false;
$_smarty_tpl->tpl_vars['option'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['option_key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['option']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['option_key']->value => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
$__foreach_option_1_saved_local_item = $_smarty_tpl->tpl_vars['option'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['option_key']->value;?>
" <?php if (!empty($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {
if ($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value] == $_smarty_tpl->tpl_vars['option_key']->value) {?> selected <?php }
} else {
if ($_smarty_tpl->tpl_vars['field']->value['value'] == $_smarty_tpl->tpl_vars['option_key']->value) {?> selected <?php }
}?>><?php echo $_smarty_tpl->tpl_vars['option']->value;?>
</option><?php
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_1_saved_local_item;
}
if ($__foreach_option_1_saved_item) {
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_1_saved_item;
}
if ($__foreach_option_1_saved_key) {
$_smarty_tpl->tpl_vars['option_key'] = $__foreach_option_1_saved_key;
}
}
} elseif ($_smarty_tpl->tpl_vars['field']->value['type'] == 'radio') {
if ((!empty($_smarty_tpl->tpl_vars['field']->value['options']))) {
$_from = $_smarty_tpl->tpl_vars['field']->value['options'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_option_2_saved_item = isset($_smarty_tpl->tpl_vars['option']) ? $_smarty_tpl->tpl_vars['option'] : false;
$__foreach_option_2_saved_key = isset($_smarty_tpl->tpl_vars['option_key']) ? $_smarty_tpl->tpl_vars['option_key'] : false;
$_smarty_tpl->tpl_vars['option'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['option_key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['option']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['option_key']->value => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
$__foreach_option_2_saved_local_item = $_smarty_tpl->tpl_vars['option'];
?><input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['field']->value['field'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['option_key']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['field']->value['field'];?>
_<?php echo $_smarty_tpl->tpl_vars['option_key']->value;?>
" <?php if (!empty($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {
if ($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value] == $_smarty_tpl->tpl_vars['option_key']->value) {?> checked <?php }
} else {
if ($_smarty_tpl->tpl_vars['field']->value['value'] == $_smarty_tpl->tpl_vars['option_key']->value) {?> checked <?php }
}?>> <label for="<?php echo $_smarty_tpl->tpl_vars['field']->value['field'];?>
_<?php echo $_smarty_tpl->tpl_vars['option_key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['option']->value;?>
</label><?php
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_2_saved_local_item;
}
if ($__foreach_option_2_saved_item) {
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_2_saved_item;
}
if ($__foreach_option_2_saved_key) {
$_smarty_tpl->tpl_vars['option_key'] = $__foreach_option_2_saved_key;
}
}
} elseif ($_smarty_tpl->tpl_vars['field']->value['type'] == 'checkbox') {?><label for="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">&nbsp;</label><?php } else {
if ($_smarty_tpl->tpl_vars['field']->value['tag_name'] == 'textarea') {
if (isset($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {
echo $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value];
} else {
echo $_smarty_tpl->tpl_vars['field']->value['value'];
}
}
}?></<?php echo $_smarty_tpl->tpl_vars['field']->value['tag_name'];?>
><?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['suffix']))) {?><span class="input-group-addon" id="basic-addon1"><?php echo $_smarty_tpl->tpl_vars['field']->value['suffix'];?>
</span><?php }
if ((!empty($_smarty_tpl->tpl_vars['field']->value['prefix']) || !empty($_smarty_tpl->tpl_vars['field']->value['suffix']))) {?></div><?php }
if ((!empty($_smarty_tpl->tpl_vars['field']->value['description']))) {?><p class="form-text text-muted"><?php echo $_smarty_tpl->tpl_vars['field']->value['description'];?>
</p><?php }?></div><?php if ($_smarty_tpl->tpl_vars['field']->value['multiple']) {
if (isset($_smarty_tpl->tpl_vars['aData']->value['screenshots'])) {
echo '<script'; ?>
>$("input#def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
").fileinput({initialPreview: <?php echo json_encode($_smarty_tpl->tpl_vars['aData']->value['screenshots']['preview']);?>
,initialPreviewAsData: true,overwriteInitial: false,deleteUrl: '<?php echo $_smarty_tpl->tpl_vars['aConf']->value['base_url'];
echo $_smarty_tpl->tpl_vars['aConf']->value['active_module'];?>
/remove_file/<?php echo $_smarty_tpl->tpl_vars['nId']->value;?>
',initialPreviewConfig: <?php echo json_encode($_smarty_tpl->tpl_vars['aData']->value['screenshots']['config']);?>
,initialCaption: "<?php echo $_smarty_tpl->tpl_vars['field']->value['title'];?>
",maxFilePreviewSize: 10240});$('input#def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
').on('filesorted', function(event, params) {var sort = [];$( '.file-preview-thumbnails > .file-preview-frame' ).each(function( index ) {sort[index] = $(this).attr('data-uri');});$.post('', {sort: sort}, function(data){console.log(data);})});<?php echo '</script'; ?>
><?php }
}
if ($_smarty_tpl->tpl_vars['field']->value['type'] == 'wysiwyg') {
echo '<script'; ?>
>tinymce.init({ selector:'textarea#def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
' });<?php echo '</script'; ?>
><?php }?></div><?php }
}
}
}
