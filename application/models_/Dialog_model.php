<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Dialog_model extends Default_model
{
    public $user_id = 0;
    function __construct()
    {
        parent::__construct();
        $this->table = 'dialogs';
    }

    function setUser($user_id){
        $this->user_id = $user_id;
    }

    function getIm($user_id){
        $this->db->select('d.*, COUNT(du.user_id) as c');
        $this->db->from('dialogs d');
        $this->db->join('dialogs_users du1', 'du1.dialog_id = d.id AND du1.user_id ='.$this->user_id, 'inner');
        $this->db->join('dialogs_users du2', 'du2.dialog_id = d.id AND du2.user_id = '.$user_id, 'inner');
        $this->db->join('dialogs_users du', 'du.dialog_id = d.id', 'left');
        $this->db->group_by('d.id');
        $this->db->having('c = 2');

        $query = $this->db->get();
        $result = $query->row();
        if($result){
            return $result;
        }else{
            $this->setTable('dialogs');
            $dialog_id = $this->save(array('created' => time(), 'leader' => $this->user_id, 'type' => 0, 'title' => ''), 'add');

            $this->table = 'dialogs_users';
            $this->db->insert($this->table, array('dialog_id' => $dialog_id, 'user_id' => $user_id, 'date' => time(), 'role' => 0));
            $this->db->insert($this->table, array('dialog_id' => $dialog_id, 'user_id' => $this->user_id, 'date' => time(), 'role' => 0));

            return $this->getIm($user_id);
        }
    }

    function getDialogUsers($dialog_id, $user_id = false){
        $this->db->select('du.*, p.src as avatar_file')
            ->from('dialogs_users du')
            ->join('users u', 'u.id = du.user_id', 'left')
            ->join('photos p', 'p.id = u.avatar', 'left')
            ->where('du.dialog_id', $dialog_id);
        if($user_id)
            $this->db->where('du.user_id != '.$user_id);

        $query = $this->db->get();
        return $query->result();
    }

    function getMessages($id){
        $this->db->select('m.*, p.src as avatar_file');
        $this->db->from('messages m');
        $this->db->join('users u', 'u.id = m.user_id', 'left');
        $this->db->join('photos p', 'p.id = u.avatar', 'left');
        $this->db->where(array('m.dialog_id' => $id));

        $query = $this->db->get();
        return $query->result();
    }

    function getDialogs($uid){
        $sql = "SELECT d.dialog_id, dinfo.title, COUNT(DISTINCT u.id)+1 as users_count, COUNT(DISTINCT unm.id) as unread_count, u.fname, u.lname, u.id as user_id, u.last_action, p.src as avatar_file, m.user_id as from_user_id, m.text, m.date
                FROM soc_dialogs_users d 
                LEFT JOIN soc_dialogs_users du ON(du.dialog_id = d.dialog_id AND du.user_id != $uid) 
                LEFT JOIN soc_dialogs dinfo ON(dinfo.id = d.dialog_id) 
                LEFT JOIN (SELECT * FROM soc_messages ORDER BY date DESC limit 9999999999999) m ON(m.dialog_id = dinfo.id) 
                LEFT JOIN soc_users u ON(du.user_id = u.id) 
                LEFT JOIN soc_photos p ON(p.id = u.avatar)
                LEFT JOIN soc_messages unm ON(unm.dialog_id = dinfo.id AND unm.user_id != $uid AND unm.status < 2) 
                WHERE d.user_id = $uid 
                GROUP BY d.dialog_id
                ORDER BY m.date DESC";
        
        $query = $this->db->query($sql);

        return $query->result();
    }

    function send($data){
        $this->setTable('messages');
        return $this->save($data, 'add');
    }
}