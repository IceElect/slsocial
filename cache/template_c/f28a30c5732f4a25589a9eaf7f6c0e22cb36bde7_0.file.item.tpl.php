<?php
/* Smarty version 3.1.29, created on 2020-01-08 19:32:32
  from "/srv/slto.ru/www/application/themes/Social/user/item.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e160420902ad6_11594608',
  'file_dependency' => 
  array (
    'f28a30c5732f4a25589a9eaf7f6c0e22cb36bde7' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/user/item.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e160420902ad6_11594608 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<li class="user-item">
	<a href="/@<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
" class="avatar middle" data-type="load">
		<?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['user']->value),$_smarty_tpl);?>

	</a>
	<a href="/@<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
" class="user <?php if (($_smarty_tpl->tpl_vars['user']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?>" data-type="load">
		<span class="name"><?php echo $_smarty_tpl->tpl_vars['user']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->lname;?>
</span>
	</a>
	<div class="user-button">
		<?php if ($_smarty_tpl->tpl_vars['user']->value->is_friend) {?>
			<button class="md-icon" data-type="actions" data-action="user" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
">group</button>
		<?php }?>
		<button class="md-icon" data-type="actions" data-action="user" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
">more_horiz</button>
		<ul class="actions-menu user-menu">
			<li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
		</ul>
	</div>
</li><?php }
}
