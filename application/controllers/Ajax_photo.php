<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_photo extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('photo_model', 'Photo');

        $this->user_id = $this->session->userdata('user_id');
    }

    function _need_log(){
        if(!$this->user->is_logged()){
            $this->response['error'] = 10;
            echo json_encode($this->response);
            exit;
        }
    }

    public function close()
    {
        echo $this->frontend->returnJson($this->response);
        return;
    }

    function setAvatar(){
        $this->_need_log();
        
        $aData = $this->input->post();

        if(isset($aData['photo_id'])){
            $photo = $this->Photo->getPhoto(intVal($aData['photo_id']));
            if($photo){
                $this->user_model->update_user($this->user_id, array('avatar' => $photo->id));

                $this->response['response'] = true;
            }
        }

        $this->close();
    }

    function like($id, $type){
        $this->_need_log();
        
        if(!$id)
            return;

        $this->load->model('default_model', 'Likes');
        $this->Likes->setTable('likes');

        $photo = $this->Photo->getPhoto($id);

        $check = $this->Likes->getDataByWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'photo'));
        if(isset($check[0]))
            $check = $check[0];

        //$notify_data = array('post_id' => $comment['id'], 'text' => $comment['content'], 'wall_id' => $comment['wall_id'], 'type' => $type);
        //$notify = array('type' => 'post_like', 'from_user_id' => $this->user_id, 'to_user_id' => $comment['author'], 'date' => time(), 'data' => json_encode($notify_data));
        if($check){
            if($check->value != $type){
                $this->Likes->saveWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'photo'), array('value' => $type));
                //if($notify['to_user_id'] !== $this->user_id)
                    //@$this->Notifications->update($notify, $this->templater->element('mail/notify'));
            }else{
                $this->Likes->delWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'photo'));
                $this->response['remove'] = true;
                //if($notify['to_user_id'] !== $this->user_id)
                    //@$this->Notifications->delete($notify);
            }
            $this->response['response'] = true;
        }else{
            if($this->Likes->save(array('author' => $this->user_id, 'item' => $id, 'value' => $type, 'type' => 'photo', 'date' => time()), 'add')){
                //if($notify['to_user_id'] !== $this->user_id)
                    //@$this->Notifications->add($notify, $this->templater->element('mail/notify'));
                $this->response['response'] = true;
            }
        }
        
        if($this->response['response'] == true){

            $photo = $this->Photo->getPhoto($id);
            $this->response['response'] = array('likes_count' => $photo->likes_count, 'dislikes_count' => $photo->dislikes_count, 'rating_count' => ($photo->likes_count - $photo->dislikes_count));
        }

        echo json_encode($this->response);
    }

    function view($id, $width = 320 ){
        $link = false;
        $continue = true;
        $src = $this->input->get('src');

        $this->load->library('Photo');
        $photo = $this->Photo->getPhoto($id);

        $thumbnail = $this->photo->getThumbnail($photo, $width, true);
        $this->photo->view(FCPATH . $thumbnail, $width);
        return;

        if($src){
            if(file_exists( FCPATH . $src)){
                $src = FCPATH . $src;
                $continue = false;
            }else{
                $src = FCPATH .'/no_avatar.png';
            }


            $continue = false; // TEST
        }

        if($continue){
            @$size = getimagesize( FCPATH .  $photo->src);
            if($size) {
                $src = FCPATH . $photo->src;
            }else{
                $src = FCPATH . "no_avatar.png";
            }
        }

        if (empty($id)) {
            $id = 0;
        }

        $this->photo->view($src, $width);
        return;

        if(!$link){
            $src = "<img src='".$src."'>";
            echo $src;
        }else{
            echo $src;
        }
    }

}