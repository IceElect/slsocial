<div class="comment" data-comment="{$comment->id}">
    <a href="/@{$comment->author}" class="avatar middle" data-type="load">
        {get_avatar u_id=$comment->author u_av=$comment->avatar}
    </a>
    <div class="content">
        <div class="info {if ($comment->last_action >= ($time - 900))}online{/if}">
            <a href="/@{$comment->author}" class="name" data-type="load">{$comment->fname} {$comment->lname}</a>
            <div class="spacer"></div>
            <div class="actions">
                <button class="icon fa icon-dot-3"></button>
            </div>
        </div>
        <div class="text">
            {$comment->text}
        </div>
        <div class="info comment-options">
            <span><abbr title='{$comment->date|date_format:"%Y-%m-%d %H:%M:%S"}' class="time"></abbr></span>
            <div class="spacer"></div>
            <div class="actions likes-actions">
                <span class="like-action {if $comment->is_liked}active{/if}" onclick="wall.likeComment({$comment->id}, 0, event)">
                    <button class="icon md-icon not-button">thumb_up</button>
                </span>

                <span class="comment-rating">
                    {if ($comment->likes_count > 0 or $comment->dislikes_count > 0) }{($comment->likes_count - $comment->dislikes_count)}{/if}
                </span>

                <span class="dislike-action {if $comment->is_disliked}active{/if}" onclick="wall.likeComment({$comment->id}, 1, event)">
                    <button class="icon md-icon not-button">thumb_down</button>
                </span>
            </div>
        </div>
    </div>
</div>