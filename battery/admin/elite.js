
$(function(){
    var file = $(".eliter-file").html();
    $(".eliter-file").replaceWith('<iframe class="eliter-file"></iframe>');
    $('iframe')[0].contentDocument.write(file);
    var cssLink = document.createElement("link");
    cssLink.href = "elite.css";  
    cssLink.rel = "stylesheet";  
    cssLink.type = "text/css";  
    var frame = $(".eliter-file");
    frame.contents().find('head').append(cssLink);
    frame.contents().on('click', "edit", function(){
        frame.contents().find(".elite-focus").removeClass('elite-focus')
        frame.contents().find(this).parent().addClass('elite-focus');
        return false;
    })
})