<?php
/* Smarty version 3.1.29, created on 2020-01-09 21:24:48
  from "/srv/slto.ru/www/application/themes/Social/user/albums.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e176ff0a6fe11_32408202',
  'file_dependency' => 
  array (
    'db566730c5e4d1667606ca07992e03d3a316aa3f' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/user/albums.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e176ff0a6fe11_32408202 ($_smarty_tpl) {
?>
<div class="albums-choose-rows">
    <?php
$_from = $_smarty_tpl->tpl_vars['albums']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_a_0_saved_item = isset($_smarty_tpl->tpl_vars['a']) ? $_smarty_tpl->tpl_vars['a'] : false;
$_smarty_tpl->tpl_vars['a'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['a']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['a']->value) {
$_smarty_tpl->tpl_vars['a']->_loop = true;
$__foreach_a_0_saved_local_item = $_smarty_tpl->tpl_vars['a'];
?>
        <div class="album-choose-row" onclick="photo.album('<?php echo $_smarty_tpl->tpl_vars['a']->value->id;?>
', 1, event);">
            <div class="album-row-img" style="background-image: url('/ajax/photo/view/<?php echo $_smarty_tpl->tpl_vars['a']->value->cover;?>
');">
                <div class="album-row-title-holder">
                    <div class="clearfix">
                    <div class="album-row-counter flr"><?php echo $_smarty_tpl->tpl_vars['a']->value->photos_count;?>
</div>
                    <div class="album-row-title" title="<?php echo $_smarty_tpl->tpl_vars['a']->value->name;?>
"><?php echo $_smarty_tpl->tpl_vars['a']->value->name;?>
</div>
                    </div>
                    <div class="photos_album_description_wrap"><div class="photos_album_description description"></div></div>
                </div>
            </div>
        </div>
    <?php
$_smarty_tpl->tpl_vars['a'] = $__foreach_a_0_saved_local_item;
}
if ($__foreach_a_0_saved_item) {
$_smarty_tpl->tpl_vars['a'] = $__foreach_a_0_saved_item;
}
?>
</div><?php }
}
