<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_user extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('user_model');

        $this->user_id = $this->session->userdata('user_id');
    }

    public function index()
    {
        $this->frontend->view('welcome');
    }

    function avatar_get($id){
        $link = false;
        $continue = true;
        $src = $this->input->get('src');

        if($src){
            if(file_exists( FCPATH . $src)){
                $src = $this->config->item('hosted_url') . $src;
                $continue = false;
            }else{
                $src = base_url('/no_avatar.png');
            }


            $continue = false; // TEST
        }

        if (empty($id)) {
            $id = 0;
        }

        if($continue){
            if($id !== 0){
                $info = $this->user_model->getInfo($id, "i.album as avatar_album, i.src AS avatar_file");
            }else{
                $info = (object) array('avatar_album' => 'none', 'avatar_file' => 'none');
            }
            if(@getimagesize( $this->config->item('hosted_url') . $info->avatar_file)){
                $src = $this->config->item('hosted_url') . $info->avatar_file;
            }else{
                if(@getimagesize( $this->config->item('hosted_url') .  'albums/' . $info->avatar_album . "/" . $info->avatar_file ) ){
                    $src = $this->config->item('hosted_url') . "albums/".$info->avatar_album."/".$info->avatar_file;
                }else{
                    $src = $this->config->item('hosted_url') . "no_avatar.png";
                }
            }
        }

        if(!$link){
            $src = "<img src='".$src."'>";
            echo $src;
        }else{
            echo $src;
        }
    }

    function avatar($id, $size, $hash){
        $link = true;
        $continue = true;

        if (empty($id)) {
            $id = 0;
        }

        if($id !== 0){
            $info = $this->user_model->getInfo($id, "i.album as avatar_album, i.src AS avatar_file");
        }else{
            $info = (object) array('avatar_album' => 'none', 'avatar_file' => 'none');
        }

        if(@getimagesize( FCPATH . $info->avatar_file)){
            $src = FCPATH . $info->avatar_file;
        }else{
            if(@getimagesize( $this->config->item('hosted_url') .  'albums/' . $info->avatar_album . "/" . $info->avatar_file ) ){
                $src = FCPATH . "albums/".$info->avatar_album."/".$info->avatar_file;
            }else{
                $src = FCPATH . "no_avatar.png";
            }
        }

        header('Content-Type: image/png');

        $avatar = new Imagick();
        $avatar->readImage($src);
        $avatar->thumbnailImage(320, 320);

        echo $avatar;
    }

    function sidebar($type){
        $sidebar = $this->load_sidebar($type);
        $sidebar['html'] = $sidebar['html']['data'];

        echo json_encode($sidebar);
    }

    function sect($type){
        $id = $this->input->get('id');
        $response = array('response' => false);

        $section = $this->load_section($type, $id);
        $section = $section['data'];

        if($id){
            if($section){
                $response['response'] = true;
                $response['html'] = $section;
            }
        }

        if(isset($_GET['page'])){
            echo $response['html'];
            return true;
        }
        echo json_encode($response);
    }

    function load_section($type, $id){
        $this->my_smarty->assign('uid', $id);
        switch($type){
            case 'wall':
                $this->load->model('wall_model');

                $page = (isset($_GET['page']))?intval($_GET['page']):1;

                $this->wall_model->user_id = $this->oUser->id;
                $wall = $this->wall_model->getPostsByWall($id, 10, $page);

                $this->my_smarty->assign('page', $page);
                $this->my_smarty->assign('wall', $wall);
                return $this->frontend->fetch('user/'.$type);
            break;
            case 'friends':
                $friends = $this->user_model->getFriends($id);
                $friends_req = $this->user_model->getFriendsReq($id);
                $friends_online = $this->user_model->getFriendsOnline($id);

                $this->my_smarty->assign('friends', $friends);
                $this->my_smarty->assign('friends_req', $friends_req);
                $this->my_smarty->assign('friends_online', $friends_online);
                return $this->frontend->fetch('user/'.$type);
            break;
            case 'albums':
                $this->load->model('photo_model', 'model');

                $albums = $this->model->getAlbums($id);

                $this->my_smarty->assign('albums', $albums);
                return $this->frontend->fetch('user/'.$type);
            break;
        }

        return false;
    }

    function set_status(){
        $this->need_log();
        
        $response = array('response' => false);

        $status = trim($this->input->post('status'));
        if($this->user_model->update_user($this->oUser->id, array('status' => $status))){
            $response['response'] = true;
            $response['status'] = $status;
        }

        $this->returnJson($response);
    }

    function actions($id){
        $this->response['response'] = true;

        $this->friend_button($id);
        if($id != $this->user_id){
            $this->response['html'] .= '<li><a href="javascript:void(0)"><i class="icon icon-flag"></i><span>Пожаловаться</span></a></li>';
        }else{
            $this->response['html'] .= '<li><a href="/edit" data-type="load"><i class="icon icon-pencil"></i><span>Редактировать</span></a></li>';
        }

        echo $this->frontend->returnJson($this->response);
    }

    function friend_button($id = false, $tag = 'li'){
        $this->need_log();
        
        if($id != $this->user_id){ // Если не моя страница
            if($this->user_model->checkFriends($this->user_id, $id)){ // Если друг
                $this->response['html'] .= '<'.$tag.'><a href="javascript:void(0)" onclick="Users.delFriend(\''.$id.'\', this)"><i class="icon icon-user"></i><span>Удалить из друзей</span></a><span class="action-confirm">Нажмите ещё раз</span></'.$tag.'>';
            }else{ // Если не друг
                if($this->user_model->checkReq($this->user_id, $id)){ // Если есть заявка в друзья
                    $this->response['html'] .= '<'.$tag.'><a href="javascript:void(0)" onclick="Users.reqCancel(\''.$id.'\', this)"><i class="icon icon-user"></i><span>Отменить заявку</span></a></'.$tag.'>';
                }else{ // Если нет заявки в друзья
                    if($this->user_model->checkSubscribe($this->user_id, $id)){ // Если подписан
                        $this->response['html'] .= '<'.$tag.'><a href="javascript:void(0)" onclick="Users.unsubscribe(\''.$id.'\', this)"><i class="icon icon-user"></i><span>Отписаться</span></a></'.$tag.'>';
                    }else{ // Если не подписан
                        $perms = $this->user_model->getPerms($id);
                        $perms = $perms[0];
                        if($perms->friends_type == 0){ // Если можно добавить в друзья
                            $this->response['html'] .= '<'.$tag.'><a href="javascript:void(0)" onclick="Users.sendReq(\''.$id.'\', this)"><i class="icon icon-user-add"></i><span>Добавить в друзья</span></a></'.$tag.'>';
                        }else{ // Если нельзя добавить в друзья
                            $this->response['html'] .= '<'.$tag.'><a href="javascript:void(0)" onclick="Users.subscribe(\''.$id.'\', this)"><i class="icon icon-user"></i><span>Подписаться</span></a></'.$tag.'>';
                        }
                    }
                }
            }
        }else{ // Если моя страница

        }
    }

    //Доделать проверку на уже существующую заявку
    function send_req($id){
        $this->need_log();
        
        $data = array('result' => false);
        if($id != $this->user_id){
            if(!$this->user_model->checkFriends($this->user_id, $id)){
                if($this->user_model->checkReq($id, $this->user_id)){
                    if($this->user_model->reqOk($id, $this->user_id)){
                        $this->user_model->subscribe($this->user_id, $id);
                        $data['result'] = true;
                        $data['text'] = 'Удалить из друзей';
                        $data['onclick'] = 'Users.delFriend(\''.$id.'\', this, true)';
                        $this->friend_button($id);
                    }else{
                        $data['error'] = 'Произошла ошибка.';
                    }
                }else{
                    $perms = $this->user_model->getPerms($id);
                    $perms = $perms[0];
                    if($perms->friends_type == 0){
                        if($this->user_model->sendReq($this->user_id, $id)){
                            $this->user_model->subscribe($this->user_id, $id);
                            $data['result'] = true;
                            $data['text'] = 'Отменить заявку';
                            $data['onclick'] = 'Users.reqCancel(\''.$id.'\', this)';
                            $this->friend_button($id);
                        }else{
                            $data['error'] = 'Произошла ошибка.';
                        }
                    }else{
                        $data['error'] = 'Этого пользователя нельзя добавить в друзья.';
                    }
                }
            }else{
                $data['error'] = 'Этот пользователь уже является вашим другом.';
            }
        }else{
            $data['error'] = 'Вы не можете добавить себя в друзья.';
        }
        echo $this->frontend->returnJson($this->response);return true;
        echo json_encode($data);
    }

    function req_cancel($id){
        $this->need_log();
        
        $data = array('result' => false);
        if($id != $this->user_id){
            if($this->user_model->checkFriendsReq($this->user_id, $id)){
                if($this->user_model->reqCancel($this->user_id, $id)){
                    $data['result'] = true;
                    $data['text'] = 'Добавить в друзья';
                    $data['onclick'] = 'user_model.sendReq(\''.$id.'\', this)';
                    $this->friend_button($id);
                }else{
                    $data['error'] = 'Произошла ошибка.';
                }
            }else{
                $data['error'] = 'Такой заявки не существует.';
            }
        }else{
            $data['error'] = 'Вы не можете добавить себя в друзья.';
        }
        echo $this->frontend->returnJson($this->response);return true;
        echo json_encode($data);
    }

    function subscribe($id){
        $this->need_log();
        
        $data = array('result' => false);
        if($id != $this->user_id){
            if($this->user_model->subscribe($this->user_id, $id)){
                $data['result'] = true;
                $data['text'] = 'Отписаться';
                $data['onclick'] = 'Users.unsubscribe(\''.$id.'\', this)';
                $this->friend_button($id);
            }else{
                $data['error'] = 'Произошла ошибка.';
            }
        }else{
            $data['error'] = 'Вы не можете подписаться на себя.';
        }
        echo $this->frontend->returnJson($this->response);return true;
        echo json_encode($data);
    }


    // Доделать проверку на возможность добавления в друзья
    function unsubscribe($id){
        $this->need_log();
        
        $data = array('result' => false);
        if($id != $this->user_id){
            if($this->user_model->checkSubscribe($this->user_id, $id)){
                if($this->user_model->unsubscribe($this->user_id, $id)){
                    $data['result'] = true;
                    $data['text'] = 'Подписаться';
                    $data['onclick'] = 'Users.subscribe(\''.$id.'\', this, true)';
                    $this->friend_button($id);
                }else{
                    $data['error'] = 'Произошла ошибка.';
                }
            }else{
                $data['error'] = 'Такой заявки не существует.';
            }
        }else{
            $data['error'] = 'Вы не можете добавить себя в друзья.';
        }
        echo $this->frontend->returnJson($this->response);return true;
        echo json_encode($data);
    }

    function req_no($id){
        $this->need_log();
        
        $data = array('result' => false);
        if($id != $this->user_id){
            if($this->user_model->checkReq($id, $this->user_id)){
                if($this->user_model->reqNo($id, $this->user_id)){
                    $data['result'] = true;
                }else{
                    $data['error'] = 'Произошла ошибка.';
                }
            }else{
                $data['error'] = 'Такой заявки не существует.';
            }
        }else{
            $data['error'] = 'Вы не можете добавить себя в друзья.';
        }
        echo $this->frontend->returnJson($this->response);return true;
        echo json_encode($data);
    }

    function req_ok($id){
        $this->need_log();
        
        $data = array('result' => false);
        if($id != $this->user_id){
            if($this->user_model->checkReq($id, $this->user_id)){
                if($this->user_model->reqOk($id, $this->user_id)){
                    $this->user_model->subscribe($this->user_id, $id);
                    $data['result'] = true;
                }else{
                    $data['error'] = 'Произошла ошибка.';
                }
            }else{
                $data['error'] = 'Такой заявки не существует.';
            }
        }else{
            $data['error'] = 'Вы не можете добавить себя в друзья.';
        }
        echo $this->frontend->returnJson($this->response);return true;
        echo json_encode($data);
    }
    
    function del_friend($id){
        $this->need_log();
        
        $data = array('result' => false);
        if($id != $this->user_id){
            if($this->user_model->checkFriends($this->user_id, $id)){
                if($this->user_model->delFriend($id, $this->user_id)){
                    $data['result'] = true;
                    $data['text'] = 'Добавить в друзья';
                    $data['onclick'] = 'Users.sendReq(\''.$id.'\', this)';
                    $this->friend_button($id);
                }else{
                    $data['error'] = 'Произошла ошибка.';
                }
            }else{
                $data['error'] = 'Вы не друзья.';
            }
        }else{
            $data['error'] = 'Вы не можете добавить себя в друзья.';
        }
        echo $this->frontend->returnJson($this->response);return true;
        echo json_encode($data);
    }
}
