<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_comment extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('wall_model', 'Posts');

        $this->user_id = $this->session->userdata('user_id');
    }

    function _need_log(){
        if(!$this->user->is_logged()){
            $this->response['error'] = 10;
            echo json_encode($this->response);
            exit;
        }
    }

    public function index()
    {
        $this->frontend->view('welcome');
    }

    function add($id){
        $this->_need_log();
        
        $aData = $this->input->post();
        $this->response = array('result' => false);

        if(!$id)
            return;

        if(isset($aData['content'])){
            $content = $aData['content'];
            if(!empty($content)){

                $attach = array();
                if(isset($aData['attach'])){
                    $attach = $aData['attach'];
                }
                $attach = json_encode($attach);
                $data = array(
                    'wall_id' => $id,
                    'author' => $this->user_id,
                    'content' => $content,
                    'attach' => $attach,
                    'date' => time(),
                );

                $post_data = $this->Posts->addPost($data, true);
                if($post_data){
                    //$this->load_model('OAttach', 'Attach');

                    $notify_data = json_encode(array('post_id' => $post_data->id, 'text' => $content, 'wall_id' => $id));
                    $notify = array('type' => 'post', 'from_user_id' => $this->user_id, 'to_user_id' => $id, 'date' => time(), 'data' => $notify_data);
                    //if($notify['to_user_id'] !== $this->user_id)
                        //@$this->Notifications->add($notify, $this->templater->element('mail/notify'));

                    $post_data->fname = $this->oUser->fname;
                    $post_data->lname = $this->oUser->lname;
                    $post_data->last_action = $this->oUser->last_action;
                    $post_data->new = true;

                    /*$attachesA = json_decode($post_data['attach']);
                    $post_data['attach'] = array('file' => array());
                    if(count($attachesA) > 0){
                        foreach($attachesA as $attach){
                            $params = explode('_', $attach);
                            $type = $params[0];
                            $post_data['attach'][$type][] = $params[1];
                        }
                    }
                    $post_data['attach']['file'] = $this->Attach->getFiles($post_data['attach']['file']);*/

                    $this->my_smarty->assign('post', $post_data);

                    $this->response['post'] = $this->frontend->fetch('wall/post');
                    $this->response['result'] = true;
                }else{
                    $this->response['error'] = 'Произошла ошибка.';
                }
            }else{
                $this->response['error'] = 'Введите текст.';
            }
        }else{
            $this->response['error'] = 'Введите текст.';
        }

        echo json_encode($this->response);
    }

    function like($id, $type){
        $this->_need_log();
        
        if(!$id)
            return;

        $this->load->model('default_model', 'Likes');
        $this->Likes->setTable('likes');

        $comment = $this->Posts->getComment($id);

        $check = $this->Likes->getDataByWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'comment'));
        if(isset($check[0]))
            $check = $check[0];

        //$notify_data = array('post_id' => $comment['id'], 'text' => $comment['content'], 'wall_id' => $comment['wall_id'], 'type' => $type);
        //$notify = array('type' => 'post_like', 'from_user_id' => $this->user_id, 'to_user_id' => $comment['author'], 'date' => time(), 'data' => json_encode($notify_data));
        if($check){
            if($check->value != $type){
                $this->Likes->saveWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'comment'), array('value' => $type));
                //if($notify['to_user_id'] !== $this->user_id)
                    //@$this->Notifications->update($notify, $this->templater->element('mail/notify'));
            }else{
                $this->Likes->delWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'comment'));
                $this->response['remove'] = true;
                //if($notify['to_user_id'] !== $this->user_id)
                    //@$this->Notifications->delete($notify);
            }
            $this->response['response'] = true;
        }else{
            if($this->Likes->save(array('author' => $this->user_id, 'item' => $id, 'value' => $type, 'type' => 'comment', 'date' => time()), 'add')){
                //if($notify['to_user_id'] !== $this->user_id)
                    //@$this->Notifications->add($notify, $this->templater->element('mail/notify'));
                $this->response['response'] = true;
            }
        }
        
        if($this->response['response'] == true){

            $comment = $this->Posts->getComment($id);
            $this->response['response'] = array('likes_count' => $comment->likes_count, 'dislikes_count' => $comment->dislikes_count, 'rating_count' => ($comment->likes_count - $comment->dislikes_count));
        }

        echo json_encode($this->response);
    }

}