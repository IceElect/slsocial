<div class="popup_bg"></div>
<div class="popup popup-photo" data-photo="{$photo->id}">
    <!--
    <div class="popup_header_holder">
        <div class="popup_header module_title">Альбом {$photo->album_name} <a class="fl_r close" onclick="photo.photo({$photo->id}, false, event);"><i class="fa fa-close fa-fw fa-lg"></i></a></div>
    </div>
    -->
    <div class="actions-holder" style="padding-left: 0px">
        <div class="actions">
            <ul class="actions-menu user-menu">
                <li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
            </ul>
            <button class="action icon md-icon" onclick="photo.photo('{$photo->id}', false, event);">close</button>
            <div class="spacer"></div>
            <button class="action icon fa icon-dot-3" data-type="actions" data-action="photo" data-id="{$photo->id}"></button>
        </div>
    </div>
    <div class="popup_content module_content no_p">
        <div class="photo" data-id="{$photo->id}">
            <div class="photo-holder">
            
                <img src="{$photo->src}" alt="">
            
            </div>
            <!--
            <div class="info">
                <a onclick="photo.photoSet({$photo->id}, 'cover', event);">Установить как обложку альбома</a>
                <br /><br />
                {$photo->comment}
                <a class="post_like post_button {if $photo->is_liked}selected{/if}" onclick="photo.like('<?php echo $this->photo['id']; ?>', 0, event);">
                    <i class="fa fa-thumbs-up fa-lg fa-fw"></i>
                    <span class="likes_count">{$photo->likes_count}</span>
                    <div class="alt top dark">Нравится</div>
                </a>
                <a class="post_like post_button {if $photo->is_disliked}selected{/if}" onclick="photo.like('<?php echo $this->photo['id']; ?>', 1, event);">
                    <i class="fa fa-thumbs-down fa-lg fa-fw"></i>
                    <span class="dislikes_count">{$photo->dislikes_count}</span>
                    <div class="alt top dark">Не нравится</div>
                </a>
                <hr>
                <div class="send_holder">
                    <div class="send_form send_comment" data-photo-id="{$photo->id}">
                        {*
                        <a href="/user/{$oUser->user_id}" class="avatar" data-type="load">
                            {get_avatar id=$oUser->user_id}
                        </a>
                        *}
                        <div class="field" onclick="photo.showCommentFormButtons('{$photo->id}', event);" contenteditable="true" placeholder="Написать комментарий"></div>
                        <div class="buttons fl_r" hidden>
                            <button role="send" class="btn btn_accept" onclick="photo.comment('{$photo->id}');">Отправить</button>
                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="comments">
                    {*
                    <?php
                        $comments = $this->pm->do_action('getPhotoComments',array($this->photo['id']));
                        if(count($comments) > 0){
                                foreach($comments as $value){
                                    $value['item_type'] = 'photo';
                                    echo $this->element('wall/comment', 0, $value);
                                }
                        }
                    ?>
                    *}
                </div>
            </div>
            <div style="clear: both;"></div>
            -->

            <div class="clearfix"></div>
        </div>
    </div>
    <div class="actions-holder actions-bottom">
        <div class="actions">
            <div class="action">
                {* {if true}  == is_liked    *}
                {* {if false} == is_disliked *}

                <span class="like-action {if $photo->is_liked}active{/if}" onclick="photo.like({$photo->id}, 0, event)">
                    <i class="md-icon">thumb_up</i>
                </span>

                <b class="action-count rating">
                    {if ($photo->likes_count > 0 or $photo->dislikes_count > 0) }{($photo->likes_count - $photo->dislikes_count)}{/if}
                </b>
                
                <span class="dislike-action {if $photo->is_disliked}active{/if}" onclick="photo.like({$photo->id}, 1, event)">
                    <i class="md-icon">thumb_down</i>
                </span>
            </div>

            <!--
            <div class="action share {if false}active{/if}">
                 {* {if $post->is_shared} *}
                <i class="md-icon">share</i>
                 {* {if $post->share_count > 0} must be inserted instead of {if false} *}
                {if false}
                    <b class="action-count">{* {$post->share_count} *}</b>
                {/if}
            </div>
            -->

            <div class="spacer"></div>

            <div class="action action-comment">
                <span>
                    <b class="action-count">{if $photo->comments_count > 0}{$photo->comments_count}{/if}</b>
                    <i class="md-icon">mode_comment</i>
                </span>
            </div>
        </div>
    </div>
</div>