<div class="users-list">
	<div class="simple-scrollbar">
		<ul>
			{if $notify}
			<li>
				<ul class="list">
					{foreach from=$notify item=$n}
						{include file="notify/item.tpl" notify=$n}
					{/foreach}
				</ul>
			</li>
			{/if}
		</ul>
	</div>
</div>