<?php header('Content-type: text/html; charset=utf-8'); ?>

<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Document</title>

	<link rel="stylesheet" href="http://agent.slto.ru/application/themes/Social/styles/fonts.css">
	<link rel="stylesheet" href="http://agent.slto.ru/application/themes/Social/styles/client.css">
</head>
<body>
	<script src="http://agent.slto.ru/application/themes/Social/scripts/jquery.js"></script>
	
	<div class="slchat-block-holder">
		<div class="slchat-block-header">
			<div class="slchat-block-title">Отримати юридичну допомогу</div>
		</div>
		<div class="slchat-block slchat-dialog">
			<div class="slchat-messages-holder"></div>

			<div class="slchat-send-post-form slchat-send-comment">
		        <div style="padding-top: 11px" onkeypress="onCtrlEnter(event, this);" class="slchat-send-form-area" contenteditable="true" placeholder="Введите сообщение" id="jivo_text"></div>
		        <div class="buttons">
		            <button class="slchat-fl-r slchat-send-button icon icon-paper-plane"></button>
		            <label for="slchat-attach-input" class="fl-r slchat-attach-button icon icon-attach">
		            	<span class="count"></span>
		        	</label>
		        	<input type="file" id="slchat-attach-input" class="slchat-attach-input" multiple="multiple">
		        </div>
		    </div>
	    </div>
	    <div class="slchat-block slchat-qu">
	    	<form id="slchat-form" class="slchat-form">
	    		<input type="text" class="slchat-field" name="lname" placeholder="Прізвище">
	    		<input type="text" class="slchat-field" name="fname" placeholder="Ім’я">
	    		<input type="text" class="slchat-field" name="mname" placeholder="По батькові">
	    		<input type="text" class="slchat-field" name="company" placeholder="Компанія">
	    		<input type="text" class="slchat-field" name="email" placeholder="E-mail">
	    		<input type="text" class="slchat-field" name="phone" placeholder="Контактний телефон">
				
				<label for="type" class="slchat-label">Вид допомоги, який Вам необхідний</label>
	    		<select name="type" class="slchat-field" id="type">
	    			<option value="0">Консультація</option>
	    			<option value="1">Допубликаційна експертиза</option>
	    			<option value="2">Допомога у складанні юридичного документу</option>
	    			<option value="3">Захист у суді</option>
	    			<option value="4">Інше</option>
	    		</select>

				<fieldset class="slchat-fieldset-file">
					<label for="slchat-attach-input" class="slchat-button slchat-button-gray">Прикріпити документи</label>
				</fieldset>

	    		<span class="slchat-label">Натиснувши кнопку Відправити Ви даєте згоду на обробку своїх персональних даних</span>
	    		<button class="slchat-button" type="success">Відправити</button>
	    	</form>
	    </div>
    </div>

	<script>
		var input = $('#slchat-attach-input');
		var form_data = new FormData();
		var status = 0;
		var created = 0;

		$(document).on('submit', '#slchat-form', function(e){
			e.preventDefault();
		    var $that = $(this),

		    formData = new FormData($that.get(0));

		    var data = {
		    	'name': formData.get('fname')+' '+formData.get('mname')+' '+formData.get('lname'),
		    	'type': formData.get('type'),
		    	'email': formData.get('email'),
		    	'phone': formData.get('phone'),
		    	'company': formData.get('company'),
		    }

		    $.post('http://agent.slto.ru/ajax/dialogs/setInfo', data, function(data_a){

			    var file_data = input.prop('files');

			    formData.append('text', 'Новая заявка');
			    
			    $.each( file_data, function( key, value ) {
			    	formData.append('files[]', file_data[key]);
				});

			    $.ajax({
			      url: 'http://agent.slto.ru/ajax/dialogs/send',
			      type: 'post',
			      contentType: false,
			      processData: false,
			      data: formData,
			      dataType: 'html',
			      async: true,
			      beforeSend: function(){
			          
			      },
			      success: function(data){
			      	data = eval('('+data+')');
		      		if(data.response){
		      			var form_data = new FormData();
		      			$(".slchat-attach-button .count").html("");

		      			input.prop('value', null);

		      			$('.slchat-block-holder').removeClass('slchat-no-agents');
		      			get();
		      		}
			      }
			    });
			})
		})
		function onCtrlEnter(ev, handler) {
		  ev = ev || window.event;
		  if (ev.keyCode == 10 || ev.keyCode == 13 && (ev.ctrlKey || ev.metaKey && browser.mac)) {
		      handler();
		      cancelEvent(ev);
		  }else{
		  	if(ev.keyCode == 13){
		  		send();
		  	}
		  }
		}

		var dialog = {};
		$(document).on('click', '.slchat-send-button', function(e){
		    e.preventDefault();
		    send();
		});

		$(document).on('change', '.slchat-send-post-form .buttons', function(e){
			var file_data = input.prop('files');

			var count = file_data.length;
		    $(".slchat-attach-button .count").html(count);
		})

		function send(){
			var aText = $("#jivo_text").html();
			if(aText.trim() == '') return false;

			var form_data = new FormData();

			form_data.append('text', aText);

			var file_data = input.prop('files');
		    
		    $.each( file_data, function( key, value ) {
		    	form_data.append('files[]', file_data[key]);
			});

		    $.ajax({
		      url: 'http://agent.slto.ru/ajax/dialogs/send',
		      dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
		      method: 'POST',
		      async: true,
		      beforeSend: function(){
		          $("body").addClass('loading');
		          //$('.send-button').addClass('fa fa-spin fa-refresh');
		      },
		      success: function(data){
		      	data = eval('('+data+')');
		      	if(data.response == true){
		      		printMessages(data.aData.template);

		      		var d = $('.slchat-messages-holder');
						d.scrollTop(d.prop("scrollHeight"));

			        $("#jivo_text").html('');

			        input.prop('value', null);

			        form_data = new FormData();
			        $(".slchat-attach-button .count").html("");
			    }
		      }
		    });
		}
		function get(){
			$.post('http://agent.slto.ru/ajax/dialogs/get', {'first': true}, function(data){
				data = eval('('+data+')');

				if(data.response){
					status = data.aData.dialog.status;
					created = data.aData.dialog.created;

					$(".slchat-messages-holder").html('');
					console.log(data.aData.messages);
					if(data.aData.messages == ''){
						$('.slchat-block-holder').addClass('slchat-no-agents');
					}else{
						dialog = data.aData.dialog;
						printMessages(data.aData.messages);
						var d = $('.slchat-messages-holder');
							d.scrollTop(d.prop("scrollHeight"));

						poll();
					}
				}
			})
		}

		var time = 0;

		function poll() {
		   setTimeout(function() {
		       $.ajax({ url: "http://agent.slto.ru/ajax/dialogs/get", type: 'post', data: {'page': location.href, 'page_title': document.title} ,success: function(data) {
		            data = eval('('+data+')');

	            	time = data.time;

					if(data.response){
						dialog = data.aData.dialog;
						printMessages(data.aData.new_messages);
						if(data.aData.new_messages){
							if(!$(".slchat-block").hasClass('slchat-trig')){
								$(".slchat-block").addClass('slchat-trig');
							}
							var d = $('.slchat-messages-holder');
							d.scrollTop(d.prop("scrollHeight"));
						}

						if(data.aData.dialog.created != created){
							created = data.aData.dialog.created;
							$(".slchat-block-holder").removeClass('slchat-trig');
							$('.slchat-block-holder').addClass('slchat-no-agents');
						}
					}
		       }, async: true, complete: poll });
		    }, 100);
		};

		get();

		function printMessages(messages){
			$('.slchat-messages-holder').append(messages);
		}

		$(document).on('click', '.slchat-block-header', function(e){
			e.preventDefault();
			
			$(".slchat-block-holder").toggleClass('slchat-trig');
		})
	</script>
</body>
</html>