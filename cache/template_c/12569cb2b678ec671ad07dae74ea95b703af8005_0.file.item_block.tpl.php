<?php
/* Smarty version 3.1.29, created on 2020-01-08 23:29:32
  from "/srv/slto.ru/www/application/themes/Social/user/item_block.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e163bacadb847_26386164',
  'file_dependency' => 
  array (
    '12569cb2b678ec671ad07dae74ea95b703af8005' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/user/item_block.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e163bacadb847_26386164 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<li class="user-item thumb">
    <a href="/@<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
" class="avatar middle" data-type="load">
        <?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['user']->value),$_smarty_tpl);?>

    </a>
    <a href="/@<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
" class="user <?php if (($_smarty_tpl->tpl_vars['user']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?>" data-type="load">
        <span class="name"><?php echo $_smarty_tpl->tpl_vars['user']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value->lname;?>
</span>
    </a>
    <div class="user-button">
        <?php if ($_smarty_tpl->tpl_vars['user']->value->is_friend) {?>
            <button class="md-icon" data-type="actions" data-action="user" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
">group</button>
        <?php }?>
        <button class="md-icon" data-type="actions" data-action="user" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
">more_horiz</button>
        <ul class="actions-menu user-menu">
            <li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
        </ul>
    </div>
</li><?php }
}
