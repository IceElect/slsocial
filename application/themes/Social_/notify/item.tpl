<li class="user-item notify-item">
	<a href="{$n->link}" class="avatar middle" data-type="load">
		{get_avatar id=$n->from_user_id}
	</a>
	<a href="{$n->link}" class="user {if ($n->last_action >= ($time - 900))}online{/if}" data-type="load">
		<span class="name">{$n->fname} {$n->lname}</span>
		<p>Оставил сообщение на вашей стене</p>
	</a>
	{*
	<div class="user-button">
		{if $user->is_friend}
			<button class="md-icon" data-type="actions" data-action="user" data-id="{$user->id}">group</button>
		{/if}
		<button class="md-icon" data-type="actions" data-action="user" data-id="{$user->id}">more_horiz</button>
		<ul class="actions-menu user-menu">
			<li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
		</ul>
	</div>
	*}
</li>