<?php
/* Smarty version 3.1.29, created on 2020-01-05 16:28:57
  from "/srv/slto.ru/www/application/themes/Social/wall/comment.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e11e4990be0e9_81836936',
  'file_dependency' => 
  array (
    '076fe8612236ccbd09d53f0307aaa376e002fe48' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/wall/comment.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e11e4990be0e9_81836936 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
if (!is_callable('smarty_modifier_date_format')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
?>
<div class="comment" data-comment="<?php echo $_smarty_tpl->tpl_vars['comment']->value->id;?>
">
    <a href="/@<?php echo $_smarty_tpl->tpl_vars['comment']->value->author;?>
" class="avatar middle" data-type="load">
        <?php echo smarty_function_get_avatar(array('u_id'=>$_smarty_tpl->tpl_vars['comment']->value->author,'u_av'=>$_smarty_tpl->tpl_vars['comment']->value->avatar),$_smarty_tpl);?>

    </a>
    <div class="content">
        <div class="info <?php if (($_smarty_tpl->tpl_vars['comment']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?>">
            <a href="/@<?php echo $_smarty_tpl->tpl_vars['comment']->value->author;?>
" class="name" data-type="load"><?php echo $_smarty_tpl->tpl_vars['comment']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['comment']->value->lname;?>
</a>
            <div class="spacer"></div>
            <div class="actions">
                <button class="icon fa icon-dot-3"></button>
            </div>
        </div>
        <div class="text">
            <?php echo $_smarty_tpl->tpl_vars['comment']->value->text;?>

        </div>
        <div class="info comment-options">
            <span><abbr title='<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['comment']->value->date,"%Y-%m-%d %H:%M:%S");?>
' class="time"></abbr></span>
            <div class="spacer"></div>
            <div class="actions likes-actions">
                <span class="like-action <?php if ($_smarty_tpl->tpl_vars['comment']->value->is_liked) {?>active<?php }?>" onclick="wall.likeComment(<?php echo $_smarty_tpl->tpl_vars['comment']->value->id;?>
, 0, event)">
                    <button class="icon md-icon not-button">thumb_up</button>
                </span>

                <span class="comment-rating">
                    <?php if (($_smarty_tpl->tpl_vars['comment']->value->likes_count > 0 || $_smarty_tpl->tpl_vars['comment']->value->dislikes_count > 0)) {
echo ($_smarty_tpl->tpl_vars['comment']->value->likes_count-$_smarty_tpl->tpl_vars['comment']->value->dislikes_count);
}?>
                </span>

                <span class="dislike-action <?php if ($_smarty_tpl->tpl_vars['comment']->value->is_disliked) {?>active<?php }?>" onclick="wall.likeComment(<?php echo $_smarty_tpl->tpl_vars['comment']->value->id;?>
, 1, event)">
                    <button class="icon md-icon not-button">thumb_down</button>
                </span>
            </div>
        </div>
    </div>
</div><?php }
}
