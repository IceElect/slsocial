<div class="popup_bg"></div>
<div class="popup popup-small block" data-id="add_album">
    <div class="popup_header_holder">
        <div class="popup_header module_title">
            Создать альбом <button class="md-icon close" onclick="popup.hide('add_album');">close</button>
        </div>
    </div>
    <form action="/ajax/photo/add_album" data-type="ajax"></form>
        <div class="popup_content module_content">
            
            <div class="form-row clearfix">
                <div class="form-label"> <label for="add_album_name">Название</label> </div>
                <div class="form-input-wrap input-wrap-text">
                    <input type="text" name="name" id="add_album_name" class="field">
                </div>
            </div>

            <div class="form-row clearfix">
                <div class="form-label"> <label for="add_album_description">Описание</label> </div>
                <div class="form-input-wrap input-wrap-text">
                    <textarea type="text" name="description" id="add_album_description" class="field"></textarea>
                </div>
            </div>
            
            <div class="load-info">
                <span class="form_result"></span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="popup_controls module_conten">
            <button type="submit" name="submit" value="1" class="button flr">Создать альбом</button>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--<script src="/views/Social/js/drop_load.js"></script>-->