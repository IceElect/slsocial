<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Notify
{

    private $CI;
    private $link_storage;
    private $menu_storage;
    private $menu_store_tree;
    private $base_url;
    private $client;
    private $model;
    private $oUser;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->config = $this->CI->config->item('notify');
        $this->translate = &$this->CI->translate;
        $this->cache = &$this->CI->cache;
        $this->user = &$this->CI->user;

        $this->CI->load->model('notify_model');
        $this->model = $this->CI->notify_model;
    }

    function set_user($user){
        $this->oUser = $user;
    }

    function delete($from_user, $to_user, $notify){
        return $this->model->delWhere(array('from_user_id' => $from_user, 'to_user_id' => $to_user, 'data' => json_encode($notify)));
    }
    function update($from_user, $to_user, $notify, $notify_old){
        return $this->model->update(array('from_user_id' => $from_user, 'to_user_id' => $to_user, 'data' => json_encode($notify)), array('from_user_id' => $from_user, 'to_user_id' => $to_user, 'data' => json_encode($notify_old)));
    }

    function add($type, $to_user, $data, $text, $link = false){
        if(!$link){
            $link = '/@'.$this->oUser->id;
        }
        $aData = array(
            'type' => $type,
            'from_user_id' => $this->oUser->id,
            'to_user_id' => $to_user,
            'link' => $link,
            'date' => time(),
            'data' => $data,
        );

        if($type == 'notify'){
            $saveData = $aData;
            $saveData['data'] = json_encode($saveData['data']);
            $id = $this->model->save($saveData, 'add');
        }else{
            $id = $data['id'];
        }

        if($id){

            if(isset($data['dialog_id']))
                $aData['dialog_id'] = $data['dialog_id'];

            $aData['id'] = $id;
            $aData['text'] = $text;

            $this->CI->my_smarty->assign('notify', $aData);
            $this->CI->my_smarty->assign('_user', $this->oUser);

            $aData['html'] = $this->CI->frontend->fetch('notify/over');
            $aData['html'] = $aData['html']['data'];

            $this->emit($type, $to_user, $aData);
        }
    }

    function emit($type, $to_user, $data) {
        $host = $this->CI->config->item('socket_host');
        $port = $this->CI->config->item('socket_post');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $host.':'.$port);

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        //curl_setopt($ch, CURLOPT_PORT, $port);
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

        curl_setopt($ch, CURLOPT_POST, true);

        $pf = array('f' => $type, 'to_user' => $to_user, 
                 'data' => array());

        foreach($data as $k => $v) {
            $pf[$k] = $v;
        }
        $pf['data'] = $data['data'];

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($pf));

        $res = curl_exec($ch);
        curl_close($ch);
    }

}
