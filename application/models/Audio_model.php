<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: MatrX
 * Date: 20.08.2018
 * Time: 19:08
 */

class Audio_model extends Default_model
{
    function __construct()
    {
        parent::__construct();
        $this->table = 'audios';

    }

    function getAudios($user_id,$page = 1, $limit = 10, $list = false, $search = false){
        $offset = ($page - 1) * $limit;

        if(!$list){

            $list = $this->getUserMainPlaylist($user_id);
            $list = $list['id'];
        }

        return $this->db->select('*')->from($this->table)
            ->join('playlists_audios','playlists_audios.a_id =audios.id','left')
            ->order_by('audios.loaded_date','DESC')->limit($limit, $offset)->get()->result();
    }

    function getList($id){
        return $this->db->select('*')->from('playlists')->where('id',$id)->get()->result();
    }

    function getUserMainPlaylist($id){
        $result = $this->db->select('*')->from('playlists')->where('author',$id)->where('type',0)->get();

        if($result->num_rows()){
            return $result->result();
        }else{
            $this->db->insert('playlists',array('author' => $id, 'type' => 0, 'date' => time(), 'name' => 'Мои аудиозаписи'));
            $query=$this->db->select('*')->from('playlists')->where('author',$id)->where('type',0)->get();
            if($query->num_rows()){
                return $query->result();
            }
            else return FALSE;
        }
    }

    function add($data){
        return $this->db->insert('audios', $data);
    }

    function addToList($list, $audio){
        return $this->db->insert('playlists_audios', array('pl_id' => $list, 'a_id' => $audio, 'date' => time()));
    }

    function delFromList($list, $audio){
        return $this->db->delete('playlists_audios', array('pl_id' => $list, 'a_id' => $audio));
    }
}