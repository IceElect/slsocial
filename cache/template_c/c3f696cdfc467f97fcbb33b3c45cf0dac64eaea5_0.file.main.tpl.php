<?php
/* Smarty version 3.1.29, created on 2020-01-05 16:28:56
  from "/srv/slto.ru/www/application/themes/Social/main.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e11e498a85a67_71628877',
  'file_dependency' => 
  array (
    'c3f696cdfc467f97fcbb33b3c45cf0dac64eaea5' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/main.tpl',
      1 => 1576341906,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:sys/js_conf_inc.tpl' => 2,
    'file:sys/js_css_inc.tpl' => 1,
    'file:sys/js_inc.tpl' => 2,
  ),
),false)) {
function content_5e11e498a85a67_71628877 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>SternLight</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

	<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"><?php echo '</script'; ?>
>
	
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_conf_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_css_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('place'=>"header"), 0, false);
?>

    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('place'=>"header"), 0, false);
?>

</head>
<body class="<?php echo $_smarty_tpl->tpl_vars['body_class']->value;?>
">
	<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="https://cdn.socket.io/socket.io-1.4.5.js"><?php echo '</script'; ?>
>

	<div class="popup_layer"></div>

	<div class="main">
		<div class="fll left-wrap">
			<!-- begin app-bar -->
			<div class="app-bar full-viewport-height main-flex fll">
				<nav role="navigation" class="left-nav simple-scrollbar">
					<ul class="flex-fill">
						<li>
							<a href="javascript:void(0)" data-id="notify" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">alarm</span>
								<span class="app-bar-text">Действия</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-id="friends" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">person</span>
								<span class="app-bar-text">Друзья</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-id="dialogs" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">textsms</span>
								<span class="app-bar-text">Чаты</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" data-id="music" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">headset</span>
								<span class="app-bar-text">Музыка</span>
							</a>
						</li>
						<li class="mobile-show">
							<a href="javascript:void(0)" data-id="settings" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">menu</span>
								<span class="app-bar-text">Другое</span>
							</a>
						</li>
						<li class="mobile-hide">
							<a href="javascript:void(0)" data-id="pages" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">group</span>
								<span class="app-bar-text">Страницы</span>
							</a>
						</li>
						<li class="mobile-hide spacer"></li>
						<li class="mobile-hide">
							<a href="javascript:void(0)" data-id="settings" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">settings</span>
								<span class="app-bar-text">Настройки</span>
							</a>
						</li>
						<li class="mobile-hide">
							<a href="/@" class="avatar<?php if (!$_smarty_tpl->tpl_vars['oUser']->value->id) {?> login-button<?php }?>" <?php if ($_smarty_tpl->tpl_vars['oUser']->value->id) {?>data-type="load"<?php }?>>
								<?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['oUser']->value),$_smarty_tpl);?>

							</a>
						</li>
					</ul>
				</nav>
			</div>
			<!-- end app-bar -->

			<!-- begin app-content -->
			<div class="app-content full-viewport-height fll">
				<div class="loader">
					<a><i class="fa fa-circle-o-notch fa-spin fa-3x"></i></a>
				</div>
				<div class="app-content-wrap">
					<div class="search">
						<div class="app-top">
							<div class="search-flex">
								<form action="#" class="search-form">
									<div class="group">
										<input class="field text-field search-field" type="text" placeholder="Поиск">
										<button class="md-icon search-but">search</button>
									</div>
								</form>
								<button class="md-icon app-icon search-outer">add</button>
							</div>
						</div>
					</div>
					<div class="tab-content"></div>
				</div>
			</div>
			<!-- end app-content -->
		</div>
		<div class="page-content">
			<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

		</div>
		<div class="notify_over">
			
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_conf_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('place'=>"footer"), 0, true);
?>


	<?php echo '<script'; ?>
>
        Notify.init('<?php echo $_smarty_tpl->tpl_vars['oUser']->value->id;?>
', '<?php echo $_smarty_tpl->tpl_vars['oUser']->value->hash;?>
', '<?php echo $_smarty_tpl->tpl_vars['socket_url']->value;?>
');
    <?php echo '</script'; ?>
>
    <?php if ($_smarty_tpl->tpl_vars['additional']->value) {?>
	<?php echo '<script'; ?>
>
		nav.setA(<?php echo $_smarty_tpl->tpl_vars['additional']->value;?>
);
	<?php echo '</script'; ?>
>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['func']->value) {?>
	<?php echo '<script'; ?>
>
		eval('<?php echo $_smarty_tpl->tpl_vars['func']->value;?>
');
	<?php echo '</script'; ?>
>
    <?php }?>

	<?php echo '<script'; ?>
>
		var sidebar = '<?php echo $_smarty_tpl->tpl_vars['sidebar']->value;?>
';
		load_sidebar(sidebar);
		$(document).on('click', '[data-type="actions"]', function(e){
			e.preventDefault();
			var list = $(this).parent().find('.actions-menu');
			if($('body').width()<480){
				list.width($(this).parent().parent().width());
			}
			if(list.hasClass('show')){
				list.removeClass('show');
			}else{
				list.addClass('show');
				if(!list.hasClass('loaded')){
					var id = $(this).attr('data-id');
					var type = $(this).attr('data-action');
					$.post('/ajax/'+type+'/actions/'+id, {}, function(data){
						list.addClass('loaded');
						data = eval('('+data+')');
						list.html(data.html);
					})
				}
				$('.actions-menu').removeClass('show');
				list.toggleClass('show');
			}
		})
		$(document).on('click', '.tree-header', function(e){
			var o = $(this).find('.icon');
			var list = $(this).parent().find(' > ul');
			if(o.hasClass('icon-down-dir')){
				list.hide();
				o.addClass('icon-right-dir').removeClass('icon-down-dir');
			}else{
				list.show();
				o.removeClass('icon-right-dir').addClass('icon-down-dir');
			}
		})
		$(document).on('click', '.app-bar li a.app-bar-link', function(e){
			e.preventDefault();
			var o = $('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(sidebar !== $(this).attr('data-id'))
					return false;
				$('body').css('overflow', 'auto');
				width = o.find('.app-content-wrap').width();
				o.animate({ left: '-'+parseInt(width + 120) }, 200).removeClass('show');
			}else{
				if($('body').width() < 480){
					$('body').css('overflow', 'hidden');
					o.animate({ left: '0px' }, 200).addClass('show');
				}else if($('body').width() > 480 && $('body').width() < 768){
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		})
		window.onresize = function(e) {
			var o = $('.left-wrap .app-content');
			if(o.hasClass('show')){
				if($('body').width() < 480){
					o.animate({ left: '0px' }, 200).addClass('show');
				}else{
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		}
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
