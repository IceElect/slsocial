<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_upload extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('photo_model');
        if(!$this->user->is_logged())
            exit;

        $this->user_id = $this->session->userdata('user_id');
    }

    function close(){
        echo $this->frontend->returnJson($this->response);
    }

    function photo(){
        $aData = $this->input->get();

        if(!isset($aData['album_id'])){
            $this->response['error'] = 'Album is undefined';
            return $this->close();
        }

        $data = array('album' => $aData['album_id'], 'author' => $this->user_id, 'added' => time());

        $album = $this->photo_model->getAlbum(intVal($aData['album_id']));

        if(!$album){
            return $this->close();
        }

        if (!empty($_FILES['photo']['name'])){
            $this->load->library('uploader');

            $path = 'uploads/u'.$album->author.'/a'.$album->id.'/';
            $file_name = uniqid();
            $this->uploader->set_upload_config(array(
                    'file_name' => $file_name,
                    'upload_path' => $path,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'create_folder' => true,
            ));
            //$this->uploader->set_field_title($this->translate->t('post_image', 'Изображение'));//variant_image
            $image_data = $this->uploader->run('photo');
            if ($image_data['error']) {
                //$is_validate = false;
                $this->messages->add_tmessages_list('error', $image_data['data'], 'photo');
            } else {
                $data['src'] = '/' . $path . $image_data['data']['file_name'];
                $id = $this->photo_model->addPhoto($data);
                if($id){
                    $this->response['photo_id'] = $id;
                    $this->response['album_id'] = $album->id;
                    $this->response['path'] = $data['src'];
                    $this->response['response'] = true;
                }
            }
        }

        return $this->close();

    }
}