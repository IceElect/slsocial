<div class="dialog-page" data-dialog-id="{$dialog->id}">
    <div class="thumb dialog-page-inner">
        <div class="messages-holder">
            {foreach from=$messages key=i item=message}
                {if (!isset($u_id))} {$u_id = $message->user_id} {/if}
                {if $i > 0}
                    {if $u_id != $message->user_id}
                        </div>
                            <abbr title="{$message->date|date_format:"%M:%S"}" class="time">{$message->date|date_format:"%H:%M"}</abbr>
                        </div>
                        <div class="message-holder">
                            <a href="/@{$message->user_id}" data-type="load" class="avatar middle">
                                {get_avatar src=$message->avatar_file id=$message->user_id}
                            </a>
                            <div class="message {if $message->user_id == $oUser->id}my{/if}">
                    {/if}
                {else}
                    <div class="message-holder">
                        <a href="/@{$message->user_id}" data-type="load" class="avatar middle">
                            {get_avatar src=$message->avatar_file id=$message->user_id}
                        </a>
                        <div class="message {if $message->user_id == $oUser->id}my{/if}">
                {/if}

                <div class="message-row">
                    {$message->text}
                    <div class="clearfix"></div>
                </div>

                {if $i == $messages|@count-1 }
                        </div>
                        <abbr title="{$message->date|date_format:"%M:%S"}" class="time">{$message->date|date_format:"%H:%M"}</abbr>
                    </div>
                {/if}

                {$u_id = $message->user_id}

            {/foreach}
        </div>
    </div>
    <div class="send-post-form send-post send-message" id="send-post-form" data-dialog-id="{$dialog->id}">
        <div class="buttons">
            <button class="fl-l icon attach-button icon-attach"></button>
        </div>
        <div class="send-form-area" contenteditable="true" placeholder="Введите сообщение" id="send-post-content"></div>
        <div class="buttons">
            <button class="fl-r send-button icon icon-paper-plane"></button>
            <!--
            <label for="attach-input" class="fl-r attach-button icon icon-attach">
                <span class="count"></span>
            </label>
            <input type="file" id="attach-input" class="attach-input" multiple="multiple">
            -->
        </div>
    </div>
</div>