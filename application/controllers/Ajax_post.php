<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_post extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('wall_model', 'Posts');

        $this->user_id = $this->session->userdata('user_id');
        $this->Posts->user_id = $this->user_id;
    }

    function _need_log(){
        if(!$this->user->is_logged()){
            $this->response['error'] = 10;
            echo json_encode($this->response);
            exit;
        }
    }

    public function index()
    {
        $this->frontend->view('welcome');
    }

    function actions($id){
        $this->response['response'] = true;

        $post = $this->Posts->getPost($id);

        $this->response['html'] .= '<li><a href="javascript:void(0)"><i class="icon icon-flag"></i><span>Пожаловаться</span></a></li>';
        if($post->author == $this->oUser->id || $post->wall_id == $this->oUser->id){
            $this->response['html'] .= '<li><a href="javascript:void(0)" onclick="wall.deletePost('.$post->id.' , event)"><i class="icon icon-trash"></i><span>Удалить</span></a></li>';
        }

        echo $this->frontend->returnJson($this->response);
    }

    function filter($string)
    {
        $string = trim($string);
        $string = preg_replace('/(\<div\>\<br\>\<\/div\>)+/', "\r\n", $string);
        return $string;
    }

    function add($id){
        $this->_need_log();

        $aData = $this->input->post();
        $this->response = array('result' => false);

        if(!$id)
            return;

        if(isset($aData['content'])){
            $content = $this->filter($aData['content']);

            if(!empty($content)){

                $attach = array();
                if(isset($aData['attach'])){
                    $attachData = $aData['attach'];

                    foreach($attachData as $key => $value){
                        if($value['type'] == 'photo'){

                        }
                        $attach[] = $value;
                    }
                }
                $data = array(
                    'wall_id' => $id,
                    'author' => $this->user_id,
                    'content' => $content,
                    'attach' => json_encode($attach),
                    'date' => time(),
                );

                $post_data = $this->Posts->addPost($data, true);
                if($post_data){
                    //$this->load_model('OAttach', 'Attach');
                    
                    $this->load->library('Notify');
                    $this->notify->set_user($this->oUser);

                    $notify = array('post_id' => $post_data->id, 'text' => $content, 'wall_id' => $id);
                    $this->notify->add('notify', $id, $notify, 'Оставил запись на вашей стене2', '/@'.$id);

                    $post_data->fname = $this->oUser->fname;
                    $post_data->lname = $this->oUser->lname;
                    $post_data->last_action = $this->oUser->last_action;
                    $post_data->attach = $attach;
                    $post_data->new = true;

                    $post_data->attach = json_encode($attach);

                    $this->my_smarty->assign('post', $post_data);

                    $this->response['post'] = $this->frontend->fetch('wall/post');
                    $this->response['result'] = true;
                }else{
                    $this->response['error'] = 'Произошла ошибка.';
                }
            }else{
                $this->response['error'] = 'Введите текст.';
            }
        }else{
            $this->response['error'] = 'Введите текст.';
        }

        echo json_encode($this->response);
    }

    function comment($id){
        $this->_need_log();
        
        $aData = $this->input->post();
        $response = array('response' => false);

        if(!$id)
            return;
        
        if(isset($aData['text'])){
            $post = $this->Posts->getPost($id);

            $content = $aData['text'];
            $answer = 0;
            if(!empty($content)){
                if(isset($aData['answer']))
                    $answer = intval($aData['answer']);

                $data = array(
                    'item' => $id,
                    'author' => $this->user_id,
                    'text' => $content,
                    'answer' => $answer,
                    'date' => time(),
                );

                $post_data = $this->Posts->addComment($data);

                if($post_data){
                    $this->load->library('Notify');
                    $this->notify->set_user($this->oUser);

                    $notify = array('post_id' => $post_data->item, 'wall_id' => $post->wall_id, 'text' => $post_data->text, 'comment_id' => $post_data->id);
                    //$notify = array('type' => 'post_comment', 'from_user_id' => $this->user_id, 'to_user_id' => $post->author, 'date' => time(), 'data' => json_encode($notify_data));
                    $this->notify->add('notify', $post->author, $notify, 'Прокомментрировал вашу запись', '/@'.$post->wall_id);

                    if($answer){
                        $comment = $this->Posts->getComment($answer);

                        $post_data['answer_name'] = $comment->fname.' '.$comment->lname;
                        $post_data['answer'] = $answer;

                        $notify = array('comment_id' => $answer, 'text' => $post_data->text, 'post_id' => $post_data->item);
                        //$notify = array('type' => 'answer', 'from_user_id' => $this->user_id, 'to_user_id' => $comment->author, 'date' => time(), 'data' => json_encode($notify_data));

                        
                    }

                    $post_data->fname = $this->oUser->fname;
                    $post_data->lname = $this->oUser->lname;
                    $post_data->last_action = $this->oUser->last_action;
                    $post_data->new = true;

                    $this->my_smarty->assign('comment', $post_data);
                    $this->response['post'] = $this->frontend->fetch('wall/comment');
                    $this->response['response'] = true;
                }else{
                    $data['error'] = 'Произошла ошибка.';
                }
            }else{
                $data['error'] = 'Введите текст.';
            }
        }else{
            $data['error'] = 'Введите текст.';
        }

        echo json_encode($this->response);
    }

    function like($id, $type){
        $this->_need_log();
        
        if(!$id)
            return;

        $this->load->library('Notify');
        $this->notify->set_user($this->oUser);

        $this->load->model('default_model', 'Likes');
        $this->Likes->setTable('likes');

        $post = $this->Posts->getPost($id);

        $check = $this->Likes->getDataByWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'post'));
        if(isset($check[0]))
            $check = $check[0];

        $notify = array('post_id' => $post->id, 'text' => $post->content, 'wall_id' => $post->wall_id, 'type' => $type);
        //$notify = array('type' => 'post_like', 'from_user_id' => $this->user_id, 'to_user_id' => $post['author'], 'date' => time(), 'data' => json_encode($notify_data));
        if($check){
            if($check->value != $type){
                $old_type = ($type == 0)?1:0;
                $notify_old = $notify;$notify_old['type'] = $old_type;
                $this->Likes->saveWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'post'), array('value' => $type));
                $this->notify->update($this->user_id, $post->author, $notify, $notify_old);
            }else{
                $this->Likes->delWhere(array('author' => $this->user_id, 'item' => $id, 'type' => 'post'));
                $this->response['remove'] = true;
                $this->notify->delete($this->user_id, $post->author, $notify);
            }
            $this->response['response'] = true;
        }else{
            if($this->Likes->save(array('author' => $this->user_id, 'item' => $id, 'value' => $type, 'type' => 'post', 'date' => time()), 'add')){
                $this->notify->add('notify', $post->author, $notify, 'Оценил вашу запись', '/@'.$post->wall_id);
                $this->response['response'] = true;
            }
        }
        
        if($this->response['response'] == true){

            $post = $this->Posts->getPost($id);
            $this->response['response'] = array('likes_count' => $post->likes_count, 'dislikes_count' => $post->dislikes_count, 'rating_count' => ($post->likes_count - $post->dislikes_count));
        }

        echo json_encode($this->response);
    }

    function delete($id){
        $this->_need_log();
        
        if(!$id)
            return;

        $post = $this->Posts->getPost($id);
        if($post){
            if($post->author == $this->oUser->id || $post->wall_id == $this->oUser->id){
                if($this->Posts->update(array('deleted' => 1), array('id' => $id))){
                    $this->my_smarty->assign('post', $post);
                    $html = $this->frontend->fetch('wall/deleted_post');

                    $this->response['html'] = $html['data'];
                    $this->response['response'] = true;
                }else{

                }
            }
        }

        echo json_encode($this->response);
    }

    function restore($id){
        $this->_need_log();
        
        if(!$id)
            return;

        $post = $this->Posts->getPost($id);

        if($post){
            if($post->author == $this->oUser->id || $post->wall_id == $this->oUser->id){
                if($this->Posts->update(array('deleted' => 0), array('id' => $id))){
                    $this->my_smarty->assign('post', $post);
                    $html = $this->frontend->fetch('wall/post');
                    
                    $this->response['html'] = $html['data'];
                    $this->response['response'] = true;
                }else{

                }
            }else{

            }
        }else{

        }

        echo json_encode($this->response);
    }

}