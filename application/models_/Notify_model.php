<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Notify_model extends Default_model
{
	function __construct()
    {
        parent::__construct();
        $this->table = 'notify';
    }

    function getNotify($id, $new = true, $select = 'n.*, u.id as user_id, u.fname, u.lname, u.last_action, u.birthdate'){
        if(!$id)
            return false;

        $this->db->select($select)
        	->from($this->table.' n')
        	->join('users u', 'u.id = n.from_user_id', 'left')
        	->where('n.to_user_id = '.$id.' AND n.date < '.time());

        if($new){
            $this->db->where('n.status < 1');
        }
        
        $this->db->order_by('n.date', 'desc');
        
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
    function viewNotify($id){
        $sql = "UPDATE soc_notify SET status = 2 WHERE to_user_id = $id AND status < 2 AND date < ".time();
        return $this->db->query($sql);
    }
}