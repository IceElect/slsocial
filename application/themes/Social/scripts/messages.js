$(document).on('keypress', '#send-post-form[data-dialog-id] #send-post-content', function(e){
    var form = $(this).closest('.send-post-form');
    var keyCode = e.keyCode || e.charCode || e.which;
    if (keyCode == 10 || keyCode == 13){
        if (!e.ctrlKey){
            /*
            var selection = window.getSelection(),
                range = selection.getRangeAt(0),
                br = document.createElement("br"),
                textNode = document.createTextNode("\u00a0");
            range.deleteContents();
            range.insertNode(br);
            range.collapse(false);
            range.insertNode(textNode);
            range.selectNodeContents(textNode);
            selection.removeAllRanges();
            selection.addRange(range);
            return false;*/
            messages.send(form.attr('data-dialog-id'), $(".btn[role=send]"));
            return false;
        }else{
            
        }
        
        //return false;
    }
});
$(function(){
    $('#send-post-form[data-dialog-id] #send-post-content').pastableContenteditable();
    $('#send-post-content').on('pasteImage', function(ev, data){
        var blobUrl = URL.createObjectURL(data.blob);
        console.log(blobUrl);
        
        var name = data.name != null ? ', name: ' + data.name : '';
        data.blob.name = 'eqweqwe.png';
        data.blob.filename = blobUrl;
        /*
        $('<div class="result">image: ' + data.width + ' x ' + data.height + name + '<img src="' + data.dataURL +'" ><a href="' + blobUrl + '">' + blobUrl + '</div>').insertAfter(this);
        */
        upload.options['photo'].album_id = 1;

        var files = [data.blob];
        upload.uploadFiles('photo', files, 1);
        //console.log(data.dataURL);
    }).on('pasteImageError', function(ev, data){
        alert('Oops: ' + data.message);
        if(data.url){
            alert('But we got its url anyway:' + data.url)
        }
    }).on('pasteText', function(ev, data){
        $('<div class="result"></div>').text('text: "' + data.text + '"').insertAfter(this);
    }).on('pasteTextRich', function(ev, data){
        $('<div class="result"></div>').text('rtf: "' + data.text + '"').insertAfter(this);
    }).on('pasteTextHtml', function(ev, data){
        $('<div class="result"></div>').text('html: "' + data.text + '"').insertAfter(this);
    });
});

$(document).on('click', '#send-post-form[data-dialog-id] .send-button', function(e){
    var form = $(this).closest('.send-post-form');
    messages.send(form.attr('data-dialog-id'), $(".btn[role=send]"));
})
function messages(){
    self = {};
    self.attaches = [];

    self.send = function(dialog_id, element){
        var content = $(".send-post-form #send-post-content").html();
        $.ajax({
            type: 'POST',
            url: '/ajax/dialog/send/'+dialog_id,
            data: {content: content, attach: self.attaches},
            beforeSend: function () {
                $(element).html('Загрузка...');
            },
            success: function (data){
                data = eval('('+data+')');
                if('error' in data){
                    error(data.error);
                }else{
                    if(data.result){
                        $("[data-dialog-id='"+dialog_id+"'] .send-form-area").html('');
                        data.data['data[message_html]'] = data.data.message_html;
                        self.addTo(data.data);
                        /*$(".wall-posts").prepend(data.post.data);
                        $(".send-post-form #send-post-content").html("");
                        $("abbr.time").timeago();*/
                        self.attaches = [];
                    }else{
                        error(11);
                    }
                }
            },
            error: function(){
                error(12);
            }
        });
        $(element).html('Опубликовать');
        return false;
    };
    self.init = function(dialog_hash){
        var wtf    = $('.messages-holder');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
        Notify.socket.emit('join-dialog', dialog_hash);
    };
    self.addTo = function(data){
        var dialog_id = data.dialog_id;
        $("[data-dialog-id="+dialog_id+"] .messages-holder").append(data['data[message_html]']);
    }

    return self;
}
var messages = new messages();