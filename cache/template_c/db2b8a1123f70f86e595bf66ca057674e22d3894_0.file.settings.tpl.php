<?php
/* Smarty version 3.1.29, created on 2020-02-13 15:30:55
  from "/srv/slto.ru/www/application/themes/Social/sidebar/settings.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e45417fe6b337_76917576',
  'file_dependency' => 
  array (
    'db2b8a1123f70f86e595bf66ca057674e22d3894' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/sidebar/settings.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e45417fe6b337_76917576 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/srv/slto.ru/www/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<div class="users-list">
    <ul>
        <li class="user-item dialog-item">
            <a href="/@<?php echo $_smarty_tpl->tpl_vars['oUser']->value->id;?>
" class="avatar middle" data-type="load">
                <?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['oUser']->value),$_smarty_tpl);?>

            </a>
            <a href="/@<?php echo $_smarty_tpl->tpl_vars['oUser']->value->id;?>
" class="user <?php if (($_smarty_tpl->tpl_vars['oUser']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?>" data-type="load">
                <span class="name"><?php echo $_smarty_tpl->tpl_vars['oUser']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['oUser']->value->lname;?>
</span>
                <p>0 уровень</p>
            </a>
            <!--
            <div class="user-button">
                <button class="md-icon">textsms</button>
            </div>
            -->
        </li>
    </ul>
</div>
<ul class="app-content-menu">
    <li>
        <a href="/edit" data-type="load">
            <button class="md-icon">info_outline</button>
            <span>Информация</span>
        </a>
    </li>
    <li>
        <a href="/settings" data-type="load">
            <button class="md-icon">build</button>
            <span>Настройки</span>
        </a>
    </li>
    <li>
        <a href="/users/logout">
            <button class="md-icon">power_settings_new</button>
            <span>Выйти</span>
        </a>
    </li>
</ul><?php }
}
