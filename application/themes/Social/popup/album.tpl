<div class="popup_bg"></div>
<div class="popup popup-top" data-id="attach_photo">
    <div class="popup_header_holder">
        <div class="popup_header module_title">
            {$album->name} <button class="md-icon close" onclick="popup.hide('attach_photo');">close</button>
        </div>
    </div>
    <div class="popup_controls module_conten">
        <input id="file" type="file" style="display: none;" onchange="upload.onFile('photo', {$album->id}, this.files);" multiple="true" size="28" accept="image/jpeg,image/png,image/gif,image/heic,image/heif">
            <label for="file" class="button">Загрузить файл</label>
        <div class="clearfix"></div>
    </div>
    <div class="popup_content module_content">

        <div class="photos-choose-rows">
            {foreach from=$photos key="k" item="photo"}
            <a class="photo-choose-row" onclick="return photo.photo('{$photo->id}', 1, event);">
                <div class="photo-row-img" style="background-image: url('/ajax/photo/view/{$photo->id}');"></div>
            </a>
            {/foreach}
        </div>
        
        <div class="load-info">
            <span id="message"></span>
        </div>
        <div class="clearfix"></div>
    </div>
</div>