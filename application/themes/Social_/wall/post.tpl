<div class="post-holder" data-post="{$post->id}">
    <div class="post">
        <div class="post-header">
            <a href="/@{$post->author_id}" class="user-avatar avatar middle" data-type="load">
				{get_avatar id=$post->author_id}
			</a>
            <div class="info-holder {if ($post->last_action >= ($time - 900))}online{/if}">
                <a href="/@{$post->author_id}" class="name" data-type="load">{$post->fname} {$post->lname}</a>
                <abbr title='{$post->date|date_format:"%Y-%m-%d %H:%M:%S"}' class="time"></abbr>
            </div>
            <div class="spacer"></div>
            <div class="actions">
                <button class="icon fa icon-dot-3" data-type="actions" data-action="post" data-id="{$post->id}"></button>
                <ul class="actions-menu user-menu">
                    <li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="post-content">
            {$post->content}

            {$attach = $functions->parse_attach($post->attach)}

            <div class="photos-choose-rows">
                {foreach from=$attach.photo key="k" item="photo"}
                <a class="photo-choose-row" onclick="return photo.photo('{$photo->id}', 1, event);">
                    <div class="photo-row-img" style="background-image: url('{$photo->src}');"></div>
                </a>
                {/foreach}
            </div>
        </div>
        <div class="actions">
            <div class="action">
                {* {if true}  == is_liked    *}
                {* {if false} == is_disliked *}

                <span class="like-action {if $post->is_liked}active{/if}" onclick="wall.like({$post->id}, 0, event)">
                    <i class="md-icon">thumb_up</i>
                </span>

                <b class="action-count rating">
                    {if ($post->likes_count > 0 or $post->dislikes_count > 0) }{($post->likes_count - $post->dislikes_count)}{/if}
                </b>
                
                <span class="dislike-action {if $post->is_disliked}active{/if}" onclick="wall.like({$post->id}, 1, event)">
                    <i class="md-icon">thumb_down</i>
                </span>
            </div>

            <!--
            <div class="action share {if false}active{/if}">
                 {* {if $post->is_shared} *}
                <i class="md-icon">share</i>
                 {* {if $post->share_count > 0} must be inserted instead of {if false} *}
                {if false}
                    <b class="action-count">{* {$post->share_count} *}</b>
                {/if}
            </div>
            -->

            <div class="spacer"></div>

            <div class="action action-comment">
                <span>
                    <b class="action-count">{if $post->comments_count > 0}{$post->comments_count}{/if}</b>
                    <i class="md-icon">mode_comment</i>
                </span>
            </div>
        </div>
        {get_comments id={$post->id}}
        <div class="comments-list {if $comments|@count == 0}empty{/if}">
            {foreach from=$comments item=$comment}
                {include file="wall/comment.tpl" post=$comment}
            {/foreach}
        </div>
    </div>
    <div class="send-comment">
        <div class="send-post-form send-comment-form" data-post-id="{$post->id}">
            <div class="avatar middle">
                {get_avatar id=$oUser->id}
            </div>
            <div onkeypress="onCtrlEnter(event, this);" class="send-form-area" contenteditable="true" placeholder="Написать комментарий"></div>
            <div class="buttons">
                <button class="fl-r send-button icon icon-paper-plane"></button>
            </div>
        </div>
    </div>
</div>
