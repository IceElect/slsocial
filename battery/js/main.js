function scrollTo(id){
    $('html, body').animate({
        scrollTop: $(id).offset().top
    }, 1000);
}
$(document).on('click', ".scrollto", function(){
    scrollTo($(this).attr('href'));
})
$(function(){
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
    
    //$(".fancybox").fancybox();

    var slider = $('.slider-product').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        autoplayTimeout:3050,
        smartSpeed:1000,
        autoplayHoverPause:true,
        autoHeight: true, 
        items:1
    })
    $('.owl-carousel').on('mouseleave', function(){
        slider.trigger('play.owl.autoplay',[1050]);
    })
    $('#slider_install .left').click(function(e){
        e.preventDefault();
        slider.trigger('prev.owl.carousel');
    })
    $('#slider_install .right').click(function(e){
        e.preventDefault();
        slider.trigger('next.owl.carousel');
    })
    var slider_parthner = $('.slider_parthner').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        autoplayTimeout:1050,
        smartSpeed:1000,
        autoplayHoverPause:false,
        responsive : {
            0:{
                items:3,
            },
            768:{
                items:4,
            },
            1024:{
                items:5,
            }
        }
    })
    $('#slider_parthner .left').click(function(e){
        e.preventDefault();
        slider_parthner.trigger('prev.owl.carousel');
    })
    $('#slider_parthner .right').click(function(e){
        e.preventDefault();
        slider_parthner.trigger('next.owl.carousel');
    })

    $("#video .video-col").click(function(e){
        var url = $(this).attr('data-src');
        $("#video iframe").attr('src', url);
    })

    $(document).on('click', '.calc-button', function(e){
        e.preventDefault();
        var value1 = $('.calc_payment').val();
        value3 = (value1 * 0.05);
        if (value3 > 0) {
            var ras = value3.toFixed(1).split('.');
            $('#result_e').html(ras[0] + ',' + ras[1]);
        } else {
            $('#result_e').text(0);
        }
        value2 = $('.calc_appraisal').val();
        value4 = (value3 * 7 * value2 * 30);
        if (value4 > 0) {
            $('#result_price').text(value4.toFixed(0));
        } else {
            $('#result_price').text(0);
        }
    })
})
$(document).ready(function(){
    $("#toggle-menu").click(function(){
        $(this).toggleClass("is-active");
        $(".main-menu").slideToggle(500);
    });
    $(window).scroll(function(){
        if ($(window).scrollTop() > 0) {
            $(".top-line").addClass('scrolling');
        }else{
            $(".top-line").removeClass('scrolling');
        }
    })

    $(".additional-photos .image").click(function(e){
        e.preventDefault();

        $(".additional-photos .image").removeClass('selected');
        $(this).addClass('selected');
        var url = $(this).find('img').attr('src');
        $(".product .image-holder img").attr('src', url);
    })
});


$('.calc-summ').click(function(){
    var k = 0.2;
    var time = 365;
    var eco = 8000;
    var price = parseFloat($('input.price').val());
    var litr = parseFloat($('input.litr').val());
    var formula = (price * litr * k * time) - eco;
    if(formula.toString().indexOf('.') > 0)
    {
        formula = parseFloat(formula).toFixed(2);
        if(formula.toString().indexOf('.00') > 0)
        {
            formula = parseInt(formula);
        }
    }
    else
    {
        formula = parseInt(formula);
    }
    if(isNaN(formula) == true)
    {
        $('.calc-error').html('Введите правильные значения');
        $('.calc-result').html('');
        return false;
    }
    if(formula <= 0)
    {
        $('.calc-error').html('Не могу посчитать результат, минимальные значения не соответствуют действительности');
        $('.calc-result').html('');
        return false;
    }
    $('.calc-error').html('');
    $('.calc-result').html('Годовая экономия топлива: <b class="calc-result-price">' + formula + '</b> рублей <br/>.');
})

$(".buy").click(function(e){
    e.preventDefault();

    $(".overlay").css('visibility', 'visible');
    $(".overlay").css('opacity', 1);

    $(".popup.buy_popup").css('visibility', 'visible');
    $(".popup.buy_popup").css('top', '50%');
    $(".popup.buy_popup").css('opacity', 1);
})
$(".overlay").click(function(e){
    e.preventDefault();

    $(".overlay").css('visibility', 'hidden');
    $(".overlay").css('opacity', 0);

    $(".popup.buy_popup").css('visibility', 'hidden');
    $(".popup.buy_popup").css('top', '20%');
    $(".popup.buy_popup").css('opacity', 0); 
})

function error(text){
    alert(text);
}

$(document).on('submit', 'form[data-type=ajax]', function(e){
    e.preventDefault();
    var $that = $(this),
    formData = new FormData($that.get(0));
    $.ajax({
      url: $that.attr('action'),
      type: $that.attr('method'),
      contentType: false,
      processData: false,
      data: formData,
      dataType: 'html',
      beforeSend: function(){
          $("body").addClass('loading');
          $that.find('.form_result').html('<i class="fa fa-spin fa-refresh"></i>');
      },
      success: function(data){
        console.log(data);
        $("body").removeClass('loading');
        $(".error").html("");
        var data = eval('('+data+')');
        if(data.response){
            alert('Ваш заказ принят. Ожидайте звонка');
            $(".overlay").click();
        }else{
            error(data.error);
        }
      }
    });
});