$(function(){
    if(!Modernizr.svg) {
        $("img[src*='svg']").attr("src", function() {
            return $(this).attr("src").replace(".svg", ".png");
        });
    };
    
    $('.bxslider').bxSlider({
        'pager': false
    });

    $("#toggle-menu").click(function(e){
        e.preventDefault();
        $(this).toggleClass('is-active');
        $("ul.main-menu").slideToggle(500);
    })
    $.datepicker.setDefaults(
      $.extend(
        {'dateFormat':'dd-mm-yy'},
        $.datepicker.regional['TW']
      )
    );
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    /*$('input.datepicker').datepicker({
        showOn: 'both',
        buttonImageOnly: true,
        buttonImage: '/images/026.png'
    });*/
    var width = $(".container").width();
    if(width <= 480){
        var number = 1;
    }else{
        var number = 2;
    }
    var dateFormat = "dd.mm.yy",
      from = $( ".datepicker#from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: number,
          maxDate: 11
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( ".datepicker#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: number
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
    /*$( ".datepicker" ).datepicker({
        numberOfMonths: number,
    });*/
    $( "input[type='number']" ).spinner();
})