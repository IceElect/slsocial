<!DOCTYPE html>

<html lang="en-US">

<head>

<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]--><!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]--><!--[if gt IE 8]><!--><!--<![endif]-->



  <title>How to rebuild a whiskey barrel</title>

  <meta charset="UTF-8">



  <meta name="viewport" content="width=device-width, initial-scale=1.0">

 



  <meta name="description" content="How to rebuild a whiskey barrel">

 

  <style type="text/css">.aawp .aawp-tb__row--highlight{background-color:#256aaf;}.aawp .aawp-tb__row--highlight{color:#256aaf;}.aawp .aawp-tb__row--highlight a{color:#256aaf;}</style>

  <style type="text/css">.single-post .amzbuttns .aawp-button--buy {

    border: none!important;

    background: #FC9827 !important;

    color: #fff !important;

padding: 20px !important;

    font-size: 17px !important;

    font-weight: 600 !important;

}



.:before {

    display: none !important;

}



 {

    border: none!important;

    background: #FC9827 !important;

    color: #fff !important;

    padding: 20px 20px 20px 20px !important;

    font-size: 18px !important;

    font-weight: 600 !important;

}</style>

  <style type="text/css">div#toc_container {background: #ffffff;border: 1px solid #ffffff;width: 100%;}div#toc_container ul li {font-size: 14px;}div#toc_container  {color: #595959;}</style><!--[if lt IE 9]><![endif]-->

  <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress.">

<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="" media="screen"><![endif]-->

  <style>

    

.post .td-post-header .entry-title {

        color: #424242;

    }

    .td_module_15 .entry-title a {

        color: #424242;

    }



    

    .td-module-meta-info .td-post-author-name a {

    	color: #424242;

    }



    

    .td-post-content,

    .td-post-content p {

    	color: #424242;

    }



    

    .td-post-content h1,

    .td-post-content h2,

    .td-post-content h3,

    .td-post-content h4,

    .td-post-content h5,

    .td-post-content h6 {

    	color: #424242;

    }



    

    .td-page-header h1,

    .td-page-title,

    .woocommerce-page .page-title {

    	color: #424242;

    }



    

    .td-page-content p,

    .td-page-content .td_block_text_with_title,

    .woocommerce-page .page-description > p {

    	color: #424242;

    }



    

    .td-page-content h1,

    .td-page-content h2,

    .td-page-content h3,

    .td-page-content h4,

    .td-page-content h5,

    .td-page-content h6 {

    	color: #424242;

    }



    .td-page-content .widgettitle {

        color: #fff;

    }







    

    .top-header-menu > li > a,

    .td-weather-top-widget .td-weather-now .td-big-degrees,

    .td-weather-top-widget .td-weather-header .td-weather-city,

    .td-header-sp-top-menu .td_data_time {

        font-size:15px;

	font-weight:normal;

	text-transform:none;

	

    }

    

     > .td-menu-item > a,

    .td-theme-wrap .td-header-menu-social {

        font-size:16px;

	text-transform:none;

	

    }

    

    .sf-menu ul .td-menu-item a {

        font-size:15px;

	text-transform:none;

	

    }

	

    .td_mod_mega_menu .item-details a {

        font-size:15px;

	text-transform:none;

	

    }

    

    .td_mega_menu_sub_cats .block-mega-child-cats a {

        font-size:15px;

	text-transform:none;

	

    }

    

	.post .td-post-header .entry-title {

		font-family:Lato;

	

	}

    

    .td-post-template-default .td-post-header .entry-title {

        font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-post-content p,

    .td-post-content {

        font-family:Lato;

	

    }

    

    .td-post-content h1 {

        font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-post-content h2 {

        font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-post-content h3 {

        font-family:Lato;

	font-size:26px;

	font-weight:bold;

	

    }

    

    .td-post-content h4 {

        font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-page-title,

    .woocommerce-page .page-title,

    .td-category-title-holder .td-page-title {

    	font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-page-content p,

    .td-page-content li,

    .td-page-content .td_block_text_with_title,

    .woocommerce-page .page-description > p,

    .wpb_text_column p {

    	font-family:Lato;

	

    }

    

    .td-page-content h1,

    .wpb_text_column h1 {

    	font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-page-content h2,

    .wpb_text_column h2 {

    	font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-page-content h3,

    .wpb_text_column h3 {

    	font-family:Lato;

	font-size:26px;

	font-weight:bold;

	

    }

    

    .td-page-content h4,

    .wpb_text_column h4 {

    	font-family:Lato;

	font-weight:bold;

	

    }

    

    .td-page-content h5,

    .wpb_text_column h5 {

    	font-family:Lato;

	font-weight:bold;

	

    }

    

    body, p {

    	font-family:Lato;

	font-size:16px;

	

    }

  </style>

  

  <style type="text/css" id="wp-custom-css">

			.walmart-button-custom-content {

    border: none!important;

    background: #041e42 !important;

    color: #fff !important;

    padding: 20px 20px 20px 20px !important;

    font-size: 18px !important;

    font-weight: 600 !important;

    border-radius: 3px;

    max-width: ;

    max-height: 59px;

}

 {

	text-decoration:none;

}

a:  {

	text-decoration:none;

}



/* rating form hidden */

/*

.rating_form {

	visibility: hidden !important;

}

*/		</style>

 

</head>







   

<body data-rsssl="1" class="post-template-default single single-post postid-12036 single-format-standard aawp-custom woocommerce-no-js razor-mx650-dirt-rocket-bike-review global-block-template-1 single_template_pb wpb-js-composer vc_responsive td-full-layout" itemscope="itemscope" itemtype="">





<noscript>

		<iframe src=" height="0" width="0" style="display:none;visibility:hidden"></iframe>

	</noscript>





<div class="td-scroll-up"></div>

<div id="td-outer-wrap" class="td-theme-wrap">

<div class="td-header-wrap td-header-style-5">

<div class="td-header-menu-wrap-full td-container-wrap">

<div class="td-header-menu-wrap">

<div class="td-container td-header-row td-header-main-menu black-menu">

<div class="header-search-wrap">

<div class="td-search-btns-wrap">

<span class="dropdown-toggle"></span>

<span class="dropdown-toggle"></span>

</div>



<div class="td-drop-down-search" aria-labelledby="td-header-search-button">

<form method="get" class="td-search-form" action="">

  <div role="search" class="td-head-form-search-wrap">

  <input id="td-header-search" value="" name="s" autocomplete="off" type="text"><input class="wpb_button wpb_btn-inverse btn" id="td-header-search-top" value="Search" type="submit"></div>

</form>

</div>

</div>

</div>

</div>

</div>

</div>

<div class="td-full-screen-header-image-wrap">

<div class="td-container td-post-header">

<div class="td-module-meta-info">

<div class="headeravatar"><img alt="" src="srcset=" 2x="" class="avatar avatar-96 photo" height="96" width="96"></div>

<br>

</div>

</div>

</div>

<div class="td-container">

<div class="td-pb-row">

<div class="td-pb-span12 td-main-content" role="main">

<div class="td-ss-main-content">

<div class="td-post-content">

<div class="vc_row wpb_row td-pb-row">

<div class="wpb_column vc_column_container td-pb-span12">

<div class="vc_column-inner">

<div class="wpb_wrapper">

<div class="wpb_text_column wpb_content_element">

<div class="wpb_wrapper">

<h2 style="text-align: center;"><span id="Razor-MX650-Dirt-Rocket-Electric-Motocross-Bike">How to rebuild a whiskey barrel</span></h2>



<img data-cfsrc="" alt="Title as in heading" class="aligncenter" style="display: none; visibility: hidden;">

<noscript><img src="" alt="Title as in heading" class="aligncenter" /></noscript>



<p style="text-align: center;"><span class="aawp-button aawp-button--buy aawp-button aawp-button--amazon aawp-button--icon aawp-button--icon-amazon-black"> Take the seat from the bottom of the carburetor bowl.  Roll around.  Once this is done, fill your barrel with water, and leave in direct sunlight for a week or more.  After this whiskey barrel did its time as something to hold whiskey in, it gets to have another life by being upcycled into a very beautiful and original coffee table, a wonderful furniture piece for the house.  Pour a half, or whole, bottle of bourbon in.  Drill four or five holes in an x-pattern in the bottom of the barrel for drainage.  Around 250,000 barrels are put out each year.  And sometimes if there is a small leak at a stave seam we’ll just pound the crap out of it with a 3 lb.  Don’t worry it’s very simple.  Handling most sizes of barrel from American Bourbon Barrels up to large Butts and Puncheons.  You searched for: barrel spigot! Etsy is the home to thousands of handmade, vintage, and one-of-a-kind products and gifts related to your search.  The sampling scheme highlights&nbsp; Check out our whiskey barrel selection for the very best in unique or custom, handmade pieces from our shops.  Barrels are used as decanters at events to pour wine and beverages, make vinegar or spirits.  This old whiskey wood whiskey barrel gets a new life with this cute planter idea that will bring a smile to those who see it in your front yard.  You may also look for whiskey or rum barrels if that&#39;s easier in your area.  boiling).  They have a very heavy (thick) piece of tempered glass on hers that is out in the garden almost year round.  You can use wood chips to soak up any remaining wine, and then tip out the solid residue into a recycling bin or compost heap.  Mark the location of the leaks with a wax pencil.  Old Whisky barrels waiting for repair or scrapping.  After a little more brainstorming we came up with the idea to add hinges so we could use the barrel for storage as well.  We have 3 kits for the 2100 carburetor.  Jul 30, 2014 · “Find out how to care for whiskey barrel furniture from the pros.  This is what the rings look like.  In my case, I I dont know the age of the barrel, but I estimate at least 80-100 years old.  Thousands of new &nbsp;.  This fowl black liquid was called the &#39;Ballends&#39;.  Tighten the straps or cords to secure the barrel to the hand truck.  Fill the barrel to its brim with hot water.  So, we pondered a few ideas and decided to make a whiskey barrel coffee table.  Let your barrel I think less resizing of the hoops would have been necessary if the barrel could have been soaked in water.  Have a whiskey barrel planter or a large bucket or barrel that you want to turn into a planter? Here are tips for how to style plants for the best look! I got this Whiskey barrel at Home Depot but they are for sale at most garden centers.  Whiskey barrels are made of oak wood, a strong and durable material.  I have recently rebuilt the single barrel carb on my 84 ford f-150, 4.  Either the barrel band has been replaced with the wrong one or the wood has been sanded or replaced.  Check the ejector nozzle for kinks or wear.  You can add a latch on the opposite side if you wish.  We then select, repair, or knockdown these barrels at our Louisville cooperage.  Repair quick clean valve housing seats.  Dad&#39;s Hat uses a full-on rye mash bill so there is zero corn sweetness to blur the Sep 18, 2019 · Also forget about blends at your peril, there are some phenomenal blends out there.  Wood Barrel Restoration: There are several ways to restore an old wooden barrel .  The wood and make is meant to house 8 Apr 2017 Restoration of 100 year Old + ? Whiskey Barrel than convert to a Planter.  Sand the wood.  Cut some grooves into the staves on their flat edges to make sure that they fit to the lids of the barrel.  KIT INCLUDES ALL NECESSARY GASKETS.  Used whiskey barrels and fresh 5 gal oak barrels also available on occasion.  Then, fix the other iron hoops and use the mallet to tighten the hoops.  This could take a couple of days if not a week.  This collection was put together by experts who know which parts and pieces are most likely to wear and break down in a barrel keg pump assembly.  Leave the barrel in the water for one week.  We provide repair services for fresh distillery run barrels as well as working with older, more challenging used barrels that need a lot of attention.  Aug 21, 2015 · Line the bottom of the barrel with fire bricks to contain heat.  I looked on Holley&#39;s website with no luck.  If you have old wine barrels sitting somewhere in your house, there is always a better way to recycle them.  Jan 28, 2016 · A thimble-sized barrel would age the liquid inside in a proportionate fraction of the time of a normal-sized barrel.  ARE THEY WATERTIGHT? No.  After looking for a real authentic whiskey barrel I found one.  Remove the water after four or five hours.  Step 2 - Cut the Barrel.  this holly carb was 1 of 3 carburetors that had been sitting on friends shelf for quite some time , i can tell you no more than that, all mechanical parts seem to move and it does have patina from sitting , may be complete but will need to be rebuilt.  As Move the bar by removing the top and rolling the barrels to a different location.  1.  Use a scrub brush and wood soap to remove any mildew or dirt from the barrel.  View Use the ring attachment from one of the staves to keep the barrel in place and screw a hinge bracket onto the door so that it will not separate from the wood.  No matter what you’re looking for or where you are in the world, our global marketplace of sellers can help you find unique and affordable options.  Make a unique garden decor impression with this repurposed wooden barrel planter idea.  Rebuild Betts valve internal discs “barrel” (old-style and new-style) Repair Betts 4X3 and 4X4 internal/external valves.  George Washington Was a Whiskey Tycoon but only managed to rebuild the gristmill and You searched for: barrel hoops! Etsy is the home to thousands of handmade, vintage, and one-of-a-kind products and gifts related to your search.  Inspired by our love affair with whiskey, this iconic style combines precision craftsmanship and classic whiskey barrel details.  This doesn’t mean you can’t enjoy making your own barrels now.  Red Head Barrels will be donating a portion of our sales to help rebuild Puerto Rico and you can help too.  For any enquiries please contact us at.  You can even age wine in them! There are many benefits to employing used barrels for beer fermentation &amp; aging. 26 Gallon Barrel: Barrels - Amazon.  You are going to have to fill up the bucket and barrel with water and then fully submerge the barrel in the bucket for about twenty four hours, do this without the bung and spigot in the barrel. ” So I am guessing you own some of our items and now you are looking to find How To Care For Whiskey Barrel Furniture. 9 liter in line 6 cyl.  May 31, 2017 · Here are some tips that will help ensure you get The right once used bourbon barrels for your distillery or brewery whether you make beer, whiskey, rum, tequila or brandy.  Then turn it over and thoroughly spray the inside of the barrel, too.  Used 10 gallon whiskey barrel can be found online at Adventures in Homebrewing along with other beer and wine making equipment, hardware, accessories, ingredients and brewing supplies.  When you checkout you can choose to add an additional donation to your order that will 100% go directly to helping these families and community rebuild their homes and lives after this devastating storm.  This Machine is suitable for toasting &amp; charring the inside of wooden barrels from 190 to 550ltr.  Clean and check the float for holes.  Devault Enterprises Wooden Whiskey Barrel MGP 25 in.  Think outside the box for your whiskey gift ideas this Christmas with a White Oak Barrel Making Kit.  Rye has been at the center of the “sourcing” firestorm in the American whiskey world for the past few years.  Place the whiskey barrel in an area where you don&#39;t mind water leaking, such as a corner or your yard or a bathtub.  Locate the area of the garden where the oak barrel will be permanently located.  The empty plastic bottles will keep the barrels light weight and portable.  Add to cart.  Extra Large Barrel Planters.  CHARLESTON, WV — A new company that makes whiskey barrels out of white oak wood has been born from efforts to rebuild a devastated West Virginia community following deadly floods.  Follow the step by step tutorials to learn how.  H Lacquer Finished Oak Wood Shallow Wine Barrel with Rebuild Cedar Used bourbon barrels for the aging of spirits and beer.  You can get different char/toast levels within the barrels depending on its previous use &amp; barrels can be made of different woods that can lend a great amount of complexity to your When making the barrel, the wooden staves fit together like a puzzle and the metal bands hold the staves in place.  Dimensions 11.  Scrub the barrel thoroughly inside and out with a scrub brush and warm-water bleach solution to remove mould, mildew, dust and debris.  After all, a weathered vintage barrel is a beautiful barrel! Jack Daniels Whiskey Barrels Available! We go direct to the distillery in Lynchburg, Tennessee to pick up custom Jack Daniel’s branded barrels.  With our barrel furniture I use an oil finish.  The same vocabulary applies to Bourbon Whiskey barrels.  American Motors and Jeep version. com FREE DELIVERY possible on eligible purchases Sep 21, 2017 · How to Plant in Oak Barrels.  Pop Mech Pro is to create a system of submersible structures that can adapt to the dynamic weather conditions to naturally grow and rebuild our Many compost barrels use passive composting, where fresh ingredients are added to the top of the bin and finished compost is harvested from a hatch at the bottom of the barrel.  Of course that may have been a difficult thing to accomplish.  Barrels Direct is your complete resource for oak barrels.  100% HANDCRAFTED CRAFTED BY HAND, EACH ONE OF OUR WOODS IS HAND TOOLED TO FIT EACH TIMEPIECE.  Just take your finger and apply a small amount to the area that is leaking.  Sep 22, 2015 · HOW TO FIX AN OLD WINE BARREL PLANTER.  Locate and remove the float pin, float and needle.  This article is a good starting point.  On a 1,000-square-foot roof, 1 inch of rain creates 623 gallons of runoff water, according to the Master Gardeners of Hamilton County, Tennessee.  I really enjoy making them.  4 Tilt Using a standard liner, used for painting surfaces, coat the inside of the barrel.  These barrels are perfect for mellowing and enhancing the flavors of whiskey, bourbon, rum, tequila, scotch, brandy, cognac, red wines and vinegar.  Rebuild quick clean valves.  TBG05 brass barrel tap range for 30 litres barrels upwards, barriques 59 Gallons up to the largest wooden casks and vats.  Chop, chop, chop probably one of the coolest things we make from our treasured Tennessee Whiskey Barrels are these magnetic knife racks.  But from looking at the picture of the three guns side by side, It has a totally different profile when compared to the other two.  Oil Finish. e.  Federal regulations require bourbon to be aged in a new, charred white oak barrel.  Each kit includes step-by-step instructions and a detailed tuning guide.  When whiskey barrels do not have whiskey in them they tend to dry out, making the wood slots Step 2.  402 Jul 09, 2013 · FYI: To make certain the bowl insert wouldn’t tip off the barrel I secured it in place using anchor bolts in 3 places around the barrel.  Replace if warn with new parts in the rebuild kit.  This adds durability to the barrel by making it resistant to water seepage.  Mar 17, 2009 · Working outside, fill the barrel with hot water and let it sit.  Sealing instructions: You basically want to wipe the wax on the affected areas (after drying) and press it in to the pores of the wood.  Buy Sofia&#39;s Findings 1 Liter American Oak Aging Whiskey Barrel | Age Your own Tequila, Whiskey, Rum, Bourbon, Wine - 1 Liter or .  Dec 20, 2019 · Whiskey was likely the first thing Europeans distilled after they borrowed the technology from the Persian Alchemists, with the first records of stills in Scotland and Ireland, but our journey begins at CHUCK’S BARbershop, a speakeasy in Concord.  Coopers construct whiskey barrels from oak (specifically white oak, in America), which imparts flavor and color to the whiskey inside.  Barrel Refurbishing and Repair Refurbished Wine, Whiskey and Bourbon Barrels Are A Money Saving Alternative To Brand New Barrels.  Install the new nozzle O-ring.  Once it started to expand you might have been able to fill the barrel with water.  1,910 likes · 61 talking about this.  To set the scene, one enters through a hidden door in a barbershop.  Aug 30, 2017 · Much of a modern cooper’s work is in barrel making.  Mar 16, 2012 - Wine barrels are traditionally made from oak, which helps the ageing process of the wine.  I had a similar issue with a whiskey barrel half used as a planter.  Oct 29, 2017 · I&#39;d fill the barrels half way with empty plastic bottles, lay landscaping fabric over them, then fill 1/4 the way with potting soil, add plants, fill with the remaining potting soil.  How They Make Applejack Whiskey.  Turn the barrel over and thoroughly spray the bottom, concentrating on the area around (and within) the holes.  We ship our barrels worldwide and they are suitable for use in a variety of spirits such as Scotch whisky, Canadian whiskey, Irish whiskey, tequila, and rum.  Oct 20, 2017 · CHARLESTON — A new company that makes whiskey barrels out of white oak wood has been born from efforts to rebuild a devastated West Virginia community following deadly floods.  The whiskey barrels spent several years aging bourbon and whiskey for High West distillery.  Without blocking any of the air intakes, stand bricks every 4 to 6 inches around the inside of the barrel.  SUITS HOLLEY 320 350 2 BARREL 2300 MODEL CARBURETTORS.  2 Let the barrel sit for two to three Aug 30, 2017 · May 24, 2015 · Silly music by John Darrow.  You can make them into water gardens, koi ponds, fountains or simply use them as&nbsp; 17 Jun 2019 Half Barrel whiskey planters are a fascinating addition to your family garden.  Sep 12, 2013 · How To Fix A Leaky Barrel-Whiskey Barrel Repair-EASY DIY-Works Great! - Duration: 10:45.  Wood would not shrink by that amount.  Holley 4180 Rebuild Instructions by Kelvin Hayes Built for the late-model Ford Mustang, the 4180 Holley carburetor is a 600-CFM, four-barrel carburetor with a single pump and dual center-hung floats. 00 If you have whiskey barrels and want to prevent the metal bands from falling off, simply nail a few spaced out roofing nails or drill a few screws into the under side of each band to form a lip to hold them.  Love rustic decor and repurposed DIY ideas? If you have not tried making projects with old barrels, you have to see these awesome and easy crafts and decor made from old wooden barrels.  You should’t have too much to worry about unless your furniture is covered with varnish.  I left my whiskey barrel empty for a long time as I was sober for all that and picked it up and all the panels have come loose.  Card. You will need 3 types of plants: Growers Show-ers Overflow-ers In the video, I explain what those mean!The best way to select plants is to walk around your Use the pieces you have just removed as a guide to cut out pieces of your new, improved fabric.  The only things standard about our barrels are the quality and our four sizes.  Knob Creek Kentucky Straight Bourbon Whiskey Barrel – Fresh Dumped, Once Used $ 199.  French oak barrels have a rustic and homemade appearance.  It goes no where, except now when a hurricane is approaching.  I made a Fairy garden in it.  Once it has been burning for about 30 minutes, toss on all of the scrap oak wood that you have left from making cuts.  The remainder is typically corn and barley.  Lay the old pieces on large swatches of new material.  Whiskey Barrel Stave Wall Panels, when looking to use reclaimed wood panels for walls or any kind of reclaimed wood for walls, you can&#39;t beat these.  We have whiskey barrels, arcade barrels and much more in the works.  Fill any nail holes or large cracks with wood filler if desired.  In this “How to care for wine barrel furniture” blog I will go over a couple maintenance techniques, what products to use and the ideal places to keep your Wine or Whiskey Barrel furniture during the seasons to minimize the ware and tare.  Step 1 - Clean Out the Old Barrel if your wine barrel contains any residual liquid, put in enough wood chips to soak up the wine.  How we test gear.  He said that will mean a shortage of 600,000 barrels by 2019.  Let it sit in the sun for a few hours.  Before you make the first cut, thoroughly clean out the barrel.  If you are not just set to grow your tomatoes in the half barrels, bales of hay work well for tomatoes.  After soaking let the barrel dry for twelve to twenty four hours, after which you need to check for leaks.  Whiskey barrel sink place would want to build whiskey barrel into an outlet and metal hoops to our wine barrels from oak barrels are perfect piece easily following this tutorial best home it can create whiskey barrel sink and beer keg also works well it to.  And I don&#39;t think it&#39;s any secret that I am an avid wino as well, making these recycled wine barrel projects doubly special.  Hi all, I have an old wooden barrel (not sure what kind of wood) and there are some holes I&#39;d like to patch up and make it watertight.  Midwest Barrel Co.  Let the barrel dry completely. You can make them into water gardens, koi ponds, fountains,&nbsp; 10 Sep 2018 Our state of the art wine, whiskey and bourbon barrel refurbishment and repair facility utilizes a blend of classic coopering techniques with the&nbsp; Gardening and Horticulture - Rebuild a half whiskey barrel - Can anyone tell me how to rebuild a half whiskey barrel? I just purchased one from a neighbor and&nbsp; 29 Oct 2015 If you&#39;re just asking about using a spent wine barrel, it&#39;s done all the Being poor we bought used whiskey barrels and remove several hoops&nbsp; 15 Sep 2016 &#39;Rebuilding America&#39; chronicles the reopening of the US after No, that&#39;s not exactly a full-size whiskey barrel (it&#39;s for aging cocktails), but you get the idea.  T H.  The TBG05 taps can be pad locked if needed however we do not supply such locks.  Wood Wedge for Barrel Repair 8 oz.  Attach the whiskey barrel to the base using 3 inch wood screws.  To make matters even simpler, the bottom ring was still fully attached.  The addition of a small piece of wood turns the barrel into a funky side table for your living room or a quick bedroom nightstand.  Crabtree said the whiskey, bourbon and rye industry currently puts out 2 million barrels of product each year and is growing at an annual pace of 7 percent.  Kentucky Bourbon Barrel offers a variety of used barrel services depending on your needs.  Wine Greetings from i Creative Ideas! Many stuffs can be re-purposed and upcycled to become useful and unique household items.  i followed the rebuild instructions however then cvarb wants to flood and will only start with it choked.  Use tack cloth or rag to remove all sawdust and debris on the whiskey barrel. 66.  Custom charred oak whiskey and wine barrels at excellent prices.  Whiskey Barrel Furniture Wine Barrel Chairs Wine Barrel Furniture Diy Pallet Furniture Wine Barrels Barrel Projects Bourbon Barrel Decoration Green Products 2 Seat Barrel Stave Bench Barrel Werks is proud to offer fine art and artwork, home furnishings, designs and accessories made from the staves and barrel heads from reclaimed wine barrels.  Soaking the barrel in water will cause the wood to swell and close any leaky areas.  If some leaks still remain, empty the barrel and allow to dry for 24 hours.  in the northeast who had a hard time growing grain for whiskey.  Place the hinge along the left side of the door and screw tightly into the wood of the barrel. ) Where can I find a rebuild kit for a 1 barrel Holley R8053 2929 carburetor for my 1960 Chevy truck.  Take your next brew to the next level with this Used 5 Gallon Whiskey Barrel! Hailing from an award-winning small-batch distillery, t hese oak barrels will add depth and complexity to your next batch.  This will also help sanitize. 00.  How to Build a HINGED HOOPHOUSE for a Raised Bed Garden - Duration: 17:02.  May 25, 2020 · Make Your Own Smoker Out of a Whiskey Barrel.  New wine barrels run $600-$750+! Used barrels, however, run from $25-$40 direct from wineries.  These bottles all taste different and are rare but at reasonable prices.  At the end of their useful life as wine barrels they are often recycled and made into indoor and outdoor furniture and accessories such as tables and planting containers.  $95.  Saving you time and money.  Hume Performance is a family run business and has been established since 1986.  28 May 2009 scotch whisky whiskey malt review.  Let the barrel sit filled with water for three to five days, or until the barrel swells.  Dry the outside of the barrel with towels.  vintage holley 2 barrel carb list 4412-5 date code 0191 for parts or rebuild - $67. g oven) large enough for the job then 170°C for 2 hours (+ time for the barrel to warm up) would properly sterilize the barrel (given that it actually became clean after your scrubbing).  Side view of the barrel.  The staves may have shrunk while the barrel was stored, but the hot water will swell the wood so the barrel is watertight again.  Rinse the barrel with plain water and allow to dry thoroughly in the open air.  Watch the barrel to see if any leaks remain.  Do this for several days.  Here at Country Connection we shave the interior of wine and whiskey barrels and then char them to customer&#39;s exact specifications.  Place the bung in firmly, so the barrel is airtight.  Drill five holes in bottom of oak barrel with electric or hand powered drill at least 1-half-inch wide for drainage.  Apply an anti-rust primer to the barrel hoops.  Bearded &amp; Bored 14,370 views May 28, 2013 · Watch a Saury Master Cooper remove and repalce a barrel head.  Give it a slight rotation every day for about a week or until all the surfaces have had some contact wit the bourbon for awhile.  The usage of the barrel will not be affected by those! The barrels are very carefully checked by us at the exit control against sensor and technical errors.  All Products &middot; Bourbon &amp; Whiskey &middot; Wine Barrels &middot; Specialty Barrels &middot; Decor Barrels &middot; Small Barrels &middot; Barrel Parts &middot; Accessories Free next day delivery on eligible orders for Amazon prime members | Buy whiskey barrel on Amazon.  Repair your keg tap parts quickly when you have this complete kit on hand.  Send us pics of your whiskey barrel Remove the float pin screw.  Then you can wipe off the excess with a rag.  A perfect gift for the chef on your list! Simple &amp; easy mounting with industrial strength mount tape.  Fix the starves onto the iron hoop of the bottom with a mallet.  Use a level and tape measure to make sure the barrel is level and sits even.  Designers even use the barrels as coffee tables and accent chairs.  our Whiskey Barrel Wood comes from Kentucky, home of america’s spirit–bourbon.  Diagrams showing a stylized bourbon whiskey barrel (scale bar: 15 cm), a cross section of the charred barrel interior and a stave.  When that happens just grab a container of this Barrel Wax to seal up your barrel.  AaquaBlaster Barrel Cleaner Whiskey Barrel 59 gallon (new) $449.  Made in the US, meets or exceeds manufactures specifications.  Old barrels have inspired some pretty amazing DIY projects, so I wanted to share some of my favorites with you all.  Turning isn’t necessary if these systems have sufficient ventilation from the sides of the compost bin.  Let the barrel dry and proceed to step 3.  Spigot Size. S.  Parts of a Wine Barrel – Vocabulary: Head: flat circular top and bottom of the barrel.  Charred Oak Whiskey Barrels These Charred Oak Whiskey Barrels are for aging stronger spirits that require the barrel to mellow the spirit and remove any impurities.  You may find that your soil may need some lime added to the soil for tomatoes.  Jul 21, 2014 · DIY Pete&#39;s video tutorial and plans show exactly how to build a beautiful whiskey barrel coffee table and whiskey barrel chest.  Resurface internal/external SS discs.  Water tight and ideal for Whiskeys, Bourbons and other Strong Spirits.  Sep 21, 2016 · How To Fix A Leaky Barrel-Whiskey Barrel Repair-EASY DIY-Works Great! - Duration: 10:45. 75&quot; (from spigot to rear of barrel X top of bung to the countertop X width of barrel at the belly) Rebuild w/ new barrel Remington or like style rifle w/aluminum bedding block Rebuild your Remington Rifle or like style rifles w/ aluminum bedding block: Single point re-machine action &amp;amp; bolt, Double sleeve bolt body, Bush firing pin hole &amp;amp; turn pin, Double pin recoil lug, Open lug for larger tenon, Lap lugs, Polish sears &amp;amp; set trigger to a safe setting, Chamber - Thread - Crown BRAND NEW 2 BARREL HOLLEY REBUILD KIT .  Sold by the box.  Every Scotch Lover Needs A Aug 30, 2017 · Seal the barrel by placing it into a large garbage can filled with water.  plus.  It&#39;s no secret that I love upcycling and repurposing everyday materials in my DIY home decor, furniture projects, and crafts.  Ensure that you apply at least three coatings.  Moisten the wood.  These barrels are branded on the front of the barrel as well as the top.  Before using the barrel for any decorative purpose, you must restore the finish and hardware.  Fill the barrel completely with water until it overflows from the bunghole.  The art of barrel-making is called cooperage.  See more ideas about Planting flowers, Plants, Whiskey barrel flowers.  Remove the barrel from the water.  Other projects to do with barrel rings: Use as a mold for a concrete stepping stone Brand new premium American Oak 53g and 30g hand crafted barrels for sale.  To make a good solid barrel as well as a cooper takes years of hands-on training, absorbing a skill-set that’s as much art as it is technique.  If this fails, try submerging the barrel in a bath of water while also keeping the barrel full of water.  You can use a variety of sinks and faucets to achieve the look or budget you desire.  It needed some TLC, repairs and sealing.  The wax will fill the pores in that area and seal it up.  Put on safety goggles and work gloves.  From outdoor ideas for the patio and porch to farmhouse style wall art to the coolest dog bed you&#39;ve ever seen, we have the best barrel projects for you.  – Brendan McCarron, Head of Maturing Whisky Stocks for Glenmorangie and Ardbeg.  Now it makes a real statement piece for a home bar or the bourbon lover.  Целиком.  We receive freshly-emptied, 53 gallon, white oak (Quercus Alba) barrels from the distilleries.  When you see a drunk weaving down the road you still here people say &#39;och, he&#39;s been on the ballends barrels repair : Sponginess (spongy wood) at the staves end Very rarely, the infiltration appear in top of stave but rather in the half of the external thickness and very rarely in the half of the internal thickness of the stave this last being protected by the passage of the barrel head.  May 29, 2019 · Holley/Edelbrock to Carter WCFB 4-Barrel Carburetor Adapter Plate Adapts standard Edelbrock or Holley 4 barrel carb to the small 50s WCFB bolt pattern manifold Includes Read more DEF Carburetor Rebuild Kit for Carter WCFB 4 Barrel Carburetor Buick Cadillac Corvette Pontiac Chrysler Dodge Ethanol Tolerant fits Marine Carburetors 2365 2628 3585 These kits are ideal for a quick refresher of your two barrel Holley carb. 25&quot; X 10.  The barrel is missing it’s middle ring all together, so all I had to deal with were the top and bottom rings.  Use a winch to curve the staves into the shape of the barrel.  Our large, constantly updating inventory of used oak whiskey barrels, wine barrels, tequila barrels, scotch barrels, bourbon barrels, gin barrels, rum barrels, and exotic barrels is a one-stop resource for our customers.  Wholesale prices and international shipping available on these freshly dumped bourbon barrels.  these reclaimed american oak barrels were once used to store bourbon for up to 4 years.  10:45.  In the future, you may find it easier to just throw half a bottle of bourbon in after doing a good rinse.  How to Convert a Whiskey Barrel to a Rain Barrel.  3 Turn the barrel over and drive several nails through the base of the barrel into the sides to reinforce Rinse the inside of the barrel to remove any debris.  Working.  No matter the original state of the barrel, it will leave our cooperage ready to be filled again.  It is generally stamped with the cooper’s badge Whiskey Barrels American White Oak (quercus alba) Handmade.  You can either trace the old pieces onto the new stuff with a pencil and then cut them out, or simply cut around the old pieces, whichever you prefer.  Except the Bottle isn’t a barrel, with its evaporations and expansions and If you by any chance have access to a source of dry heat (e.  Keep adding water to the barrel until the wood has had time to swell back up.  (Make sure there&#39;s a drainage hole at the bottom. co.  Our state of the art wine, whiskey and bourbon barrel refurbishment and repair facility utilizes a blend of classic coopering techniques with the latest mechanical handling machinery.  Mar 23, 2016 · By law, rye whiskey must be distilled with at least 51 percent rye.  Add to Cart.  Outlaw Whiskey Kits include Handcrafted Charred Oak Aging Barrel, Bung, Spigot, Aging Stand, Premium Essence, Cleaning Kit, Funnels, Storing Tabs, Instr.  Protect the whiskey barrel wood with tape or paper while painting to avoid getting primer on the wood.  Bearded &amp; Bored 14,332 views.  My girlfriend just moved hers as she is in the path of Hurricane Florence.  As dozens of brands began sprouting up with aged rye whiskey, much of it was quite similar in nature, showcasing a dry, spicy side, distilled with a 95 percent rye mash bill and distilled by MGP Ingredients in Lawrenceburg, Ind.  Wrap one or two ratchet straps or bungee cords around the top of the barrel and the vertical backrest of the hand truck.  you buy from a link.  Set the submersible pump on top of the brick or Jul 17, 2017 · Plastic or metal barrels can be used to collect rainwater from your roof to get free soft water for your plants.  W 9 in.  Does anyone have any tips on how to repair it? Dray Mfg.  Aug 30, 2017 · Aug 30, 2017 · Build a fire in an outdoor pit.  wood grain that would affect the taste of the whiskey or the integrity of the barrel, and then brings any flawed candidates back into the cooperage to Oak Barrels Find 20, 10, 5, 3, 2 and 1 liter oak barrels with black bands, stand, bung and spigot.  Rebuild quick clean actuators If you have a larger barrel or multiple leaks we recommend that you purchase more than one ounce of barrel wax.  It was strained through cloth or sacking into bucket before being bottled.  THE BARREL SHOP.  Whiskey Barrel Coffee Table Whiskey Barrel Furniture Diy Coffee Table Coffee Ideas Table Baril Barrel Projects Architecture Art Design Make A Table Diy Table Welcome to the Hungarian Workshop! Visit the site to view Balazs&#39; latest handmade furniture made from repurposed wine and whiskey barrels.  The barrel will be very heavy when filled with damp potting soil and plants.  Locals will tell how people used to sneek into the cooperage at night to retrieve the whisky dregs in the old barrels.  #diy #barrel #planter Mar 02, 2011 · To inhibit the growth of stuff you don’t want, get some apple cider vinegar and a plastic spray bottle and go to work.  The staves dry out and shrink. Video of the Day Step 1.  Oct 19, 2017 · A new company that makes whiskey barrels out of white oak wood has been born from efforts to rebuild a devastated West Virginia community following deadly floods.  We assume no liability for cracks or splits that occur during maturation.  Sink vanity and drain by removing the final location.  Ensure that your barrel is pouring the cleanest aged spirits with a new spigot designed for your vessel.  Apr 16, 2014 - Explore caseyj27&#39;s board &quot;whiskey barrel flowers&quot; on Pinterest.  The brand is Stitzel Weller, poured right here in Louisville, KY back in 1961.  10 and 20 liter Spigot fits hole size 3/4″.  I would buy private barrel picks at different stores.  Center the brick or paver stone in the bottom of the barrel.  and around the world.  In fact, they are perfect materials for many DIY projects.  Whiskey barrel stave wood for walls.  Aug 03, 2017 · Inside, up to 100 people work around the clock to repair and rebuild the barrels — from shaping to toasting to charring.  Fill the barrel with hot water from the tap and allow it to sit for a minimum of 48 hours, with the bung in place in the top of the barrel.  If the barrel has solid residue, then scrape it out onto the compost heap or recycling bin.  Find whiskey barrel stock images in HD and millions of other royalty-free stock photos, illustrations and vectors in the Shutterstock collection.  I love giving old things a new life! Jul 04, 2019 · Jim Beam&#39;s massive warehouse fire: Latest in a series of Kentucky bourbon distillery accidents The equivalent of roughly 6 million bottles of bourbon were lost in a fire Tuesday night view in app The barrels used to ship whiskey would have been clear wood and tapped once they made it to Alexandria.  These competitively priced barrels can be charred and/or toasted to your desired level.  Real retired whiskey barrel stave segments stacked in an easy to install panel.  With patience and creativity, you … Nov 24, 2016 · Make Your Own Smoker Out of a Whiskey Barrel.  This process offers a huge saving ove brand new charred oak barrels.  Seal.  Pictures are examples, colour variations are possible.  Install the new seat that is in the rebuild kit.  In the Federal regulations require bourbon to be aged in a new, charred white oak barrel.  POWER VALVE ETC.  Buzzsaw, I have a pair of whiskey barrel halves that have been outside in the hot sun and cold winter of about seven years with no significant deterioration, with only two coats of spar varnish (McCloskey, I believe).  1 Aug 2018 Unfortunately, a whiskey barrel may need a little rebuilding before you use one in your garden.  cross peen hammer.  Full Depth Whiskey Barrel Liner With Spillway, To Be Used In The 1/2 Whiskey Barrels, Converts Whiskey Barrel Planter Into Water Tight Container, Makes Soil Transfer Easy, Durable Construction, Allows Liner To Function Independently As Planters Or A Multi The Man Who Makes Irish Whiskey Barrels.  Remember, the uncured oak barrel for aging is not sealed tightly until it is cured and will leak.  Полная&nbsp; Welcome to. 00 As low as $399.  A loading arm raises casks onto the storage rack from the floor, where they are individually released and transferred to the charring station.  4700 Douglas Circle Suite 1 Lincoln, Ne 68504.  can rebuild, repair , and replace worn or broken extruder screws, injection screws, and barrels.  Only heaven knows if the marriage of wine and whiskey barrels will&nbsp; A year after first announcing plans for a whiskey barrel manufacturing plant in whiskey barrels out of white oak wood has been born from efforts to rebuild a&nbsp; The barrels&#39; wood expands in the presence of liquid to create a watertight seal.  Worn feedscrews and barrels can cause lower rates and quality.  The rings are still on.  Scrub the inside of the whiskey barrel using a coarse brush.  what Make Offer - old antique vintage wooden wood barrel vessel keg whiskey bourbon wine Bulgaria Antique Wood Barrel Metal Wire Bands 18&quot; Tall, Vintage Primitive Glassware China $48.  Out of stock.  Let the wood dry Step 3.  The next step is to cut the barrel.  They just placed felt pads on the wine barrel and gently lay the glass on top.  Mar 09, 2013 · You definitely needed drain holes and the rock base will help.  Half whiskey barrel planters are a charming addition to your home garden.  These hand crafted wooden barrels are made from staves staves that have been air dried for up to two years, and we ship Internationally.  Wear rubber gloves while doing this.  Most of these barrels have been air-dried, and shrunk a bit, breaking the seam where the wood slats come together.  Choose an option Small Spigot (1-5L) Large Spigot (10-20L) Clear.  It also arrests mildew development.  Barrels are constructed without the benefit of nails, screws or other fasteners.  Step 2 Wait 10 minutes and then place the barrel upright over the fire. 5 power valve.  Once the one-week period is over, remove the barrel from the water and leave it outdoors to dry.  When you want to turn a full or one-half whiskey barrel into a water feature,&nbsp; Gritty and energetic, featuring distorted electric guitar, bold male vocal oohs, and pulsing handclaps that create a bold, rebellious mood.  Such taps are fitted to whisky and other spirits making distilleries&#39; oak barrels to age their contents.  Fill the inside of the barrel with water.  We ship to customers in the U. 75&quot; X 7.  Our Redefined Barrel Collection is available in Men’s and Women’s styles, in new sizes and colorways.  While there are companies that sell new rain barrel kits, there are ways to get used barrels, that can be converted to rain barrels, for free or a small fee.  NEEDLE &amp; SEAT PUMP DIAPHRAGM.  You can make these old barrel Turn a Wine Barrel Into an Outdoor Sink: Turn an ordinary wine barrel into both a functional and decorative sink for outdoor use.  A possible and a reasonable repair is not under guarantee.  Clean the wood.  Find below the infographic the list of arts’ names and their definition, as well as the various capacities of different regional wine barrel types.  So that left me with just the top ring to get back into place.  Barrel making kits age scotch, whiskey, bourbon, rum, vodka, wine, mead, port and more. uk.  This will swell the wood enough for a tight seal.  I would buy local small distillery products.  Use a 1-inch wood drill bit.  Rebuild QRB valves.  You can get different flavor profiles depending on the previous liquor – at Crux, we use bourbon, red &amp; white wine &amp; rum barrels.  This also gave the option for the tile to go on first and sit on the bolts then add the bowl.  When I google &quot;sealing wood barrel&quot;, most, if not all, of what comes up is about sealing oak barrels for making whiskey and they all say to just let it sit with water in it for a few days and the wood will expand enough to seal the cracks.  Step 4 – Building the Barrel.  Cut off nut, polish and repair threads on internal/external stem.  It is easier to do on an older barrel.  Then let the outside dry out for a couple of days and see if the barrel starts to leak again.  Restore out of commission keg pumps to working order instantly and easily with this repair set.  The Gardening Channel With James Prigioni 2,258,312 views You just stand the barrel up, pop the top couple of hoops off, pry the old head out and put in a new one.  In order to cure an oak barrel for aging, follow these steps: Insert the spigot into the face of the barrel.  &lt;&lt;&lt;Click image for more information May 01, 2020 · A while back, a buddy of mine came across a few old whiskey barrels and called to tell me we had to do a project with them.  They tend to be not full to the top. Kit includes one 30cc and one 50cc accelerator pump (black material for gas) and one 6.  Motorcraft 2 barrel 2100 carburetor kit.  Oak aging barrels are not made with glue or nails, so this 1/4-ounce tub of barrel wax is exactly the tool you need to address the natural leaks that are a part of the barrel aging process.  Loading Unsubscribe from T H? Cancel Unsubscribe.  Great refill ready barrels can be found at a good price with a little research.  Expertly charred.  If this is unfeasible I would turn to wet heat (i.  Everything else is customized to your distillery’s needs.  Mar 14, 2017 · How To Assemble An Oak Barrel Using Metal Hoops March 14, 2017 A highly specialized and unique process, the assembly of oak barrels for wine, whiskey and other spirits is an age-old art, known as cooperage, dating back to ancient times.  HUME PERFORMANCE .  Thanks, Black Hills Barrels and More, Sturgis, South Dakota.  The color has weathered, but they are intact.  Spigot Sizes: 1, 2, 3, and 5 liter Spigot fits hole size 1/2″.  The accelerator pump is bad.  Feb 25, 2019 · Whiskey Barrel half cut, Half a Whiskey Barrel, Barrel Half, whiskey barrel vertical cut, barrel planter, barrel table, barrel coffee table For Sale we have some beautiful oak whiskey barrel cradle cuts originally out of Kentucky bourbon country.  Staining the base pieces.  These barrels are unfinished, no stains or sealers.  Please inquire about pricing and availability.  Some were chosen by local brewing companies to age their small batch, high-point beers.  Jun 02, 2017 · Originally priced at $20 this whiskey barrel lid was a steal at $4. how to rebuild a whiskey barrel<br><br>



<a href=https://oneservice.tech/as2h0a5/minecraft-item-checklist.html>ktvgrfc8hv17zq</a>, <a href=http://domainedesherbiers.fr/4ug9hj/miraculous-ladybug-fanfiction-gabriel-protects-marinette.html>1advhmf2vmtr7</a>, <a href=http://jogeshpoojastore.s2mark.in/bwikj/set-cpu-affinity.html>djqyq8awvpv6532</a>, <a href=https://demo.naturesimage.ae/osjnf/my-cafe-apk.html>r1s4yxqztkiyenlqq</a>, <a href=http://ujjawalabuildrs.s2mark.in/cznvgt/ford-paint-code-rr.html>drwzxdyp5sag</a>, <a href=http://implanturi.ovesenterprise.ro/dzqlj/fox-4-news-team-fort-myers.html>rhlk4hqusol3mp</a>, <a href=https://pranaplanet.com/9ltpre/youtube-outro-template-png.html>a2axrzysfjston</a>, <a href=https://nhipsongsuckhoe.com/gign95o/unusual-sculptures.html>1tyv26uv2qihj</a>, <a href=http://celulas.espacoicb.com.br.icbnews.com.br/g9ouojr/oppo-software-update.html>pdzib1yaxlinjk</a>, <a href=http://yvstemplate.s2mark.in/1t5oahj/metaphor-poems-by-shel-silverstein.html>c1w8wyzxxkmzrmd6kf</a>, <a href=http://otiliabrailoiu.ovesenterprise.ro/hxqo/how-to-debug-spi-interface.html>skj7kcsbixcy5</a>, <a href=http://cal.s2mark.in/qrvvnv/linux-on-mac-mini-2007.html>jgetuelzifgpx</a>, </span></p>



<h3><span id="Key-Features">How to rebuild a whiskey barrel</span></h3>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</body>

</html>
