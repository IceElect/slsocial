<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {translate} function plugin
 *
 * Type:     function<br>
 * Name:     birthday<br>
 * Date:     Sen 17, 2017<br>
 * Purpose:  
 * Examples: {birthday date="0000-00-00 00:00:00"}
 * Output:   
 * Params:
 * <pre>
 * - date        - (required) - birthday date
 * </pre>
 *
 * @author Elect (slto.ru/elect)
 * @version 1.0
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 * @uses CI translate library()
 */
function smarty_function_decline($params, $template)
{

    $text = '';
    $number = '';
    $eArray = array();
    foreach($params as $_key => $_val) {
        switch ($_key) {
            case 'text':
            case 'number':
            case 'eArray':
                $$_key = $_val;
                break;
        }
    }

    $text = preg_replace('%\%n\%%', $number, $text);
    if(isset($eArray[0])){
        $text = preg_replace('%\%o\%%', getNumEnding($number, $eArray), $text);
    }else{
        $text = preg_replace('%\%o1\%%', getNumEnding($number, array('й', 'я', 'ев')), $text);
        $text = preg_replace('%\%o2\%%', getNumEnding($number, array('ый', 'ых', 'ых')), $text);
        $text = preg_replace('%\%o3\%%', getNumEnding($number, array('ка', 'ок', 'ых')), $text);
        $text = preg_replace('%\%o4\%%', getNumEnding($number, array('ая', 'ых', 'ых')), $text);
        $text = preg_replace('%\%o5\%%', getNumEnding($number, array('ь', 'и', 'ей')), $text);
    }
    echo $text;
}

function getNumEnding($number, $endingArray)
{
    $number = $number % 100;
    if ($number>=11 && $number<=19) {
        $ending=$endingArray[2];
    }
    else {
        $i = $number % 10;
        switch ($i)
        {
            case (1): $ending = $endingArray[0]; break;
            case (2):
            case (3):
            case (4): $ending = $endingArray[1]; break;
            default: $ending=$endingArray[2];
        }
    }
    return $ending;
}

?>