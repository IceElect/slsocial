<div class="notify" id="notify_over_{$notify.id}">
	<a class="notify-inner" href="{$notify.link}" data-type="load">
		<div class="notify-photo">
			{get_avatar src=$_user->avatar_file}
		</div>
		<div class="notify-content">
			<div class="notify-title">
				{$_user->fname} {$_user->lname}
			</div>
			<div class="notify-content">
				{$notify.text}
			</div>
		</div>
	</a>
</div>