function nav(){
    var self = {};
    self.additional = false;

    self.history = function(url, e){
        history.pushState(url, document.title, url);
    }

    self.load = function(url, e, push){        
        e.preventDefault();

        var prev_url = location.protocol + '//' + location.host + location.pathname;

        var element = document.querySelector('.page-content');
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                var data = eval('('+xhr.responseText+')');
                $("abbr.time").timeago();

                document.title = data.title;

                if (push){
                    history.pushState(url, document.title, url);
                }


                element.innerHTML = data.html;

                self.setA(data.additional);
                eval(data.func);

                $(document).find('.wall-posts').infinitescroll({
                    navSelector  : "div.nav",
                    nextSelector : "div.nav a:first",
                    itemSelector : ".wall-posts .post-holder",
                    loading: {
                        finishedMsg:'',
                        img:'/logo.png'
                    },
                });

                $('html, body').animate({
                    scrollTop: 0
                }, 100);

                if($('body').width() <= 768){
                    var o = $('.left-wrap .app-content');
                    if(o.hasClass('show')){
                        $('body').css('overflow', 'auto');
                        width = o.find('.app-content-wrap').width();
                        o.animate({ left: '-'+parseInt(width + 20) }, 200).removeClass('show');
                    }
                }

                document.body.classList.remove('loading');
                popup.init();
                //if($(".messages").length > 0){
                //    messages.init();
                //}
            }
        };

        xhr.open('get', url, true);
        xhr.setRequestHeader('Content-Only', 1);
        xhr.send();

        document.body.classList.add('loading');
        return false;
    }
    self.init = function(){
        self.main = document.querySelector('.main>.table>.row');
        window.addEventListener('popstate', function (e) {
            self.load(e.state, e, false);
        });

        history.replaceState(location.href, document.title, location.href);
    }
    self.setA = function(a){
        self.additional = a;
    }
    self.getA = function(key){
        if(self.additiona){
            return self.additional.key;
        }
    }
    return self;
}
var nav = new nav();
nav.init();
$(document).on('click', '[data-type=load]', function(e){
    
    nav.load($(this).attr('href'), e, true);

    return false;
})

$(document).on('click', '[data-type=load_sidebar]', function(e){
    load_sidebar($(this).attr('data-id'));
    return false;
})

function error(text){
    alert(text);
}

function load_sidebar_p(id, params){
    $(".app-content").addClass('loading');
    $.post('/ajax/user/sidebar/'+id, params, function(data){
        data = eval('('+data+')');
        if(data.search){
            $(".app-content .search").show();
        }else{
            $(".app-content .search").hide();
        }
        $(".app-content .tab-content").html(data.html);
        $(".app-content").removeClass('loading');
    });
}
function load_sidebar(id){
    $(".app-content").addClass('loading');
    $.post('/ajax/user/sidebar/'+id, function(data){
        data = eval('('+data+')');
        if(data.search){
            $(".app-content .search").show();
        }else{
            $(".app-content .search").hide();
        }
        $(".app-content .tab-content").html(data.html);
        $(".app-bar a.app-bar-link.selected").removeClass("selected");
        $(".app-bar a.app-bar-link[data-id="+id+"]").addClass("selected");
        $(".app-content").removeClass('loading');
    });
    setTimeout(function(){
        sidebar = id;
    }, 200);
}

$(document).on('keyup', '.search .search-field', function(e){
    e.preventDefault();
    if(e.keyCode == 9 || e.keyCode == 16 || e.keyCode == 17 || e.keyCode == 18 || e.keyCode == 20 || e.keyCode == 91) return;
    var search = $(this).val();
    if(search.length == 0){
        load_sidebar(sidebar);
        return false;
    }
    load_sidebar_p('search', {search: search});
})