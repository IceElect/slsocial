<?php

class Im extends Default_controller
{

    function __construct()
    {
        parent::__construct();
        $this->setActiveModule('im');
        $this->frontend->add_body_class('im');
        if(!$this->oUser->id)
            exit;

        $this->load->model('dialog_model', 'dialog');
        $this->dialog->setUser($this->oUser->id);
    }

    function dialog($id, $is_im){
        $id = intval($id);
        $is_im = intval($is_im);

        $dialog = $this->dialog->getIm($id);
        $messages = $this->dialog->getMessages($dialog->id);

        $this->my_smarty->assign('dialog', $dialog);
        $this->my_smarty->assign('messages', $messages);

        $this->frontend->setFunc('messages.init("'.md5($dialog->id.$this->oUser->id).'")');
        $this->frontend->setAdditional(array('dialog' => $dialog->id));

        $this->frontend->view('dialog');
    }

}