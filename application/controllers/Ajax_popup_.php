<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_popup extends Default_Controller {
	private $response = array('response' => false, 'html' => '');
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		if(!$this->user->is_logged())
			exit;

		$this->user_id = $this->session->userdata('user_id');
	}

	public function index()
	{
		$this->frontend->view('welcome');
	}

	function confirm($type){
		$this->response = $this->frontend->fetch('popup/confirm_'.$type);
		echo $this->frontend->returnJson($this->response);
	}

	function actionPhotoOld(){
        $this->load_model('OAlbums');
        $this->OAlbums->setUser($this->user_id);

        $this->pm->add_action('getPhotoComments', array($this->OAlbums,'getPhotoComments'));

        $response = array('response' => false);
        $photo = $this->OAlbums->getPhoto($_POST['photo']);
        $this->templater->Bind('photo', $photo);
        $response['response']['html'] = $this->frontend->fetch('popup/photo');

        echo json_encode($response);
    }

    function photo($id){
        $this->load->model('Photo_model', 'OAlbums');
        $this->OAlbums->user_id = $this->oUser->id;

        //$this->pm->add_action('getPhotoComments', array($this->OAlbums,'getPhotoComments'));

        $response = array('response' => false);
        $photo = $this->OAlbums->getPhoto($id);
        if($photo){
        	$this->my_smarty->assign('photo', $photo);
        	$response = $this->frontend->fetch('popup/photo');
    	}

        echo json_encode($response);
    }

    function photo_upload($type){
        $response = array('response' => false);

        $this->my_smarty->assign('album', $this->input->post('album'));

        switch ($type) {
            case 'upload':
                $response = $this->frontend->fetch('popup/photo_upload');
                break;
            case 'avatar':
                $response = $this->frontend->fetch('popup/avatar_upload');
                break;
            default:
                # code...
                break;
        }

        echo json_encode($response);
    }

    function avatar_crop(){
        $aData = $this->input->post();
        $response = array('response' => false);

        $response = $this->frontend->fetch('popup/avatar_crop');

        echo json_encode($response);
    }

    function attach_photo(){
        $response = array('response' => false);

        $this->load->model('photo_model', 'model');
        $photos = $this->model->getPhotos(false);

        $this->my_smarty->assign('photos', $photos);

        $response = $this->frontend->fetch('popup/attach_photo');
        echo json_encode($response);
    }

}