<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {translate} function plugin
 *
 * Type:     function<br>
 * Name:     birthday<br>
 * Date:     Sen 17, 2017<br>
 * Purpose:  
 * Examples: {birthday date="0000-00-00 00:00:00"}
 * Output:   
 * Params:
 * <pre>
 * - date        - (required) - birthday date
 * </pre>
 *
 * @author Elect (slto.ru/elect)
 * @version 1.0
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 * @uses CI translate library()
 */
function smarty_function_birthday($params, $template)
{
    $monthes = array(
        '01' => 'Января' , '02' => 'Февраля' , '03' => 'Марта' ,
        '04' => 'Апреля' , '05' => 'Мая' , '06' => 'Июня' ,
        '07' => 'Июля' , '08' => 'Августа' , '09' => 'Сентября' ,
        '10' => 'Октября' , '11' => 'Ноября' ,
        '12' => 'Декабря'
    );

    $date = '';
    foreach($params as $_key => $_val) {
        switch ($_key) {
            case 'date':
                $$_key = $_val;
                break;
        }
    }

    $birthday = date('d', strtotime($date));
    $birthmonth = date('m', strtotime($date));
    $birthyear = date('Y', strtotime($date));

    if($birthmonth > date('m') || $birthmonth == date('m') && $birthday > date('d'))
      $age = (date('Y') - $birthyear - 1);
    else
      $age = (date('Y') - $birthyear);

    echo $birthday.' '.$monthes[$birthmonth].' '.$birthyear.' '.'('.$age.' лет)';

}

?>