function Notify(){
	var self = {};
	self.socket = false;

	self.soundPlay = function(type){
        new Audio('/notify'+type+'.wav').play(); return false;
    }

	self.init = function(id, hash, socket_url){
		if(typeof(io) != undefined){
			self.socket = io.connect(socket_url);
		}
		if(self.socket){
			self.socket.emit('auth', {id: id, hash: hash});
			self.socket.on('notify', self.add);
			self.socket.on('message', messages.addTo);
		}
	}

	self.add = function(data){
		console.log(data);

		html = data.html;
		$(".notify_over").prepend(html);

		self.soundPlay(2);

		handlePageCount(data.id, 5, 'http://gogle.com', true);
	}

	return self;
}
var Notify = new Notify();