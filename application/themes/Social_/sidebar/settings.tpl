<div class="users-list">
    <ul>
        <li class="user-item dialog-item">
            <a href="/@{$oUser->id}" class="avatar middle" data-type="load">
                {get_avatar id=$oUser->id}
            </a>
            <a href="/@{$oUser->id}" class="user {if ($oUser->last_action >= ($time - 900))}online{/if}" data-type="load">
                <span class="name">{$oUser->fname} {$oUser->lname}</span>
                <p>0 уровень</p>
            </a>
            <!--
            <div class="user-button">
                <button class="md-icon">textsms</button>
            </div>
            -->
        </li>
    </ul>
</div>
<ul class="app-content-menu">
    <li>
        <a href="/edit" data-type="load">
            <button class="md-icon">info_outline</button>
            <span>Информация</span>
        </a>
    </li>
    <li>
        <a href="/settings" data-type="load">
            <button class="md-icon">build</button>
            <span>Настройки</span>
        </a>
    </li>
    <li>
        <a href="/users/logout">
            <button class="md-icon">power_settings_new</button>
            <span>Выйти</span>
        </a>
    </li>
</ul>