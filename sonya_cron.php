<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// https://oauth.vk.com/authorize?client_id=5413162&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends&response_type=token&v=5.52
// 2b99fe73cb9e25409494e2e2fd2c7a09e64c5620e9fd144c2dafdb6b6510b7d829f683e608c854e2ba187

$is_first = false;

$path = __DIR__ . '/';

$data = (object) array('times' => (array) array());
$time = date("H:i");
$date = date('Y-m-d');
if($time < "06:05"){
	$date = date('Y-m-d', strtotime($date. ' - 1 days'));
}
$file = $date . '.json';

if(is_file($path . 'sonya_stats/' . $file)){
	$data = json_decode(file_get_contents($path . 'sonya_stats/' . $file));
}else{
	if(date("H:i") > '05:50') $is_first = true;
}

if(empty($data->times)) $is_first = true;

$sonya_id = 312788880;
$token = "2b99fe73cb9e25409494e2e2fd2c7a09e64c5620e9fd144c2dafdb6b6510b7d829f683e608c854e2ba187";
$aData = json_decode(file_get_contents("https://api.vk.com/method/friends.getOnline?user_id=102114911&access_token={$token}&v=5.102"));
if(in_array($sonya_id, $aData->response)){
	$data->times[] = $time;

	if($is_first){
		$shift = (date("H:i") > '08:51')?'2':'1';
		$data->shift = $shift;
		messagesSend(102114911, 'Соня проснулась в ' . $time . ', сегодня она работает в ' . $shift . ' смену');
	}
}
file_put_contents($path . 'sonya_stats/' . $file, json_encode($data));

function _vkApi_call($method, $params = array()) {
    $params['access_token'] = 'f5bbceedf34f5585e0089fbd6e13ee3c22d450edf6fbb355fb31ecf0c655291b541b9080b9a65532fb8f0'; 
    $params['v'] = "5.102"; 
    $url = "https://api.vk.com/method/" . $method . '?' . http_build_query($params);
    $curl = curl_init($url); 
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
    $json = curl_exec($curl); 
    curl_close($curl); 
    $response = json_decode($json, true);
    if(isset($response['response'])){
        return $response['response']; 
    }
    dump($response);
}

function messagesSend($peer_id, $message, $attachments = array()) {
    return _vkApi_call('messages.send', array( 
        'user_ids' => $peer_id, 
        'message' => $message, 
        'random_id' => rand(0, 64), 
        'attachment' => implode(',', $attachments), 
    )); 
}