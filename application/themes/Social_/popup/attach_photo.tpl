<div class="popup_bg"></div>
<div class="popup popup-top" data-id="attach_photo">
    <div class="popup_header_holder">
        <div class="popup_header module_title">
            Прикрепление фотографии <button class="md-icon close" onclick="popup.hide('attach_photo');">close</button>
        </div>
    </div>
    <div class="popup_content module_content">
        <div class="file-upload">
            <input id="file" type="file" style="display: none;" onchange="upload.onFile('attach', '{$oUser->avatar_album}', this.files);" size="28" accept="image/jpeg,image/png,image/gif,image/heic,image/heif">
            <label for="file" class="button">Загрузить файл</label>
        </div>
        <div class="photos-choose-rows">
            {foreach from=$photos key="k" item="photo"}
            <a class="photo-choose-row" onclick="return wall.attachPhoto('{$photo->album}', '{$photo->id}', '{$photo->src}');">
                <div class="photo-row-img" style="background-image: url('{$photo->src}');"></div>
            </a>
            {/foreach}
        </div>
        
        <div class="load-info">
            <span id="message"></span>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!--<script src="/views/Social/js/drop_load.js"></script>-->