<?php
/* Smarty version 3.1.29, created on 2020-02-13 15:26:27
  from "/srv/slto.ru/www/application/themes/Social/sidebar/notify.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e45407384fe74_78103169',
  'file_dependency' => 
  array (
    '391f26aaaa0220c02d73a0b3014ed413fd868991' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/sidebar/notify.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:notify/item.tpl' => 1,
  ),
),false)) {
function content_5e45407384fe74_78103169 ($_smarty_tpl) {
?>
<div class="users-list">
	<div class="simple-scrollbar">
		<ul>
			<?php if ($_smarty_tpl->tpl_vars['notify']->value) {?>
			<li>
				<ul class="list">
					<?php
$_from = $_smarty_tpl->tpl_vars['notify']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_n_0_saved_item = isset($_smarty_tpl->tpl_vars['n']) ? $_smarty_tpl->tpl_vars['n'] : false;
$_smarty_tpl->tpl_vars['n'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['n']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['n']->value) {
$_smarty_tpl->tpl_vars['n']->_loop = true;
$__foreach_n_0_saved_local_item = $_smarty_tpl->tpl_vars['n'];
?>
						<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:notify/item.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('notify'=>$_smarty_tpl->tpl_vars['n']->value), 0, true);
?>

					<?php
$_smarty_tpl->tpl_vars['n'] = $__foreach_n_0_saved_local_item;
}
if ($__foreach_n_0_saved_item) {
$_smarty_tpl->tpl_vars['n'] = $__foreach_n_0_saved_item;
}
?>
				</ul>
			</li>
			<?php }?>
		</ul>
	</div>
</div><?php }
}
