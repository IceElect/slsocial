<?php
/* Smarty version 3.1.29, created on 2020-02-13 15:25:51
  from "/srv/slto.ru/www/application/themes/Social/popup/photo.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5e45404fe7cde4_39985424',
  'file_dependency' => 
  array (
    '280a6a12e6d1cc672996c7320c2acef18fda2df3' => 
    array (
      0 => '/srv/slto.ru/www/application/themes/Social/popup/photo.tpl',
      1 => 1576341907,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e45404fe7cde4_39985424 ($_smarty_tpl) {
?>
<div class="popup_bg"></div>
<div class="popup popup-photo" data-photo="<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
">
    <!--
    <div class="popup_header_holder">
        <div class="popup_header module_title">Альбом <?php echo $_smarty_tpl->tpl_vars['photo']->value->album_name;?>
 <a class="fl_r close" onclick="photo.photo(<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
, false, event);"><i class="fa fa-close fa-fw fa-lg"></i></a></div>
    </div>
    -->
    <div class="actions-holder" style="padding-left: 0px">
        <div class="actions">
            <ul class="actions-menu user-menu">
                <li class="loading"><a><i class="fa fa-circle-o-notch fa-spin"></i></a></li>
            </ul>
            <button class="action icon md-icon" onclick="photo.photo('<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
', false, event);">close</button>
            <div class="spacer"></div>
            <button class="action icon fa icon-dot-3" data-type="actions" data-action="photo" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
"></button>
        </div>
    </div>
    <div class="popup_content module_content no_p">
        <div class="photo" data-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
">
            <div class="photo-holder">
            
                <img src="<?php echo $_smarty_tpl->tpl_vars['photo']->value->src;?>
" alt="">
            
            </div>
            <!--
            <div class="info">
                <a onclick="photo.photoSet(<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
, 'cover', event);">Установить как обложку альбома</a>
                <br /><br />
                <?php echo $_smarty_tpl->tpl_vars['photo']->value->comment;?>

                <a class="post_like post_button <?php if ($_smarty_tpl->tpl_vars['photo']->value->is_liked) {?>selected<?php }?>" onclick="photo.like('<?php echo '<?php ';?>echo $this->photo['id']; <?php echo '?>';?>', 0, event);">
                    <i class="fa fa-thumbs-up fa-lg fa-fw"></i>
                    <span class="likes_count"><?php echo $_smarty_tpl->tpl_vars['photo']->value->likes_count;?>
</span>
                    <div class="alt top dark">Нравится</div>
                </a>
                <a class="post_like post_button <?php if ($_smarty_tpl->tpl_vars['photo']->value->is_disliked) {?>selected<?php }?>" onclick="photo.like('<?php echo '<?php ';?>echo $this->photo['id']; <?php echo '?>';?>', 1, event);">
                    <i class="fa fa-thumbs-down fa-lg fa-fw"></i>
                    <span class="dislikes_count"><?php echo $_smarty_tpl->tpl_vars['photo']->value->dislikes_count;?>
</span>
                    <div class="alt top dark">Не нравится</div>
                </a>
                <hr>
                <div class="send_holder">
                    <div class="send_form send_comment" data-photo-id="<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
">
                        
                        <div class="field" onclick="photo.showCommentFormButtons('<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
', event);" contenteditable="true" placeholder="Написать комментарий"></div>
                        <div class="buttons fl_r" hidden>
                            <button role="send" class="btn btn_accept" onclick="photo.comment('<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
');">Отправить</button>
                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="comments">
                    
                </div>
            </div>
            <div style="clear: both;"></div>
            -->

            <div class="clearfix"></div>
        </div>
    </div>
    <div class="actions-holder actions-bottom">
        <div class="actions">
            <div class="action">
                
                

                <span class="like-action <?php if ($_smarty_tpl->tpl_vars['photo']->value->is_liked) {?>active<?php }?>" onclick="photo.like(<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
, 0, event)">
                    <i class="md-icon">thumb_up</i>
                </span>

                <b class="action-count rating">
                    <?php if (($_smarty_tpl->tpl_vars['photo']->value->likes_count > 0 || $_smarty_tpl->tpl_vars['photo']->value->dislikes_count > 0)) {
echo ($_smarty_tpl->tpl_vars['photo']->value->likes_count-$_smarty_tpl->tpl_vars['photo']->value->dislikes_count);
}?>
                </b>
                
                <span class="dislike-action <?php if ($_smarty_tpl->tpl_vars['photo']->value->is_disliked) {?>active<?php }?>" onclick="photo.like(<?php echo $_smarty_tpl->tpl_vars['photo']->value->id;?>
, 1, event)">
                    <i class="md-icon">thumb_down</i>
                </span>
            </div>

            <!--
            <div class="action share <?php if (false) {?>active<?php }?>">
                 
                <i class="md-icon">share</i>
                 
                <?php if (false) {?>
                    <b class="action-count"></b>
                <?php }?>
            </div>
            -->

            <div class="spacer"></div>

            <div class="action action-comment">
                <span>
                    <b class="action-count"><?php if ($_smarty_tpl->tpl_vars['photo']->value->comments_count > 0) {
echo $_smarty_tpl->tpl_vars['photo']->value->comments_count;
}?></b>
                    <i class="md-icon">mode_comment</i>
                </span>
            </div>
        </div>
    </div>
</div><?php }
}
